//
//  Application.swift
//  tawla
//
//  Created by mouhammed ali on 12/25/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit
import LanguageManager_iOS
class Application: UIApplication,UIApplicationDelegate {
    
    override open var userInterfaceLayoutDirection: UIUserInterfaceLayoutDirection {
        
        get {
            if !UserDefaults.standard.bool(forKey: "openedBefore") {
                LanguageManager.shared.defaultLanguage = .ar
                UserDefaults.standard.set(true, forKey: "openedBefore")
            }
            if LanguageManager.shared.isRightToLeft {
                
                return .rightToLeft
                
            }else {
                
                return .leftToRight
                
            }
            
        }
    }
    
}
