//
//  Service.swift
//  tawla
//
//  Created by Moaz Ezz on 7/26/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import Foundation



class Service: Decodable {
    var id : Int?
    var name : String?
    var image : String?
    var icon : String?
}
class CategoryData: Decodable {
    var data : [Service]?
}
