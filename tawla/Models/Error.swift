//
//  Error.swift
//  Dream Box
//
//  Created by Moaz Ezz on 11/19/17.
//  Copyright © 2017 Moaz Ezz. All rights reserved.
//

import Foundation

class ErrorHandler : Decodable{
    var message : String?
    var errors : ErrorTypes?
    
    func parseError() -> String {
        var str = ""
        if let temp = self.errors?.email{
            str.append(temp[0])
        }
        if let temp = self.errors?.name{
            str.append(temp[0])
        }
        if let temp = self.errors?.password{
            str.append(temp[0])
        }
        if let temp = self.errors?.image_base64{
            str.append(temp[0])
        }
        if let temp = self.errors?.attachment{
            str.append(temp[0])
        }
        if let temp = self.errors?.hourly_rate{
            str.append(temp[0])
        }
        if let temp = self.errors?.city_id{
            str.append(temp[0])
        }
        if let temp = self.errors?.gender{
            str.append(temp[0])
        }
        if let temp = self.errors?.phone{
            str.append(temp[0])
        }
        if let temp = self.errors?.mobile{
            str.append(temp[0])
        }
        if let temp = self.errors?.password_confirmation{
            str.append(temp[0])
        }
        if let temp = self.errors?.specialization_id{
            str.append(temp[0])
        }
        if let temp = self.errors?.subject{
            str.append(temp[0])
        }
        if let temp = self.errors?.message{
            str.append(temp[0])
        }
        if let temp = self.errors?.body{
            str.append(temp[0])
        }
        if let temp = self.errors?.date{
            str.append(temp)
        }
        if let temp = self.errors?.errors_summary{
            str.append(temp)
        }
        if let temp = self.errors?.gathering_type_id{
            str.append(temp[0])
        }
        if let temp = self.errors?.table_type_id{
            str.append(temp[0])
        }
        if let temp = self.errors?.position{
            str.append(temp[0])
        }
        if let temp = self.errors?.value{
            str.append(temp[0])
        }
        if let temp = self.errors?.code{
            str.append(temp[0])
        }
        return str
    }
    
    
}
struct ErrorImd : Decodable {
    var errors : [ErrorTypes]?
}
struct ErrorTypes : Decodable {
    var email : [String]?
    var password : [String]?
    var name : [String]?
    var attachment : [String]?
    var hourly_rate : [String]?
    var city_id : [String]?
    var gender : [String]?
    var image_base64 : [String]?
    var phone : [String]?
    var mobile : [String]?
    var password_confirmation : [String]?
    var specialization_id : [String]?
    var message : [String]?
    var subject : [String]?
    var body : [String]?
    var date : String?
    var errors_summary : String?
    var gathering_type_id : [String]?
    var table_type_id : [String]?
    var position : [String]?
    var value : [String]?
    var code : [String]?
    
    
    
}
