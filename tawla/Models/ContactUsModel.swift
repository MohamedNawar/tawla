//
//  ContactUsModel.swift
//  tawla
//
//  Created by Moaz Ezz on 7/30/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import Foundation

class contactUsModel : Decodable{
    var data: contactUsModelData?
}
class contactUsModelData : Decodable{
    var phone: String?
    var mobile: String?
    var email : String?
    var website : String?
    var twitter : String?
    var instagram : String?
    var youtube : String?
    var about : About?
    var usage : About?
}

class About: Decodable {
    var id : Int?
    var name : String?
    var description : String?
}

