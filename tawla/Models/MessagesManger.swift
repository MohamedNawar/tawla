//
//  File.swift
//  tawla
//
//  Created by Moaz Ezz on 10/26/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import Foundation
import ActionSheetPicker_3_0

class MessagesManger {
    private init(){}
    static let shared = MessagesManger()
    
    var introTitle = [
        NSLocalizedString("Choose your favorite Restaurant and Cafe", comment:"")
        ,NSLocalizedString("Select your time and gathering type", comment:"")
,NSLocalizedString("Enjoy your Favorite Food and Drink", comment:"")]
    var introSubTitle = [NSLocalizedString("", comment:"")
        ,NSLocalizedString("example to put here", comment:"")
        ,NSLocalizedString("example to put here", comment:"")]
    
    var loginFirst = NSLocalizedString("You must login first", comment:"")
    var error = NSLocalizedString("Error", comment:"")
    var success = NSLocalizedString("Success", comment:"")
    var checkYourData = NSLocalizedString("", comment:"حدث خطا بالرجاء التأكد من صحة البيانات المدخلة")
    var checkYourData1 = NSLocalizedString("Terms and conditions must be approved", comment:"حدث خطا بالرجاء التأكد من صحة البيانات المدخلة")

    var tryAgainLater = NSLocalizedString("Something went wrong! Please try again later", comment:"حدث خطأ برجاء إعادة المحاولة لاحقاً")
    var done = NSLocalizedString("Done", comment:"تم")
    var passwordDontMatch = NSLocalizedString("Passwords Don't match", comment:"كلمة المرور غير متطابقة")
    var checkPhoneCode = NSLocalizedString("Please make sure from the phone number", comment:"من فضلك تأكد من إدخال الهاتف بطريقة صحيحة")
    var checkZeros = NSLocalizedString("please don't enter a Zero", comment:"برجاء عدم ادخال صفر")
    var emptyField = NSLocalizedString("Empty field", comment:"حقل فارغ")
    var gatheringType = NSLocalizedString("Choose Gathering Type", comment:"حقل فارغ")
     var tableType = NSLocalizedString("Choose Table Type", comment:"حقل فارغ")
       var tableType2 = NSLocalizedString("Choose Table Place inside or outside", comment:"حقل فارغ")
     var position = NSLocalizedString("Choose Category", comment:"حقل فارغ")
     var chooseMembers = NSLocalizedString("choose number of individuals", comment:"أختر عدد الأفراد ")
    var changePassSuccess = NSLocalizedString("The Password had been changed successfully", comment:"تم تغيير كلمة السر بنجاح")
    var codeDontMatch = NSLocalizedString("Sent Code is wrong", comment:"الكود المرسل غير صحيح")
    var showTimes = NSLocalizedString("Show avaliable times", comment:"أظهر الأوقات المتاحة")
    var reserveNow = NSLocalizedString("Reserve Now", comment:"احجز الأن")
    var orderNum = NSLocalizedString("Reservation Number", comment:"رقم الحجز")
    var orderDone = NSLocalizedString("Menu had been added to your reservation" + "\n" + "To check your reservetions please go to Reservations Page", comment:"تم اضافة المنيو الي حجزك" + "\n" + "للمتابعة الحجز من فضلك توجه لقسم حجوزاتي")
    var thanks = NSLocalizedString("Thank You", comment:"شكراً لك")
    var signIn = NSLocalizedString("Sign In", comment:"تسجيل دخول")
    var main = NSLocalizedString("Main", comment:"الرئيسية")
    var signOut = NSLocalizedString("Sign Out", comment:"تسجيل الخروج")
    var fav = NSLocalizedString("Favourite", comment:"المفضلة")
    var ContactUs = NSLocalizedString("Contact Us", comment:"تواصل معنا")
    var AboutUs = NSLocalizedString("About Us", comment:"عن التطبيق")
    var myReservations = NSLocalizedString("Reservations", comment:"حجوزاتي")
    var ReserveByRole = NSLocalizedString("ReserveByRole", comment:"حجوزاتي")
    var ReserveByTime = NSLocalizedString("ReserveByTime", comment:"حجوزاتي")
    var AddExceptionTime = NSLocalizedString("AddExceptionTime", comment:"حجوزاتي")
    var Exception = NSLocalizedString("Exception", comment:"حجوزاتي")
    var rates = NSLocalizedString("My Rates", comment:"تقيماتي")
    var myProfile = NSLocalizedString("Profile", comment:"الملف الشخصي")
    var chosseCity = NSLocalizedString("choose City", comment:"اختر مدينة")
    var termAndCoditions = NSLocalizedString("Agree on <u><b>Terms and Conditions</b></u>", comment:"الموافقة على <u><b>الشروط والأحكام</b></u>")
    var noFav = NSLocalizedString("No Favourite Places", comment:"لا يوجد أماكن مفضلة")
    var sorry = NSLocalizedString("Sorry", comment:"نأسف")
    var dayNotAvaliable = NSLocalizedString("Sorry But this day is not available", comment:"نأسف ولكن هذا اليوم غير متاح")
    var addToFav = NSLocalizedString("Added To Favourite", comment:"تمت الإضافة للمفضلة")
    var queue = NSLocalizedString("Number :", comment:"الدور :")
    var reserve = NSLocalizedString("Reserve", comment:"حجز")
    var chosseAnotherTime = NSLocalizedString("Choose different Time", comment:"اختيار ميعاد اخر")
    var noAvaliableTimes = NSLocalizedString("Sorry No available Time", comment:"عفوا لا  يوجد مواعيد متاحة ")
    var chosseADiffirantDay = NSLocalizedString("Please change you selections Or choose a different Time", comment:"من فضلك قم بتعديل الاختيار او تفضل باختيار يوم اخر")
    var reserveSucsse = NSLocalizedString("Reserved Successfully", comment:"تم الحجز بنجاح")
    var chosseFromFollowing = NSLocalizedString("Please choose from the following", comment:" من فضلك اخر احدي الاختيارات التاليه")
    var estimatedReserve = NSLocalizedString("Excpected reservetion", comment:"الحجز المتوقع")
    var activated = NSLocalizedString("Activated", comment:"تم التفعيل")
    var chosseMenu = NSLocalizedString("Please choose from the following", comment:"من فضلك قم باختيار من المنيو")
    var mustResendCode = NSLocalizedString("You have to resend the activation code", comment:"يجب إعادة ارسال كود التفعيل")
    var codeSent = NSLocalizedString("The activation code has been sent", comment:"تم إرسال كود التفعيل")
    var checkInternet = NSLocalizedString("Please Check Internet Connection", comment:"من فضلك قم باعادة الاتصال بالانترنت مرة اخري")
    
    var categories = NSLocalizedString("Categories", comment:"الأقسام")
    var mostVisited = NSLocalizedString("Most Visited", comment:"الأكثر زيارة")
    var all = NSLocalizedString("All", comment:"الكل")
    var offers = NSLocalizedString("Offers", comment:"العروض")
    var cancel = NSLocalizedString("Cancel", comment:"الغاء")
    var reject = NSLocalizedString("Reject", comment:"رفض")
    var appreve = NSLocalizedString("Aprrove", comment:"قبول")
    func reinit() {
        self.introTitle = [
            NSLocalizedString("Choose your favorite Restaurant and Cafe", comment:"")
            ,NSLocalizedString("Select your time and gathering type", comment:"")
            ,NSLocalizedString("Enjoy your Favorite Food and Drink", comment:"")]
        self.introSubTitle = [NSLocalizedString("", comment:"هذا مثال للنص يمكن ان يوضع هنا")
            ,NSLocalizedString("example to put here", comment:"هذا مثال للنص يمكن ان يوضع هنا")
            ,NSLocalizedString("example to put here", comment:"هذا مثال للنص يمكن ان يوضع هنا")]
        
        self.loginFirst = NSLocalizedString("You must login first", comment:"")
        self.error = NSLocalizedString("Error", comment:"")
        self.success = NSLocalizedString("Success", comment:"")
        self.checkYourData = NSLocalizedString("Something went wrong! Please check your enterd data", comment:"حدث خطا بالرجاء التأكد من صحة البيانات المدخلة")
        self.tryAgainLater = NSLocalizedString("Something went wrong! Please try again later", comment:"حدث خطأ برجاء إعادة المحاولة لاحقاً")
        self.done = NSLocalizedString("Done", comment:"تم")
        self.passwordDontMatch = NSLocalizedString("Passwords Don't match", comment:"كلمة المرور غير متطابقة")
        self.checkPhoneCode = NSLocalizedString("Please make sure from the phone number", comment:"من فضلك تأكد من إدخال الهاتف بطريقة صحيحة")
        self.checkZeros = NSLocalizedString("please don't enter a Zero", comment:"برجاء عدم ادخال صفر")
        self.emptyField = NSLocalizedString("Empty field", comment:"حقل فارغ")
        self.changePassSuccess = NSLocalizedString("The Password had been changed successfully", comment:"تم تغيير كلمة السر بنجاح")
        
        self.codeDontMatch = NSLocalizedString("Sent Code is wrong", comment:"الكود المرسل غير صحيح")
        self.showTimes = NSLocalizedString("Show avaliable times", comment:"أظهر الأوقات المتاحة")
        self.reserveNow = NSLocalizedString("Reserve Now", comment:"احجز الأن")
        self.orderNum = NSLocalizedString("Reservation Number", comment:"رقم الحجز")
        self.orderDone = NSLocalizedString("Menu had been added to your reservation" + "\n" + "To check your reservetions please go to Reservations Page", comment:"تم اضافة المنيو الي حجزك" + "\n" + "للمتابعة الحجز من فضلك توجه لقسم حجوزاتي")
        self.thanks = NSLocalizedString("Thank You", comment:"شكراً لك")
        self.signIn = NSLocalizedString("Sign In", comment:"تسجيل دخول")
        self.main = NSLocalizedString("Main", comment:"الرئيسية")
        self.signOut = NSLocalizedString("Sign Out", comment:"تسجيل الخروج")
        self.fav = NSLocalizedString("Favourite", comment:"المفضلة")
        self.ContactUs = NSLocalizedString("Contact Us", comment:"تواصل معنا")
        self.AboutUs = NSLocalizedString("About Us", comment:"عن التطبيق")
        self.myReservations = NSLocalizedString("Reservations", comment:"حجوزاتي")
        self.rates = NSLocalizedString("My Rates", comment:"تقيماتي")
        self.myProfile = NSLocalizedString("Profile", comment:"الملف الشخصي")
        self.chosseCity = NSLocalizedString("choose City", comment:"اختر مدينة")
        self.termAndCoditions = NSLocalizedString("Agree on <u><b>Terms and Conditions</b></u>", comment:"الموافقة على <u><b>الشروط والأحكام</b></u>")
        self.noFav = NSLocalizedString("No Favourite Places", comment:"لا يوجد أماكن مفضلة")
        self.sorry = NSLocalizedString("Sorry", comment:"نأسف")
        self.dayNotAvaliable = NSLocalizedString("Sorry But this day is not available", comment:"نأسف ولكن هذا اليوم غير متاح")
        self.addToFav = NSLocalizedString("Added To Favourite", comment:"تمت الإضافة للمفضلة")
        self.queue = NSLocalizedString("Number :", comment:"الدور :")
        self.reserve = NSLocalizedString("Reserve", comment:"حجز")
        self.chosseAnotherTime = NSLocalizedString("Choose different Time", comment:"اختيار ميعاد اخر")
        self.noAvaliableTimes = NSLocalizedString("Sorry No available Time", comment:"عفوا لا  يوجد مواعيد متاحة ")
        self.chosseADiffirantDay = NSLocalizedString("Please change you selections Or choose a different Time", comment:"من فضلك قم بتعديل الاختيار او تفضل باختيار يوم اخر")
        self.reserveSucsse = NSLocalizedString("Reserved Successfully", comment:"تم الحجز بنجاح")
        self.chosseFromFollowing = NSLocalizedString("Please choose from the following", comment:" من فضلك اخر احدي الاختيارات التاليه")
        self.estimatedReserve = NSLocalizedString("Excpected reservetion", comment:"الحجز المتوقع")
        self.activated = NSLocalizedString("Activated", comment:"تم التفعيل")
        self.chosseMenu = NSLocalizedString("Please choose from the following", comment:"من فضلك قم باختيار من المنيو")
        self.mustResendCode = NSLocalizedString("You have to resend the activation code", comment:"يجب إعادة ارسال كود التفعيل")
        self.codeSent = NSLocalizedString("The activation code has been sent", comment:"تم إرسال كود التفعيل")
        self.checkInternet = NSLocalizedString("Please Check Internet Connection", comment:"من فضلك قم باعادة الاتصال بالانترنت مرة اخري")
        
        self.categories = NSLocalizedString("Categories", comment:"الأقسام")
        self.mostVisited = NSLocalizedString("Most Visited", comment:"الأكثر زيارة")
        self.all = NSLocalizedString("All", comment:"الكل")
        self.offers = NSLocalizedString("Offers", comment:"العروض")
    }

}
//, SubTitle:
