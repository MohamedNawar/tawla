//
//  ResOrCafe.swift
//  tawla
//
//  Created by Moaz Ezz on 7/30/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import Foundation

struct ResOrCafeData: Decodable {
    var data: ResOrCafe?
}
struct ResOrCafe: Decodable {
    var address : String?
    var album : [Album]?
    var area : String?
    var banner_link : String?
    var city : String?
    var country : String?
    var details : String?
    var id : Int?
    var image : String?
    var latitude : String?
    var longitude : String?
    var name : String?
    var offers : [Place]?
    var phones : [String]?
    var place_features : [Place]?
    var place_owner : String?
    var place_type : Place?
    var rate : String?
    var rates : [Comment]?
    var social_media : Social_media?
    var working_schedule : String?
    var links : Links?
//    var links : [Links]?
    
    func getImagesAlbum() -> [String] {
        var temp = [String]()
        for i in (album ?? []){
            temp.append(i.link ?? "")
        }
        return temp
    }
    
    func getImagesOffers() -> [String] {
        var temp = [String]()
        for i in (offers ?? []){
            temp.append(i.image ?? "")
        }
        return temp
    }
    
    func getImagesFeatures() -> [String] {
        var temp = [String]()
        for i in (place_features ?? []){
            temp.append(i.image ?? "")
        }
        return temp
    }
}

struct Social_media : Decodable {
    var facebook : String?
    var google : String?
    var instagram : String?
    var twitter : String?
    var youtube : String?
}

struct Album : Decodable {
    var link : String?
    var media_id : Int?
    
}

struct Links :Decodable {
    var ss : String?
    var add_to_favorite : hepitous?
    var remove_from_favorites : hepitous?
    var rate : hepitous?
}


struct hepitous : Decodable {
    var href : String?
    var method : String?
}
