//
//  Place.swift
//  tawla
//
//  Created by Moaz Ezz on 7/26/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import Foundation


class Place : Decodable {
    var id : Int?
    var image : String?
    var name : String?
    var price : String?
}
