//
//  UserData.swift
//  Dream Box
//
//  Created by Moaz Ezz on 11/17/17.
//  Copyright © 2017 Moaz Ezz. All rights reserved.
//

import Foundation
import UIKit

struct userData : Decodable {
    var data : UserDetails?
    var message : String?
    var token : String?
    var reserveId :Int?
    
    func saveUser() {
        UIApplication.shared.registerForRemoteNotifications()
        UserDefaults.standard.set(token, forKey: "user")
        UserDefaults.standard.set(data?.email, forKey: "email")
        UserDefaults.standard.set(data?.name, forKey: "name")
//        UserDefaults.standard.set(data?.image?.thumb, forKey: "photo")
        UserDefaults.standard.set(data?.id, forKey: "id")
        UserDefaults.standard.set(data?.mobile, forKey: "mobile")
        UserDefaults.standard.set(data?.type, forKey: "type")
        UserDefaults.standard.set(data?.verified, forKey: "verified")
        
    }
    mutating func remove() {
         UIApplication.shared.unregisterForRemoteNotifications()
         UserDefaults.standard.removeObject(forKey: "reserveId")
        UserDefaults.standard.removeObject(forKey: "user")
        UserDefaults.standard.removeObject(forKey: "email")
        UserDefaults.standard.removeObject(forKey: "name")
//        UserDefaults.standard.removeObject(forKey: "photo")
        UserDefaults.standard.removeObject(forKey: "id")
        UserDefaults.standard.removeObject(forKey: "mobile")
        UserDefaults.standard.removeObject(forKey: "type")
        UserDefaults.standard.removeObject(forKey: "verified")
//        UserDefaults.standard.removeObject(forKey: "role_name")
//        UserDefaults.standard.removeObject(forKey: "phone")
        token = ""
        
    }
    mutating func removeReserveId() {
        UserDefaults.standard.removeObject(forKey: "reserveId")
    }
    mutating func fetchUser(){
        if let temp = UserDefaults.standard.object(forKey: "user" ) as? String{
            token = temp
        }
        var dataTemp = UserDetails()
//        var photoTemp = PhotoFormat()
        if let temp = UserDefaults.standard.object(forKey: "email" ) as? String{
            dataTemp.email = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "name" ) as? String{
            dataTemp.name = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "id" ) as? Int{
            dataTemp.id = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "mobile" ) as? String{
            dataTemp.mobile = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "type" ) as? String{
            dataTemp.type = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "verified" ) as? Int{
            dataTemp.verified = temp
        }
        if let  data = UserDefaults.standard.string(forKey: "reserveId") {
            self.reserveId = Int(data)
        }
        
        print(dataTemp)
//        dataTemp.image = photoTemp
        data = dataTemp
        
        
    }
    
    mutating func isVerified() -> Bool{
        if self.data?.verified == 1 || self.data?.type == "place_owner" {
            return true
        }else{
            return false
        }
    }
}

struct UserDetails : Decodable {
    var email : String?
    var id : Int?
    var name : String?
    var mobile: String?
    var type : String?
    var verified : Int?
//    var city_name : String?
//    var gender : String?
//    var image : PhotoFormat?
//    var area : DetailedData3?
//    var role : Int?
//    var role_name : String?
//    var 'class' : Int?
//    var unseen_messages : Int?
}


    
    
struct PhotoFormat : Decodable {
    var id : Int?
    var large : String?
    var medium : String?
    var original : String?
    var thumb : String?

}
struct link : Decodable {
    var edit : String?
}

struct Edit : Decodable {
    var edit : String?
    var editPassword : String?
    var href : String?
}
