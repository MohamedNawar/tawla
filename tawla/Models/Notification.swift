//
//  Notification.swift
//  tawla
//
//  Created by Moaz Ezz on 11/4/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import Foundation

class MyNotification: Decodable {
    var body : String?
    var date : String?
    var type : String?
    var url : String?
    var title : String?
    var data : NofityData?
   
}

class NofityData: Decodable {
    var body : String?
    var id : Int?
    var target_key : Int?
    var target_type : String?
    var title : String?
    var status : String?
}
//
//{
//    body = "reservations.flash.cancelled-body";
//    "dashboard_route" = "dashboard.reservations.show";
//    "dashboard_route_data" =                 (
//        56
//    );
//    id = 56;
//    route = "dashboard.reservations.show";
//    "route_data" =                 (
//        56
//    );
//    "target_key" = 50;
//    "target_type" = "App\\Models\\Customer";
//    title = "reservations.flash.cancelled-subject";
//    "translate_data" =                 {
//        customer = tt;
//        date = "2018-10-02";
//        place = "\U0633\U064a\U062f \U0643\U0627\U0641\U064a\U0629";
//    };
//    type = Reservation;
//}
