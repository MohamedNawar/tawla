//
//  Offer.swift
//  tawla
//
//  Created by Moaz Ezz on 8/14/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import Foundation

class Offer: Decodable {
    var id : Int?
    var image : String?
    var name : String?
    var price : String?
    var place : ResOrCafe?
}

