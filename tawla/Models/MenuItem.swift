//
//  MenuItem.swift
//  tawla
//
//  Created by Moaz Ezz on 8/4/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import Foundation

class MenuItem : Decodable {
    var name : String?
    var details : String?
    var id : Int?
    var image : String?
    var price : String?
    var quentity : Int?
    
}
