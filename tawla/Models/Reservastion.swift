//
//  Reservastion.swift
//  tawla
//
//  Created by Moaz Ezz on 7/31/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import Foundation



class Reservation: Decodable {
    var date : String?
    var id : Int?
    var gathering_types : [Service]?
    var mechanism : String?
    var schedules : Schedules?
    var table_types : [Service]?
    var customer : Customer?
    var end_at : String?
    var start_at : String?
    var place : Place?
    var status : String?
    var children_count : String?
    var men_count : String?
    var women_count : String?
    var table : Table?
    var number : Int?
    var gathering_type : Service?
}

class Reservation2: Decodable {
    var date : String?
    var id : Int?
    var gathering_types : [Service]?
    var mechanism : String?
    var schedules : Schedules?
    var table_types : [Service]?
    var customer : Customer?
    var end_at : String?
    var start_at : String?
    var place : Place?
    var status : String?
    var children_count : Int?
    var men_count : Int?
    var women_count : Int?
    var table : Table?
    var number : Int?
    var isExpanded : Bool?
    var created_at : String?
    var gathering_type : Service?
    var notes:String?
}

struct Customer : Decodable {
    var email : String?
    var id : Int?
    var mobile : String?
    var name : String?
    var type : String?
}

struct Schedules : Decodable {
    var formated_start_at : String?
    var start_at : String?
    var link : String?
    var method : String?
}
struct Times : Decodable {
    var date : String?
    var mechanism : String?
    var schedules : [Times2]?
}

struct Table : Decodable {
    var capacity : Int?
    var has_barrier : Int?
    var id : Int?
    var number : Int?
    var position : String?
    var table_type : Place?

}

struct Times2 : Decodable {
    var formated_start_at : String?
    var start_at : String?
}
