//
//  Helpers.swift
//  YourStore
//
//  Created by mouhammed ali on 7/7/18.
//  Copyright © 2018 Moaz Ezz. All rights reserved.
//

import Foundation


class Category : Decodable{
    var id : Int?
    var name : String?
}



struct links : Decodable {
    var messages: linksInner?
    var rate: linksInner?
    var rates: linksInner?
    var shop: linksInner?
}
struct linksInner:Decodable {
    var href = ""
    var method = ""
}
