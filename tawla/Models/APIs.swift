//
//  APIs.swift
//  YourStore
//
//  Created by Moaz Ezz on 7/4/18.
//  Copyright © 2018 ElnoorOnline. All rights reserved.
//

import Foundation
import LanguageManager_iOS

class APIs {
    static let sharedInstance = APIs()
    private init() {}
    
//    private let url = "http://tawla.elnooronlineworks.com/"
    private let url = "http://app.tawla.net/"
    
    
    public func getHeader() -> [String: String]{
        
        var header = [
            "Authorization" : "Bearer \(user.token ?? "")" ,
            "Accept" : "application/json"
        ]
        if LanguageManager.shared.currentLanguage == .ar {
            header["Accept-Language"] = "ar"
            return header
        }
        header["Accept-Language"] = "en"
        return header
    }
    
    public func getHeaderNotify() -> [String: String]{
        
        let header = [
            "Authorization" : "Bearer \(user.token ?? "")",
            "X-ACCESS-TOKEN" : "Bearer \(user.token ?? "")" ,
            "Accept" : "application/json"
        ]
        return header
    }
    
//    public func changeLang(lang:Bool){
//        if lang {//Arabic
//            MOLH.setLanguageTo("ar")
//            MOLH.reset()
//        }else{
//            MOLH.setLanguageTo("en")
//            MOLH.reset()
//        }
//        print(MOLHLanguage.currentAppleLanguage() )
//    }
    
    
    public func getPlaceType() -> String{
        return url + "api/place_types"
    }
    
    public func getServices(ForId:Int) -> String{
        return url + "api/place_types/\(ForId)/service_types"
    }
    
    public func getGatheringTypes() -> String{
        return url + "api/gathering_types"
    }
    
    public func AboutUs() -> String{
        return url + "api/pages"
    }
    
    public func getPlaces() -> String{
        print(url + "api/places")
        return url + "api/places"
    }
    
    public func getPlace(id:Int) -> String{
        return url + "api/places/\(id)"
    }
    
    public func signUp() -> String{
        return url + "api/auth/register"
    }
    
    public func logIn() -> String{
        return url + "api/auth/login"
    }
    public func forget() -> String{
        return url + "api/auth/password/forget"
    }
    
    public func checkCode() -> String{
        return url + "api/auth/password/check-code"
    }
    
    public func resetPass() -> String{
        return url + "api/auth/password/reset"
    }
    
    public func contactUs() -> String{
        return url + "api/contact_us"
    }
    
    public func Settings() -> String{
        print(url + "api/settings")
        return url + "api/settings"
    }
    
    public func getReservationType(id : Int) -> String{
        print(url + "api/places/\(id)/mechanism")
        return url + "api/places/\(id)/mechanism"
    }
    
    
    public func getReservationTime(id : Int) -> String{
        print(url + "api/places/\(id)/time-schedules")
        return url + "api/places/\(id)/time-schedules"
    }
    
    public func postReservationTime(id : Int) -> String{
        print(url + "api/places/\(id)/time-reservations")
        return url + "api/places/\(id)/time-reservations"
    }
    public func postReservationQueue(id : Int) -> String{
        print(url + "api/places/\(id)/queue-reservations")
        return url + "api/places/\(id)/queue-reservations"
    }
    
    public func getMenu(id : Int) -> String{
        print(url + "api/places/\(id)/menu_items")
        return url + "api/places/\(id)/menu_items"
    }
    
    public func getCities() -> String{
        print(url + "api/cities")
        return url + "api/cities"
    }
    
    public func postMenu(id : Int) -> String{
        print(url + "api/reservations/\(id)/menu_items")
        return url + "api/reservations/\(id)/menu_items"
    }
    
    
    public func getReservations() -> String{
        print(url + "api/reservations")
        return url + "api/reservations"
    }
    
    public func getFav() -> String{
        print(url + "api/favorites")
        return url + "api/favorites"
    }
    
    public func postFav(placeId:Int) -> String{
        print(url + "api/places/\(placeId)/favorites")
        return url + "api/places/\(placeId)/favorites"
    }
    
    public func postRate(id:Int) -> String{
        print(url + "api/reservations/\(id)/rates")
        return url + "api/reservations/\(id)/rates"
    }
    
    public func getOffers() -> String{
        print(url + "api/featured_offers")
        return url + "api/featured_offers"
    }
    
    public func putReservationApproved(id:Int) -> String{
        print(url + "api/reservations/\(id)/approved")
        return url + "api/reservations/\(id)/approved"
    }
    public func putReservationRejected(id:Int) -> String{
        print(url + "api/reservations/\(id)/rejected")
        return url + "api/reservations/\(id)/rejected"
    }
    public func putReservationCanceled(id:Int) -> String{
        print(url + "api/reservations/\(id)/cancelled")
        return url + "api/reservations/\(id)/cancelled"
    }
    
    public func sendMobileCode() -> String{
        print(url + "api/auth/resend-code")
        return url + "api/auth/resend-code"
    }
    public func sendMobileActivationCode() -> String{
        print(url + "api/auth/mobile-activation")
        return url + "api/auth/mobile-activation"
    }
    
    public func profile() -> String{
        print(url + "api/me")
        return url + "api/me"
    }
    
    public func Notifications() -> String{
        return url + "api/notifications"
    }
    public func putLang(lang:String) -> String{
        return url + "/api/locale/\(lang)"
    }
   
    
    
    
}
