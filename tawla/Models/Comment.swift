//
//  Comment.swift
//  tawla
//
//  Created by Moaz Ezz on 7/21/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import Foundation


class Comment: Decodable {
    var comment : String?
    var id : Int?
    var place : Place?
    var value : Int?
    var created_at:String?
    
}

