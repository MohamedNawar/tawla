//
//  CitiesAndSpecial.swift
//  Dream Box
//
//  Created by Moaz Ezz on 11/22/17.
//  Copyright © 2017 Moaz Ezz. All rights reserved.
//

import Foundation

import Foundation

class CitiesAndSpecial : Decodable {
    var data : [DetailedData3]?
    
    
    func getStrings()-> [String]{
        var tempArr = [String]()
        let countTemp = data?.count ?? 0
        for i in 0..<countTemp{
            tempArr.append(data![i].name ?? "")
        }
        return tempArr
    }
    
    func getIds()-> [Int]{
        var tempArr = [Int]()
        for i in 0..<(data?.count)!{
            tempArr.append(data![i].id)
        }
        return tempArr
    }
    
    func getName(id:Int)->String{
        var tempArr = MessagesManger.shared.chosseCity
        for i in (data ?? []){
            if i.id == id{
               tempArr = i.name ?? ""
            }
            
        }
        return tempArr
    }
}
struct DetailedData3 : Decodable {
    var name : String?
    var id = -1
    var country : DetailedData4?
}
struct DetailedData4 : Decodable {
    var is_default : Int?
    var id = -1
    var name : String?
}





