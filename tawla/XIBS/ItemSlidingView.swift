//
//  ItemSlidingView.swift
//  tawla
//
//  Created by Moaz Ezz on 7/19/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit
import HCSStarRatingView

class ItemSlidingView: UIView {

    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var descriptionLbl: UILabel!
    
    @IBOutlet weak var rate: HCSStarRatingView!
}
