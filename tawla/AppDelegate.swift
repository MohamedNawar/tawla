//
//  AppDelegate.swift
//  tawla
//
//  Created by Moaz Ezz on 7/9/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SideMenu
import FCAlertView
import GoogleMaps
import LanguageManager_iOS
import OneSignal


let mainColor = #colorLiteral(red: 0.05000000075, green: 0.05000000075, blue: 0.05000000075, alpha: 1)
let secondaryColor = #colorLiteral(red: 0.7600491643, green: 0.5759575963, blue: 0.2702492774, alpha: 1)
let mainColorGRAD = #colorLiteral(red: 0.9960784314, green: 0.8274509804, blue: 0.3176470588, alpha: 1)
let secondaryColorGRAD = #colorLiteral(red: 0.6862745098, green: 0.5254901961, blue: 0.1725490196, alpha: 1)

let NormalFont = UIFont(name: "Cairo", size: 17)
let BoldFont = UIFont(name: "Cairo-Bold", size: 17)
var lightMode = false
let lightModeColor = #colorLiteral(red: 0.3333011568, green: 0.3333538771, blue: 0.3332896829, alpha: 1)
let AlertColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,OSSubscriptionObserver {

    var window: UIWindow?
    private var reachability : Reachability!


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
//       L102Localizer.DoTheMagic()
        
        GMSServices.provideAPIKey("AIzaSyAKoS0Pp-hC3tldvFpVYPyyWjTqIzYsUfg")
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuFadeStatusBar = false
        LanguageManager.shared.setLanguage(language: Languages(rawValue: Locale.current.languageCode!)!) 
        let BarAppearace = UINavigationBar.appearance()

        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.backgroundColor = .clear
            BarAppearace.standardAppearance = navBarAppearance
            BarAppearace.scrollEdgeAppearance = navBarAppearance
        }
        let ToolBarAppearace = UIToolbar.appearance()
//        BarAppearace.barTintColor = .clear
        BarAppearace.setBackgroundImage(UIImage(), for: .default)
        BarAppearace.shadowImage = UIImage()
        BarAppearace.isTranslucent = true
        
        BarAppearace.titleTextAttributes = [
            NSAttributedStringKey.font: NormalFont!,
            NSAttributedStringKey.foregroundColor: UIColor.white
        ]
        ToolBarAppearace.backgroundColor = mainColor
        ToolBarAppearace.tintColor = UIColor.white
        let ButtonBarAppearace = UIBarButtonItem.appearance()
        ButtonBarAppearace.setTitleTextAttributes([
            NSAttributedStringKey.font: NormalFont as Any
            ], for: UIControlState.normal)
        ButtonBarAppearace.tintColor = UIColor.white
        
        //Keybored Setup
        IQKeyboardManager.shared.enable = true
        
        //For checking the Internet
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: ReachabilityChangedNotification, object: nil)
        self.reachability = Reachability.init()
        do {
            try self.reachability.startNotifier()
        } catch {
            
        }
        
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: true]
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: "59624abf-f992-4b44-b615-b4ef24775c1e",
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        OneSignal.add(self as OSSubscriptionObserver)
       
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    //Get called when the wifi statue changed
    @objc func reachabilityChanged(notification:Notification) {
        let reachability = notification.object as! Reachability
        if reachability.isReachable {
            if reachability.isReachableViaWiFi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        } else {
            makeDoneAlert(title: MessagesManger.shared.checkInternet, SubTitle: "", Image: #imageLiteral(resourceName: "img13"), color: UIColor.red)
            print("Network not reachable")
        }
    }
    
    // the alert to be Poped Up
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage, color : UIColor) {
        let alert = FCAlertView()
        alert.colorScheme = color
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: MessagesManger.shared.done, andButtons: nil)
    }
    func changeLang(lang:String) {
        if lang == "ar" {
            LanguageManager.shared.setLanguage(language: .ar)
        } else {
            LanguageManager.shared.setLanguage(language: .en)
            
        }
//        MessagesManger.shared.reinit()
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
//        rootviewcontroller.rootViewController = storyboard.instantiateInitialViewController()
//        let mainwindow = (UIApplication.shared.delegate?.window!)!
//        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
//        UIView.transition(with: mainwindow, duration: 0, options: .transitionFlipFromLeft, animations: { () -> Void in
//        }) { (finished) -> Void in
//        }
        
    }

    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
        }
        print("SubscriptionStateChange: \n\(stateChanges)")
        
        //The player id is inside stateChanges. But be careful, this value can be nil if the user has not granted you permission to send notifications.
        if let playerId = stateChanges.to.userId {
            
            UserDefaults.standard.set(playerId, forKey: "onesignalid")
            print(UserDefaults.standard.string(forKey: "onesignalid"))
            print("Current playerId \(playerId)")
            //            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "addOneSignalIdNC"), object: nil, userInfo: nil)
            
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
        
        if ( application.applicationState == .inactive || application.applicationState == .background){
            if let userInfoDict = userInfo["custom"] as? [String:AnyObject] {
                if let userInfoA = userInfoDict["a"] as? [String:AnyObject]{
                    
                    if let view = userInfoA["view"] as? String {
                        if view == "rate" {
                             if let id = userInfoA["id"] as? Int {
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "RattingViewController") as! RattingViewController
                                var s = ResOrCafe()
                            s.id = id
                            vc.shop = s
                            print(s)
                            vc.modalPresentationStyle = .overCurrentContext
                            UIApplication.shared.keyWindow?.currentViewController()?.present(vc, animated: true, completion: nil)
                        }
                        }
                        if view == "place" {
                            if let id = userInfoA["id"] as? Int {
                                print(id)
                          UserDefaults.standard.set(id ?? 0, forKey: "placeId")
                            }
                        }
                    }}}}}

}
//
//[AnyHashable("aps"): {
//    alert =     {
//        body = "\U0645\U062d\U062a\U0648\U0649 \U062a\U062c\U0631\U064a\U0628\U064a";
//        title = "\U0639\U0646\U0648\U0627\U0646 \U062a\U062c\U0631\U064a\U0628\U064a";
//    };
//    "mutable-content" = 1;
//    sound = default;
//    }, AnyHashable("custom"): {
//        a =     {
//            id = 10;
//            view = place;
//        };
//        i = "47029fb3-4645-4c2c-aa38-f630d3428492";
//    }]
