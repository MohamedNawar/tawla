//
//  MyReservationsVC2.swift
//  tawla
//
//  Created by Moaz Ezz on 9/11/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import SideMenu
import RSSelectionMenu
import LanguageManager_iOS
import JGProgressHUD


class MyReservationsVC2: UIViewController , UITableViewDelegate, UITableViewDataSource {
    var reservations = [Reservation2](){
        didSet{
            tableView.reloadData()
        }
    }
    var user = userData()
    var refreshControl = UIRefreshControl()
    @IBOutlet var tableView: UITableView!
    let hud = JGProgressHUD(style: .light)
    
    @IBOutlet weak var menuBtnOutlet: UIButton!
    @IBAction func menuBtn(_ sender: Any) {
        if LanguageManager.shared.currentLanguage == .ar{
            let VC = storyboard?.instantiateViewController(withIdentifier: "SlideMenuNavVC") as! UISideMenuNavigationController
            
            SideMenuManager.default.menuRightNavigationController = VC
            present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
        }else{
            let VC = storyboard?.instantiateViewController(withIdentifier: "SlideMenuNavVC") as! UISideMenuNavigationController
            
            SideMenuManager.default.menuLeftNavigationController = VC
            present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
        }
    }
    
    @IBOutlet weak var lanfBtnOutlet: UIButton!
    @IBAction func langBtn(_ sender: Any) {
        let simpleDataArray = ["اللغة العربية","English Language"]
        
        // Show menu with datasource array - Default SelectionType = Single
        // Here you'll get cell configuration where you can set any text based on condition
        // Cell configuration following parameters.
        // 1. UITableViewCell   2. Object of type T   3. IndexPath
        
        let selectionMenu =  RSSelectionMenu(dataSource: simpleDataArray) { (cell, object, indexPath) in
            if indexPath.row == 0{
                cell.textLabel?.text = object
                cell.imageView?.image = #imageLiteral(resourceName: "SA")
            }else{
                cell.textLabel?.text = object
                cell.imageView?.image = #imageLiteral(resourceName: "AC")
            }
        }
        
        
        selectionMenu.onDismiss = {(text) in
            if text.first == "اللغة العربية"{
                self.putLang(lang: "ar")
            }else {
                self.putLang(lang: "en")
            }
            
            
            
            
        }
        
        // show as PresentationStyle = Push
        selectionMenu.show(style: .popover(sourceView: lanfBtnOutlet, size: CGSize(width: 250, height: 100)), from: self)
    }
    
    func changeLang(lang:String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateInitialViewController()
        if lang == "ar" {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            Bundle.setLanguage(lang)
            LanguageManager.shared.setLanguage(language: .ar, rootViewController: viewController, animation: { view in
                MessagesManger.shared.reinit()
                view.transform = CGAffineTransform(scaleX: 2, y: 2)
                view.alpha = 0
            })
            
        } else {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            Bundle.setLanguage(lang)
            LanguageManager.shared.setLanguage(language: .en, rootViewController: viewController, animation: { view in
                MessagesManger.shared.reinit()
                view.transform = CGAffineTransform(scaleX: 2, y: 2)
                view.alpha = 0
            })
            
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.title = MessagesManger.shared.myReservations
        //
        let attributes = [NSAttributedStringKey.foregroundColor: mainColorGRAD, NSAttributedStringKey.font: UIFont(name: "Cairo", size: 14)]
        refreshControl.attributedTitle = NSAttributedString(string: NSLocalizedString("Pull to refresh", comment: ""),attributes:attributes as [NSAttributedStringKey : Any])
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        refreshControl.tintColor = mainColorGRAD
        
        tableView.addSubview(refreshControl) // not required when using UITableViewController
        // menuBtnOutlet.isHidden = false
        if user.data?.type == "customer"{
            self.navigationItem.leftBarButtonItem = nil
            self.navigationItem.rightBarButtonItem = nil
        }
        if user.data?.type == "place_owner"{
        }else{
            //            lanfBtnOutlet.isHidden = true
            //            self.navigationItem.hidesBackButton = false
        }
        getReservations(showHud: true)
    }
    //        getReservations(showHud: false)
    
    @objc func refresh(_ sender: Any) {
        getReservations(showHud: false)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        print(reservations.count)
        return reservations.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !(reservations[section].isExpanded ?? false) {
            return 1
        }else{
            return 2 }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            
            let cell2 =  Bundle.main.loadNibNamed("HeaderCell", owner: self, options: nil)?.first as! HeaderCell
            cell2.selectionStyle = .none
            
            //        img.setFAIconWithName(icon: .FAAngleDown, textColor: #colorLiteral(red: 0.4126763344, green: 0.4591623545, blue: 0.5150957704, alpha: 1))
            cell2.cellAction = { () in
                self.pressButton(indexPath.section)
            }
            cell2.approveAction = { () in
                print(indexPath.section)
                self.putReservationAprroved(id: self.reservations[indexPath.section].id ?? -1, type: 0, index: indexPath.section)
            }
            cell2.rejectAction = { () in
                self.putReservationAprroved(id: self.reservations[indexPath.section].id ?? -1, type: 1, index: indexPath.section)
            }
            cell2.cancelAction = { () in
                self.putReservationAprroved(id: self.reservations[indexPath.section].id ?? -1, type: 2, index: indexPath.section)
            }
            print(reservations[indexPath.section].id)
            cell2.SetupUI(res: reservations[indexPath.section])
            //        checkMyExpandable(section)
            return  cell2
        }else{
            if reservations[indexPath.section].mechanism == "time" {
                let cell4 = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! txtCell
                cell4.selectionStyle = .none
                cell4.setupUI(res: reservations[indexPath.section])
                return cell4
            }else{
                let cell5 = tableView.dequeueReusableCell(withIdentifier: "txtCell2") as! txtCell2
                cell5.selectionStyle = .none
                cell5.setupUI(res: reservations[indexPath.section])
                return cell5
            }
        }
        
        //            let txtView = cell.viewWithTag(1) as! UITextView
        //            txtView.text = stringArr[indexPath.row].replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
        //        return cell
        
        
    }
    
    private func pressButton(_ sender: Int){
        let indexPath = IndexPath(row: 0, section: sender )
        let isExpanded = self.reservations[sender].isExpanded ?? false
        self.reservations[sender].isExpanded = !isExpanded
        //        tableView.b
        UIView.performWithoutAnimation {
            
            
            if isExpanded {
                tableView.deleteRows(at: [indexPath], with: .none)
                let updateIndex = IndexPath(row: 0, section: sender-1 )
                //            tableView.reloadRows(at: [updateIndex], with: .none)
            } else {
                tableView.insertRows(at: [indexPath], with: .none)
                //            tableView.reloadRows(at: [indexPath], with: .none)
            }
            tableView.reloadRows(at: tableView!.indexPathsForVisibleRows!, with: .none)
            //   tableView.reloadData()
            //         tableView.endUpdates()
        }
    }
    
    private func checkMyExpandable(_ sender: Int){
        let indexPath = IndexPath(row: 0, section: sender )
        let isExpanded = self.reservations[sender].isExpanded ?? false
        //        self.reservations[sender].isExpanded = !isExpanded
        if isExpanded {
            tableView.deleteRows(at: [indexPath], with: .none)
            //            let updateIndex = IndexPath(row: 0, section: sender-1 )
            //            tableView.reloadRows(at: [updateIndex], with: .none)
        } else {
            tableView.insertRows(at: [indexPath], with: .none)
            //            tableView.reloadRows(at: [indexPath], with: .none)
        }
        tableView.reloadRows(at: tableView!.indexPathsForVisibleRows!, with: .none)
        
    }
    
    private func getReservations(showHud:Bool){
        print(user.data)
        let header = APIs.sharedInstance.getHeader()
        print(header)
        //        HUD.show(.progress)
        if showHud {
            hud.show(in: self.view)
            
        }
        Alamofire.request(APIs.sharedInstance.getReservations(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                self.refreshControl.endRefreshing()
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
                //                HUD.hide()
                self.hud.dismiss()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12"))
                        //                            HUD.flash(.labeledError(title: "", subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                        //                            user.saveUser(user: user)
                    }catch{
                        showError(text:MessagesManger.shared.checkYourData)
                    }
                }else{
                    if let temp = value as? Dictionary<String,AnyObject>{
                        if let temp2 = temp["data"]{
                            
                            
                            do {
                                let temp3 = try JSONSerialization.data(withJSONObject: temp2, options: .prettyPrinted)
                                self.reservations = try JSONDecoder().decode([Reservation2].self, from: temp3 )
                                
                                self.tableView.reloadData()
                                
                            }catch{
                                //                                HUD.hide()
                                self.hud.dismiss()
                                showError(text:MessagesManger.shared.tryAgainLater)
                            }
                        }
                    }
                }
                
                
            case .failure(_):
                //                HUD.hide()
                self.hud.dismiss()
                showError(text:MessagesManger.shared.tryAgainLater)
                break
            }
            
        }
    }
    
    func putReservationAprroved(id:Int,type:Int,index:Int){
        
        var link = ""
        switch type {
        case 0://approve
            link = APIs.sharedInstance.putReservationApproved(id: id)
        case 1://reject
            link = APIs.sharedInstance.putReservationRejected(id: id)
        case 2://cancel
            link = APIs.sharedInstance.putReservationCanceled(id: id)
        default:
            print(link)
        }
        
        let header = APIs.sharedInstance.getHeader()
        print(header)
        //        HUD.show(.progress)
        hud.show(in: self.view)
        Alamofire.request(link, method: .put, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                //                HUD.hide()
                self.hud.dismiss()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12"))
                        //                            HUD.flash(.labeledError(title: "", subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                        //                            user.saveUser(user: user)
                    }catch{
                        showError(text:MessagesManger.shared.checkYourData)
                    }
                }else{
                    if let temp = value as? Dictionary<String,AnyObject>{
                        if let temp2 = temp["data"]{
                            
                            
                            do {
                                let temp3 = try JSONSerialization.data(withJSONObject: temp2, options: .prettyPrinted)
                                self.reservations[index] = try JSONDecoder().decode(Reservation2.self, from: temp3)
                                self.tableView.reloadData()
                                
                                
                            }catch{
                                //                                HUD.hide()
                                self.hud.dismiss()
                                showError(text:MessagesManger.shared.checkYourData)
                            }
                        }
                    }
                }
                
                
            case .failure(_):
                //                HUD.hide()
                self.hud.dismiss()
                showError(text:MessagesManger.shared.checkYourData)
                break
            }
            
        }
    }
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        _ = alert.bounds
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: MessagesManger.shared.done, andButtons: nil)
    }
    private func putLang(lang:String){
        if (user.token ?? "") == "" {
            self.changeLang(lang: lang)
        }else{
            let header = APIs.sharedInstance.getHeader()
            
            HUD.show(.progress, onView: self.view)
            Alamofire.request(APIs.sharedInstance.putLang(lang: lang), method: .put, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
                switch(response.result) {
                case .success(let value):
                    print(response.value)
                    print(response.result.error)
                    print(response.response?.statusCode)
                    
                    print(value)
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                        
                        do {
                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                            HUD.hide()
                            self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12"))
                            
                            
                            //                            user.saveUser(user: user)
                        }catch{
                            showError(text:MessagesManger.shared.checkYourData)
                        }
                        
                    }else{
                        self.changeLang(lang: lang)
                    }
                    
                    
                case .failure(_):
                    HUD.hide()
                    showError(text:MessagesManger.shared.tryAgainLater)
                    break
                }
                
            }
            
        }
    }
    
}
