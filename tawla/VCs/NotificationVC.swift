//
//  NotificationVC.swift
//  tawla
//
//  Created by Moaz Ezz on 11/4/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit
import Alamofire
import JGProgressHUD
import FCAlertView
import LanguageManager_iOS
class NotificationVC: UIViewController {
    let hud = JGProgressHUD(style: .light)
    var notifyArr = [MyNotification]()
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        getNotifications()
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        
    }
    
    private func getNotifications(){
        //        HUD.show(.progress, onView: self.view)
        hud.show(in: self.view)
        let header = APIs.sharedInstance.getHeaderNotify()
        Alamofire.request(APIs.sharedInstance.Notifications(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                //                HUD.hide()
                self.hud.dismiss()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12"), color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1))
                        //                            HUD.flash(.labeledError(title: "", subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                        //                            user.saveUser(user: user)
                    }catch{
                        self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                        self.hud.textLabel.text = MessagesManger.shared.tryAgainLater
                        self.hud.dismiss(afterDelay: 1.0)
                    }
                }else{
                    if let temp = value as? Dictionary<String,AnyObject>{
                        if let temp2 = temp["data"]{
                            
                            
                            do {
                                let temp3 = try JSONSerialization.data(withJSONObject: temp2, options: .prettyPrinted)
                                self.notifyArr = try JSONDecoder().decode([MyNotification].self , from: temp3)
                                self.tableView.reloadData()
                            }catch{
                                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                                self.hud.textLabel.text = MessagesManger.shared.tryAgainLater
                                self.hud.dismiss(afterDelay: 1.0)
                            }
                        }
                    }
                }
                
            case .failure(_):
                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                self.hud.textLabel.text = MessagesManger.shared.tryAgainLater
                self.hud.dismiss(afterDelay: 1.0)
                
                break
                
            }
        }
        
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage, color : UIColor) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = false
        //        let updatedFrame = alert.bounds
        alert.colorScheme = color
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: MessagesManger.shared.done, andButtons: nil)
    }
    
}

extension NotificationVC : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifyArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath)
        let tilt = cell.viewWithTag(1) as? UILabel
        let subLbl = cell.viewWithTag(2) as? UILabel
        let timeLbl = cell.viewWithTag(3) as? UILabel
        
        tilt?.text = "\(notifyArr[indexPath.row].title ?? "")"
        subLbl?.text = "\(notifyArr[indexPath.row].body ?? "")"
        timeLbl?.text = "\(notifyArr[indexPath.row].date ?? "")"
        if LanguageManager.shared.currentLanguage == .ar {
            tilt?.textAlignment = .right
            subLbl?.textAlignment = .right
            timeLbl?.textAlignment = .left
        }else{
            tilt?.textAlignment = .left
            subLbl?.textAlignment = .left
            timeLbl?.textAlignment = .right
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard notifyArr[indexPath.row].type == "ReservationRateNotification" else {
            return
        }
        if notifyArr[indexPath.row].data?.status == "pendding" || notifyArr[indexPath.row].data?.status == "rejected"{
            return
        }else{
        let vc = storyboard?.instantiateViewController(withIdentifier: "RattingViewController") as! RattingViewController
            var s = ResOrCafe()
        s.id = notifyArr[indexPath.row].data?.id
        vc.shop = s
        print(s)
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
        }
    }
    
    
}



