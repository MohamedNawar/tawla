
import UIKit

class IntroVC2: UIViewController , UIScrollViewDelegate{
    
    let imageArray = [#imageLiteral(resourceName: "splash1"),#imageLiteral(resourceName: "splash2"),#imageLiteral(resourceName: "splash3")]
    
    
    var introTitleValue = [
        NSLocalizedString("Choose", comment:"")
        ,NSLocalizedString("Select", comment:"")
        ,NSLocalizedString("Enjoy", comment:"")]
    // OutLets
    //    @IBOutlet weak var IntroImages: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    
    @IBOutlet weak var skipOutlet: UIButton!
    @IBAction func skip(_ sender: Any) {
self.dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var nextOutlet: UIButton!
    @IBAction func next(_ sender: Any) {
        if pageController.currentPage == 2 {
            self.dismiss(animated: true, completion: nil)
        }else{
            pageController.currentPage = pageController.currentPage + 1
            let x = CGFloat(pageController.currentPage) * scrollView.frame.size.width
            scrollView.setContentOffset(CGPoint(x:x, y:0), animated: true)
        }
        
    }
    @IBOutlet weak var pageController: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true

        scrollView.frame = view.frame
        
        for i in 0..<imageArray.count {
            let imageView = UIImageView()
            let labelView = UILabel()
             let labelView1 = UILabel()
            //Positions
            let xPosition = self.scrollView.frame.width * CGFloat(i)
            let height = self.scrollView.frame.height
            
            //image
            imageView.frame = CGRect(x: xPosition, y: 0, width: self.scrollView.frame.width, height: height )
            imageView.contentMode = .scaleToFill
            imageView.image = imageArray[i]
            
            //label_1
            labelView.font = BoldFont?.withSize(22)
            labelView.text = MessagesManger.shared.introTitle[i]
            labelView.adjustsFontSizeToFitWidth = true
            
            labelView.frame = CGRect(x: xPosition + 10, y: height*3/4, width: self.scrollView.frame.width - 20, height: 30)
            labelView.textAlignment = .center
            labelView.textColor = UIColor.white
            labelView1.font = BoldFont?.withSize(40)
            labelView1.text = introTitleValue[i]
            labelView1.adjustsFontSizeToFitWidth = true
            
            labelView1.frame = CGRect(x: xPosition + 10, y: height/5, width: self.scrollView.frame.width - 20, height: 50)
            labelView.textAlignment = .center
            labelView.textColor = UIColor.white
            labelView1.textAlignment = .center
            labelView1.textColor = UIColor.white
            scrollView.contentSize.width = scrollView.frame.width * CGFloat(i + 1)
            
            scrollView.addSubview(imageView)
            scrollView.addSubview(labelView)
            scrollView.addSubview(labelView1)

            print("frame : \(imageView.frame)")
            
        }
        pageController.addTarget(self, action: #selector(self.changePage(sender:)), for: UIControlEvents.valueChanged)
        
    }
    
    @objc func changePage(sender: AnyObject) -> () {
        let x = CGFloat(pageController.currentPage) * scrollView.frame.size.width
        scrollView.setContentOffset(CGPoint(x:x, y:0), animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageController.currentPage = Int(pageNumber)
    }
    
    
    
}
