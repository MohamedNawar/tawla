//
//  MainVC.swift
//  YourStore
//
//  Created by Moaz Ezz on 4/11/18.
//  Copyright © 2018 ElnoorOnline. All rights reserved.
//

import UIKit
import Auk
import Alamofire
import PKHUD
import moa
import SideMenu
import FCAlertView
import SDWebImage
import Font_Awesome_Swift
import LanguageManager_iOS
import JGProgressHUD

class MainVC: UIViewController, UIScrollViewDelegate {
    var imagesArray = [#imageLiteral(resourceName: "img55"),#imageLiteral(resourceName: "img56"),#imageLiteral(resourceName: "img59"),#imageLiteral(resourceName: "img44"),#imageLiteral(resourceName: "img61"),#imageLiteral(resourceName: "img62"),#imageLiteral(resourceName: "img30"),#imageLiteral(resourceName: "img15"),#imageLiteral(resourceName: "img59"),#imageLiteral(resourceName: "img26"),#imageLiteral(resourceName: "img47"),#imageLiteral(resourceName: "img63")]
    var places = [Place]()
    var firstEnterAsUser = false
    var isfirstToOpenThisView = true
    let hud = JGProgressHUD(style: .light)
    var item = ResOrCafeData()
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBAction func SideMenuPressed(_ sender: Any) {
        if LanguageManager.shared.currentLanguage == .ar{
            let VC = storyboard?.instantiateViewController(withIdentifier: "SlideMenuNavVC") as! UISideMenuNavigationController
            
            SideMenuManager.default.menuRightNavigationController = VC
            present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
        }else{
            let VC = storyboard?.instantiateViewController(withIdentifier: "SlideMenuNavVC") as! UISideMenuNavigationController
            
            SideMenuManager.default.menuLeftNavigationController = VC
            present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
        }
        
    }
    
    @IBOutlet weak var sideMenuBtn: UIButton!
    
    
    @IBAction func SignUp(_ sender: Any) {
        
//        let navVC = storyboard?.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
//        self.navigationController?.pushViewController(navVC, animated: true)
        let navVC = storyboard?.instantiateViewController(withIdentifier: "navgationC")
        self.present(navVC!, animated: true, completion: nil)
    }
    
    @IBAction func chooseLoc(_ sender: Any) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "navVC3") as! UINavigationController
        self.present(navVC, animated: true, completion: nil)
    }
    
    @IBOutlet weak var collectionBotomCon: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if LanguageManager.shared.currentLanguage == .ar && isfirstToOpenThisView{
//            LanguageManager.shared.setLanguage(language: .ar)
//            MessagesManger.shared.reinit()
//            isfirstToOpenThisView = false
//            UIApplication.topViewController!.dismiss(animated: true) {
//                let delegate = UIApplication.shared.delegate as! AppDelegate
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let vc = storyboard.instantiateViewController(withIdentifier: "MainVC") as! MainVC
//                let navVC = UINavigationController.init(rootViewController: vc)
//                vc.isfirstToOpenThisView = false
//                delegate.window?.rootViewController = navVC
//            }
//        }
//        if LanguageManager.shared.currentLanguage == .en && isfirstToOpenThisView{
//            LanguageManager.shared.setLanguage(language: .en)
//            MessagesManger.shared.reinit()
//            isfirstToOpenThisView = false
//            UIApplication.topViewController!.dismiss(animated: true) {
//                let delegate = UIApplication.shared.delegate as! AppDelegate
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let vc = storyboard.instantiateViewController(withIdentifier: "MainVC") as! MainVC
//                let navVC = UINavigationController.init(rootViewController: vc)
//                vc.isfirstToOpenThisView = false
//                delegate.window?.rootViewController = navVC
//            }
//        }
        
        user.fetchUser()
        checkIsFirst()
        if user.token != "" && user.token != nil && !firstEnterAsUser{
            
            sideMenuBtn.removeFromSuperview()
            collectionBotomCon.constant = 0
            firstEnterAsUser = true
        }
//        PackagesSlideView.delegate = self
//        setupAuk()
//        getPackages()
        
        if user.data?.type == "place_owner"{
            print(user.data?.type)
            let vc = storyboard?.instantiateViewController(withIdentifier: "MyReservationsVC2") as! MyReservationsVC2
            vc.navigationItem.hidesBackButton = true
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
//        if !user.isVerified() && user.token != "" && user.token != nil {
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MobileActivationVC") as! MobileActivationVC
//            self.navigationController?.pushViewController(vc, animated: true)
//        }

    }
    private func getPlacesWithFilter(id:Int){
        
        let header = APIs.sharedInstance.getHeader()
        let url = APIs.sharedInstance.getPlaces()
        print(APIs.sharedInstance.getPlace(id: id))
        Alamofire.request(APIs.sharedInstance.getPlace(id:id), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        print(err.message)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        print(value)
                        self.item = try JSONDecoder().decode(ResOrCafeData.self, from: response.data!)
                        print(self.item.data?.id ?? 0)
                        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "ItemVC") as! ItemVC
                        navVC.item = self.item.data ?? ResOrCafe()
                        self.navigationController?.pushViewController(navVC, animated: true)
                       
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        print(error)
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        let placeId = UserDefaults.standard.integer(forKey: "placeId")
        if placeId > 0 {
            getPlacesWithFilter(id:placeId)
          
        }
        self.title = NSLocalizedString("Main", comment: "")
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        if user.token != "" && user.token != nil && !firstEnterAsUser{
            sideMenuBtn.removeFromSuperview()
            collectionBotomCon.constant = 0
            firstEnterAsUser = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UserDefaults.standard.removeObject(forKey: "placeId")
    }
    
    func checkIsFirst(){
        
        guard UserDefaults.standard.string(forKey: "SelectedLoc") != nil else {
            let navVC = storyboard?.instantiateViewController(withIdentifier: "navVC3") as! UINavigationController
            self.present(navVC, animated: true, completion: nil)
            getPlaceType()
            return
        }
        
        getPlaceType()
    }
    
    @objc func getPlaceType(){
//        HUD.show(.progress, onView: self.view)
        hud.show(in: self.view)

        let header = APIs.sharedInstance.getHeader()
        Alamofire.request(APIs.sharedInstance.getPlaceType(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in



            switch(response.result) {
            case .success(let value):
//                HUD.hide()
                self.hud.dismiss()

                print(value)
                if let temp = value as? Dictionary<String,AnyObject>{
                    if let temp2 = temp["data"]{


                        do {
                            let temp3 = try JSONSerialization.data(withJSONObject: temp2, options: .prettyPrinted)
                            self.places = try JSONDecoder().decode([Place].self, from: temp3 )
                            self.collectionView.reloadData()
                        }catch{
//                            HUD.hide()
                            self.hud.dismiss()

                            showError(text:MessagesManger.shared.tryAgainLater)
                        }
                    }
                }

            case .failure(_):
                HUD.hide()
                showError(text:MessagesManger.shared.tryAgainLater)

                break

            }
        }

    }
 
}


extension MainVC : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return places.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath)
        if indexPath.row == 0 {
            let catLbl = cell.viewWithTag(-1) as! UILabel
            let catImage = cell.viewWithTag(2) as! UIImageView
            catLbl.text = places[1].name
            let url = URL(string: places[1].image ?? "")
            catImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "img02"), options: .highPriority, completed: nil)
        }
        if indexPath.row == 1 {
            let catLbl = cell.viewWithTag(-1) as! UILabel
            let catImage = cell.viewWithTag(2) as! UIImageView
            catLbl.text = places[0].name
            let url = URL(string: places[0].image ?? "")
            catImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "img02"), options: .highPriority, completed: nil)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        let img = cell?.viewWithTag(5) as! UIImageView
        img.image = #imageLiteral(resourceName: "img7_1")
        if indexPath.row == 1 {
            let navVC = storyboard?.instantiateViewController(withIdentifier: "SubMainVC") as! SubMainVC
            navVC.title = places[0].name
            navVC.SelectedPlace = places[0]
            self.navigationController?.pushViewController(navVC, animated: true)
        }
        if indexPath.row == 0 {
            let navVC = storyboard?.instantiateViewController(withIdentifier: "SubMainVC") as! SubMainVC
            navVC.title = places[1].name
            navVC.SelectedPlace = places[1]
            self.navigationController?.pushViewController(navVC, animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        let img = cell?.viewWithTag(5) as! UIImageView
        img.image = #imageLiteral(resourceName: "img07")
    }
}

extension MainVC : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        print("collectionView height : \(collectionView.bounds.size.height)")
        print("collectionView width : \(collectionView.bounds.size.width)")
        return CGSize(width: collectionView.bounds.size.width , height: 180)
    }
    
}
