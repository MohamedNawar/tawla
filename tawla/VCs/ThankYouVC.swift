//
//  ThankYouVC.swift
//  tawla
//
//  Created by Moaz Ezz on 11/2/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit

class ThankYouVC: UIViewController {
    var SelectedItem = ResOrCafe()
    var reservstion = Reservation()
    
    
    var isMenu = false
    @IBAction func NotNowBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBOutlet weak var orderNumber: UILabel!
    @IBOutlet weak var mainLbl: UILabel!
    @IBOutlet weak var forlaterBtn: UIButton!
    @IBOutlet weak var nowBtnOutlet: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isMenu{
            nowBtnOutlet.isHidden = true
            forlaterBtn.setTitle(MessagesManger.shared.done , for: .normal)
        }
        self.mainLbl.text = MessagesManger.shared.thanks
        orderNumber.text = MessagesManger.shared.orderNum + "  \(user.reserveId ?? 0)"
        // Do any additional setup after loading the view.
    }
    
}
