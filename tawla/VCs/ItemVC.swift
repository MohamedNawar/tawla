//
//  ItemVC.swift
//  tawla
//
//  Created by Moaz Ezz on 7/21/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit
import HCSStarRatingView
import PKHUD
import Alamofire
import FCAlertView
import ActionSheetPicker_3_0
import JGProgressHUD
import LanguageManager_iOS
class ItemVC: UIViewController {
    var item = ResOrCafe(){
        didSet{
            
            
            print(self.item.id)
        }
    }
    var comments = [Comment]()
    var dateHolder = String()
    var reservstion = Reservation()
  
    
    @IBOutlet weak var mainImageView: UIImageView!
    let hud = JGProgressHUD(style: .light)
    @IBOutlet weak var details: UILabel!
    @IBOutlet weak var name: UILabel!
    var txtDatePicker:UITextField?
    @IBOutlet weak var rate: HCSStarRatingView!
    
    @IBOutlet weak var rateBottom: HCSStarRatingView!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var rateLbl: UILabel!
    
    @IBOutlet weak var phone: UILabel!
    
    @IBOutlet weak var time: UILabel!
    
    @IBOutlet weak var AlbumCollectionView: UICollectionView!
    @IBOutlet weak var ServicesCollectionView: UICollectionView!
    @IBOutlet weak var offersCollectionView: UICollectionView!
    
    
    @IBOutlet weak var mainImageConsraon: NSLayoutConstraint!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var favBtn: UIButton!
    
    @IBAction func favPressed(_ sender: Any) {
        if user.token == "" || user.token == nil{
            showError(text: MessagesManger.shared.loginFirst)
            return
        }
        let temp = favBtn.isSelected
        favBtn.isSelected = !temp
        addFav()
    }
    @IBAction func ratePressed(_ sender: Any) {
        if user.token == "" || user.token == nil{
            showError(text: MessagesManger.shared.loginFirst)
            return
        }
        let vc = storyboard?.instantiateViewController(withIdentifier: "RattingViewController") as! RattingViewController
        vc.shop = item
        vc.modalPresentationStyle = .overCurrentContext
        vc.FinishPressed = { () in
            self.getPlace()
        }
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func goToMap(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ShowLocationInMapViewController") as! ShowLocationInMapViewController
        vc.isPlaceLoc = true
        //        vc.modalPresentationStyle = .overCurrentContext
        vc.shop = item
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func ReserveNow(_ sender: Any) {
        txtDatePicker?.becomeFirstResponder()
//        ActionSheetDatePicker.show(withTitle: "", datePickerMode: .date , selectedDate: date, doneBlock: {
//            picker, indexes, values in
//            if let dateTemp2 = indexes {
//                self.dateHolder = self.getDate(date: dateTemp2 as! Date, flag: true)
//                self.getReservation(date: self.dateHolder)
//            }
//            return
//        }, cancel: nil, origin: self.view)
    }
    private func getReservation(date:String){
        let header = APIs.sharedInstance.getHeader()
        let par = ["date":date]
//        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.sharedInstance.getReservationType(id: item.id ?? -1), method: .get, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
                
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
//                        HUD.hide()
                        self.hud.dismiss()
                        self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12"))
                        
                        
                    }catch{
                        showError(text:MessagesManger.shared.checkYourData)
                    }
                    
                }else{
                    do {
                        self.reservstion = try JSONDecoder().decode(Reservation.self, from: response.data! )
                        print("done")
                        self.goToNext()
//                        HUD.hide()
                        self.hud.dismiss()
                        
                        //                                self.setupUpUI()
                    }catch{
                        
                        showError(text:MessagesManger.shared.checkYourData)
                    }
                }
                
                
            case .failure(_):
//                HUD.hide()
                self.hud.dismiss()
                showError(text:MessagesManger.shared.checkYourData)
                break
            }
            
        }
    }
    
    private func goToNext() {
        if reservstion.mechanism != "time" && reservstion.mechanism != "queue" {
            self.makeDoneAlert(title: MessagesManger.shared.sorry, SubTitle: MessagesManger.shared.dayNotAvaliable , Image: #imageLiteral(resourceName: "img18"))
        }else{
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyNavVC2") as! MyNavVC2
            vc.selectedItem = self.item
            vc.reservstion = self.reservstion
            vc.dateHolder = self.dateHolder
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    private func getDate(date:Date,flag:Bool) -> String{//return date as formated -> Y-m-d 11-15-2017
        
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        let second = calendar.component(.second, from: date)
        return flag ? "\(year)-\(String(format: "%02d", month))-\(String(format: "%02d", day))" : "\(String(format: "%02d", hour)):\(String(format: "%02d", minute)):\(String(format: "%02d", second))"
    }
    
    
    
    override func viewDidLoad() {
        if #available(iOS 13, *)
        {
            let statusBar = UIView(frame: (UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame)!)
            statusBar.backgroundColor = UIColor.systemBackground
            UIApplication.shared.keyWindow?.addSubview(statusBar)
        }
        setNeedsStatusBarAppearanceUpdate()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.20464676)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.20464676)
        
        self.navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.20464676)
        self.navigationController?.navigationBar.backItem?.backBarButtonItem?.tintColor = .clear
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        txtDatePicker = UITextField(frame: CGRect(x: -5, y: -100, width: 0, height: 0) )
        self.view.addSubview(txtDatePicker ?? UIView())
        showDatePicker()
        super.viewDidLoad()
        tableView.estimatedRowHeight = 128
        tableView.rowHeight = UITableViewAutomaticDimension
       
        
        mainImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.mainImagePressed(sender:))))
        
    }
    
    @objc func mainImagePressed(sender:UITapGestureRecognizer) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PhotosVC") as! PhotosVC
        vc.imagesIndex = 0
        vc.imageArray = [(item.image) ?? ""]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)
        getPlace()
        self.title = item.name
        name.text = item.name
        details.text = item.details ?? ""
        let url = URL(string: item.image ?? "")
        mainImageView?.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "img02"), options: .highPriority, completed: nil)
        rate.value = CGFloat((item.rate as NSString?)?.floatValue ?? 0.0)
        rateBottom.value = CGFloat((item.rate as NSString?)?.floatValue ?? 0.0)
        rateLbl.text = item.rate
        address.text = item.address
        phone.text = ""
        setupUI()
        for i in (item.phones ?? []) {
            phone.text = phone.text! + i + " - "
        }
        time.text = item.working_schedule ?? "----"
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
         self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
    }
    
    @IBOutlet weak var rateBtn: UIButton!
    
    private func setupUI(){
        if item.links?.remove_from_favorites != nil {
            favBtn.isSelected = true
        }else{
            favBtn.isSelected = false
        }
        if item.links?.rate != nil {
            rateBtn.isHidden = false
        }else{
            rateBtn.isHidden = true
        }
    }
    
    
    private func getPlace(){
        
        let header = APIs.sharedInstance.getHeader()
        
        
        
        print(APIs.sharedInstance.getPlace(id: item.id ?? -1))
        print(APIs.sharedInstance.getPlace(id: item.id ?? -1))
        Alamofire.request(APIs.sharedInstance.getPlace(id: item.id ?? -1), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                
                print(value)
                if let temp = value as? Dictionary<String,AnyObject>{
                    if let temp2 = temp["data"]{
                        
                        
                        do {
                            let temp3 = try JSONSerialization.data(withJSONObject: temp2, options: .prettyPrinted)
                                self.item = try JSONDecoder().decode(ResOrCafe.self, from: temp3 )
                            self.setupUI()
                            self.tableView.reloadData()
                        }catch{
                            self.setupUI()
                        }
                    }
                }
                
            case .failure(_):
                self.setupUI()
                
                break
                
            }
        }
        
    }
    
    private func addFav(){
        
        
        let header = APIs.sharedInstance.getHeader()
        
        
        //        let url = APIs.sharedInstance.postMenu(id: reservation.id ?? -1);
        Alamofire.request(APIs.sharedInstance.postFav(placeId: item.id ?? -1), method: .post, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
                
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12"))
                   
                    }catch{
                        showError(text:MessagesManger.shared.checkYourData)
                    }
                }else{
                    showSucces(text: MessagesManger.shared.addToFav, completion: {
                        self.getPlace()
                    })
                    
                }
                
            case .failure(_):
                self.setupUI()
                break
            }
            
        }
    }
    @IBAction func goToYoutube(_ sender: Any) {
        if let social = item.social_media?.youtube {
        if let url = NSURL(string: "\(social)"){
            print(url)
            UIApplication.shared.openURL(url as URL)
        }
        }else{
            return
        }
    }
    
    @IBAction func goToInstgram(_ sender: Any) {
        if let social = item.social_media?.instagram {
            if let url = NSURL(string: "\(social)"){
                print(url)
                UIApplication.shared.openURL(url as URL)
            }
        }else{
            return
        }
    }
    @IBAction func goToFaceBook(_ sender: Any) {
        if let social = item.social_media?.facebook {
            if let url = NSURL(string: "\(social)"){
                print(url)
                UIApplication.shared.openURL(url as URL)
            }
        }else{
            return
        }
    }
    
    @IBAction func goToGoogle(_ sender: Any) {
        if let social = item.social_media?.google {
            if let url = NSURL(string: "\(social)"){
                print(url)
                UIApplication.shared.openURL(url as URL)
            }
        }else{
            return
        }
    }
    @IBAction func goToTwitter(_ sender: Any) {
        if let social = item.social_media?.twitter {
            if let url = NSURL(string: "\(social)"){
                print(url)
                UIApplication.shared.openURL(url as URL)
            }
        }else{
            return
        }
    }
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        _ = alert.bounds
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: MessagesManger.shared.done, andButtons: nil)
    }
    
    let datePicker = UIDatePicker()
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        datePicker.locale = Locale(identifier: LanguageManager.shared.currentLanguage.rawValue)
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: MessagesManger.shared.done, style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: ""), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        txtDatePicker?.inputAccessoryView = toolbar
        txtDatePicker?.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        
        self.dateHolder = self.getDate(date: datePicker.date, flag: true)
        print(datePicker.date)
        self.getReservation(date: self.dateHolder)

        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }

}


extension ItemVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return item.rates?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentCell
        cell.content.text = item.rates?[indexPath.row].comment ?? "---"
        cell.date.text = item.rates?[indexPath.row].created_at ?? "---"
        cell.rate.value = CGFloat(item.rates?[indexPath.row].value ?? 0)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableViewAutomaticDimension
    }
    
    
}

extension ItemVC : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == AlbumCollectionView {
            return item.album?.count ?? 0
        }else if collectionView == ServicesCollectionView{
            return item.place_features?.count ?? 0
        }else{
            return item.offers?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView{
        case AlbumCollectionView:
            let cell = AlbumCollectionView.dequeueReusableCell(withReuseIdentifier: "AlbumCell", for: indexPath)
            let photo = cell.viewWithTag(1) as? UIImageView
            let url = URL(string: item.album?[indexPath.row].link ?? "")
            photo?.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "img02"), options: .highPriority, completed: nil)
            return cell
        case ServicesCollectionView:
            let cell = ServicesCollectionView.dequeueReusableCell(withReuseIdentifier: "ServiceCell", for: indexPath)
            let lbl = cell.viewWithTag(2) as? UILabel
            let photo = cell.viewWithTag(1) as? UIImageView
            let url = URL(string: item.place_features?[indexPath.row].image ?? "")
            photo?.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "img02"), options: .highPriority, completed: nil)
            lbl?.text = item.place_features?[indexPath.row].name
            return cell
        default:
            let cell = offersCollectionView.dequeueReusableCell(withReuseIdentifier: "offerCell", for: indexPath)
            let photo = cell.viewWithTag(1) as? UIImageView
            let lbl = cell.viewWithTag(2) as? UILabel
            let lblPrice = cell.viewWithTag(3) as? UILabel
            
            let url = URL(string: item.offers?[indexPath.row].image ?? "")
            photo?.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "img02"), options: .highPriority, completed: nil)
            
            lbl?.text = item.offers?[indexPath.row].name
            lblPrice?.text = item.offers?[indexPath.row].price
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView ==  AlbumCollectionView{
            let vc = storyboard?.instantiateViewController(withIdentifier: "PhotosVC") as! PhotosVC
            vc.imagesIndex = indexPath.item
            vc.imageArray = item.getImagesAlbum()
            self.navigationController?.pushViewController(vc, animated: true)
        }
            
//        case ServicesCollectionView:
//            let vc = storyboard?.instantiateViewController(withIdentifier: "PhotosVC") as! PhotosVC
//            vc.imagesIndex = indexPath.item
//            vc.imageArray = item.getImagesFeatures()
//            self.navigationController?.pushViewController(vc, animated: true)
//        default:
//            let vc = storyboard?.instantiateViewController(withIdentifier: "PhotosVC") as! PhotosVC
//            vc.imagesIndex = indexPath.item
//            vc.imageArray = item.getImagesOffers()
//           vc.titleText = item.offers?[indexPath.row].name ?? ""
//            vc.priceLbl1 = item.offers?[indexPath.row].price ?? ""
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
    }

    
}
