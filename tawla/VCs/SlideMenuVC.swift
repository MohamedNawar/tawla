//
//  SlideMenuVC.swift
//  Mazen
//
//  Created by Moaz Ezz on 3/18/18.
//  Copyright © 2018 ElnoorOnline. All rights reserved.
//

import UIKit
import Font_Awesome_Swift


// User Declaration
var user = userData()

class SlideMenuVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var imagesArray = [#imageLiteral(resourceName: "img55"),#imageLiteral(resourceName: "img56"),#imageLiteral(resourceName: "img59"),#imageLiteral(resourceName: "img44"),#imageLiteral(resourceName: "img61"),#imageLiteral(resourceName: "img62"),#imageLiteral(resourceName: "img63"),#imageLiteral(resourceName: "img15"),#imageLiteral(resourceName: "img59"),#imageLiteral(resourceName: "img26"),#imageLiteral(resourceName: "img47"),#imageLiteral(resourceName: "img63")]
    var imagesArray1 = [#imageLiteral(resourceName: "img55"),#imageLiteral(resourceName: "img55"),#imageLiteral(resourceName: "img55"),#imageLiteral(resourceName: "img55"),#imageLiteral(resourceName: "img55"),#imageLiteral(resourceName: "img61"),#imageLiteral(resourceName: "img56"),#imageLiteral(resourceName: "img63")]
    var nameArraylogedInShopOwner = [MessagesManger.shared.myReservations,MessagesManger.shared.ReserveByRole,MessagesManger.shared.ReserveByTime,MessagesManger.shared.AddExceptionTime,MessagesManger.shared.Exception, MessagesManger.shared.ContactUs, MessagesManger.shared.AboutUs, MessagesManger.shared.signOut]
    var nameArraylogedInCustomer = [MessagesManger.shared.main, MessagesManger.shared.myReservations, MessagesManger.shared.fav, MessagesManger.shared.myProfile, MessagesManger.shared.ContactUs, MessagesManger.shared.AboutUs, MessagesManger.shared.signOut]
    var nameArrayNotlogedIn = [MessagesManger.shared.main, MessagesManger.shared.ContactUs, MessagesManger.shared.AboutUs, MessagesManger.shared.signIn]
    var MainMenu = [String]()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var footerView: UIView!

    @IBAction func goToMrNeu(_ sender: Any) {
        UIApplication.shared.open(URL(string: "http://www.elnooronline.com")!, options: [:], completionHandler: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = footerView
        MainMenu = nameArrayNotlogedIn
        if user.token != "" && user.token != nil{
            if user.data?.type == "customer"{
                MainMenu = nameArraylogedInCustomer
            }else{
                MainMenu = nameArraylogedInShopOwner
            }
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        user.fetchUser()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return MainMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SlideMenuCell")!
        let lbl = cell.viewWithTag(2) as! UILabel
        if user.data?.type == "customer" {
            let img = cell.viewWithTag(3) as! UIImageView
            img.image = imagesArray[indexPath.row]
            lbl.text = MainMenu[indexPath.row]
            
        }else{
            let img = cell.viewWithTag(3) as! UIImageView
            img.image = imagesArray1[indexPath.row]
            lbl.text = MainMenu[indexPath.row]
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch MainMenu[indexPath.row] {
        case MessagesManger.shared.main:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
            self.navigationController?.pushViewController(navVC, animated: true)
        case MessagesManger.shared.myProfile:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            self.navigationController?.pushViewController(navVC, animated: true)
        case "المحلات":
            print("1")
        case MessagesManger.shared.myReservations:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "MyReservationsVC2") as! MyReservationsVC2
            self.navigationController?.pushViewController(navVC, animated: true)
        case MessagesManger.shared.ReserveByRole:
                let navVC = storyboard?.instantiateViewController(withIdentifier: "MyReservationsVC2") as! MyReservationsVC2
                self.navigationController?.pushViewController(navVC, animated: true)
        case MessagesManger.shared.ReserveByTime:
                let navVC = storyboard?.instantiateViewController(withIdentifier: "MyReservationsVC2") as! MyReservationsVC2
                self.navigationController?.pushViewController(navVC, animated: true)
        case MessagesManger.shared.AddExceptionTime:
                let navVC = storyboard?.instantiateViewController(withIdentifier: "MyReservationsVC2") as! MyReservationsVC2
                self.navigationController?.pushViewController(navVC, animated: true)
        case MessagesManger.shared.Exception:
                let navVC = storyboard?.instantiateViewController(withIdentifier: "MyReservationsVC2") as! MyReservationsVC2
                self.navigationController?.pushViewController(navVC, animated: true)
        case MessagesManger.shared.fav:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "FavVC") as! FavVC
            self.navigationController?.pushViewController(navVC, animated: true)
        case MessagesManger.shared.ContactUs:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
            self.navigationController?.pushViewController(navVC, animated: true)
        case MessagesManger.shared.AboutUs:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "aboutVC") as! aboutVC
            self.navigationController?.pushViewController(navVC, animated: true)
        case MessagesManger.shared.signOut:
            user.remove()
            user.fetchUser()
            self.dismiss(animated: false, completion: nil)
            if let vc = storyboard?.instantiateViewController(withIdentifier: "rootnav") {
                UIApplication.shared.keyWindow?.rootViewController = vc
            }
        case MessagesManger.shared.signIn:
            print(user.token)
            if user.token == "" || user.token == nil {
                let VC = storyboard?.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
                VC.shouldBack = true
                let navVc = UINavigationController(rootViewController: VC)
                self.present(navVc, animated: true, completion: nil)
            }else{
                user.remove()
                let navVC = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
                self.navigationController?.pushViewController(navVC, animated: true)
            }
        case "تسجيل الخروج":
            print(user.token)
            if user.token == "" || user.token == nil {
                let navVC = storyboard?.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
                self.navigationController?.pushViewController(navVC, animated: true)
            }else{
                user.remove()
                let navVC = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
                self.navigationController?.pushViewController(navVC, animated: true)
            }
        case "ابحث عن محل":
            let navVC = storyboard?.instantiateViewController(withIdentifier: "MyNavVC") as! MyNavVC
            self.present(navVC, animated: true, completion: nil)
        case "اتصل بنا":
            let navVC = storyboard?.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
            self.navigationController?.pushViewController(navVC, animated: true)
        case "من نحن":
            let navVC = storyboard?.instantiateViewController(withIdentifier: "aboutVC") as! aboutVC
            self.navigationController?.pushViewController(navVC, animated: true)
        case MessagesManger.shared.rates:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "RattingViewController") as! RattingViewController
            self.present(navVC, animated: true, completion: nil)
        case "حجوزاتي":
            let navVC = storyboard?.instantiateViewController(withIdentifier: "MyReservationsVC2") as! MyReservationsVC2
            self.navigationController?.pushViewController(navVC, animated: true)
        default:
            print("1")
        }
        print(indexPath)
    }
    
    
    
    
    
    
}
