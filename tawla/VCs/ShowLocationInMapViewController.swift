//
//  ShowLocationInMapViewController.swift
//  examp2
//
//  Created by MACBOOK on 7/24/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import Alamofire
import PKHUD
import JGProgressHUD
class ShowLocationInMapViewController: UIViewController,CLLocationManagerDelegate, GMSMapViewDelegate {
    var isPlaceLoc = false
    @IBOutlet weak var GotoDirections: UIButton!
    let hud = JGProgressHUD(style: .light)
    var selectedCity = ""
    
    
    var selectedCityId = -1
    @IBOutlet weak var mapView: GMSMapView!
    var locationManager = CLLocationManager()
    var isViewer = true
    var shop = ResOrCafe()
    var shops1 = [ResOrCafe](){
        didSet{
            print(selectedCityId)
            shops.removeAll()
            if selectedCityId == -1 {
                shops = shops1
            }else{
                for shop in shops1 {
                    if shop.place_type?.id == selectedCityId {
                        shops.append(shop)
                        print(shop)

                    }
                }
            }
        }
    }
    var shops = [ResOrCafe]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView?.isMyLocationEnabled = true
        if isPlaceLoc {
            GotoDirections.isHidden = false
        }
        mapView.delegate = self
        if isViewer {
            let lat = Double(shop.latitude ?? "0.0") ?? 0.0
            print(lat)
            let long = Double(shop.longitude ?? "0.0") ?? 0.0
            print(long)
           
            //        view = mapView
            
            let currentLocation = CLLocationCoordinate2DMake(lat,long)
            print(currentLocation)
            let marker = GMSMarker(position: currentLocation)
            marker.title = shop.name
            marker.map = mapView
            if shop.place_type?.id == 1 {
                marker.icon = #imageLiteral(resourceName: "resta_map")
            }else if shop.place_type?.id == 2 {
                marker.icon = #imageLiteral(resourceName: "cafe_map")
            }
            marker.position = currentLocation
                let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 15)
            marker.isTappable = true
            self.mapView.camera = camera;
        }else{
            self.mapView.isMyLocationEnabled = true
            
            //Location Manager code to fetch current location
            self.locationManager.delegate = self
            self.locationManager.startUpdatingLocation()
            
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @IBAction func goToDirectionsPressed(_ sender: Any) {
        
        if let UrlNavigation = URL.init(string: "comgooglemaps://") {
            if UIApplication.shared.canOpenURL(UrlNavigation){
                if self.shop.longitude != nil && self.shop.latitude != nil {
                    
                    let lat =      (self.shop.latitude)!
                    let longi = (self.shop.longitude)!
                    
                    if let urlDestination = URL.init(string: "comgooglemaps://?saddr=&daddr=\(lat),\(longi)&directionsmode=driving") {
                        UIApplication.shared.openURL(urlDestination)
                    }
                }
            }
            else {
                NSLog("Can't use comgooglemaps://");
                self.openTrackerInBrowser()
                
            }
        }
        else
        {
            NSLog("Can't use comgooglemaps://");
            self.openTrackerInBrowser()
        }
    }
    
    
    func openTrackerInBrowser(){
        if self.shop.longitude != nil && self.shop.latitude != nil {
            
            let lat = (self.shop.latitude)!
            let longi = (self.shop.longitude)!
            
            if let urlDestination = URL.init(string: "https://www.google.co.in/maps/dir/?saddr=&daddr=\(lat),\(longi)&directionsmode=driving") {
                UIApplication.shared.openURL(urlDestination)
            }
        }
    }
    
    //Location Manager delegates
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        let lat = (location?.coordinate.latitude)!
        let long = (location?.coordinate.longitude)!
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 15)
        
        self.mapView.animate(to: camera)
        getPlacesWithFilter(lat: lat, long: long)
        //Finally stop updating location otherwise it will come again and again in this delegate
        self.locationManager.stopUpdatingLocation()
        
    }
    
    
    @IBAction func Done(_ sender: Any) {
        if isViewer {
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupNotations()  {
        for i in 0..<shops.count{
            
            
            let double_lat = Double(shops[i].latitude ?? "0") ?? 0.0
            let double_long = Double(shops[i].longitude ?? "0") ?? 0.0
            
            let mkr = GMSMarker()
            //            mkr.icon = [UIImage imageNamed:@"map_black"];
            if (double_lat != 0.0 && double_long != 0.0)
            {
                let postion = CLLocationCoordinate2D(latitude: double_lat, longitude: double_long)
                mkr.position = postion
                mkr.title = shops[i].name
                mkr.snippet = shops[i].city
                mkr.map = mapView
                if shops[i].place_type?.id == 1 {
                    mkr.icon = #imageLiteral(resourceName: "resta_map")
                }else if shops[i].place_type?.id == 2 {
                    mkr.icon = #imageLiteral(resourceName: "cafe_map")
                }
                
                let camera = GMSCameraPosition(target: postion, zoom: 12, bearing: 0.0, viewingAngle: 0.0)
                mkr.isTappable = true
                self.mapView.camera = camera;
            }
        }
    }
    
    
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        for i in shops{
            if i.name == marker.title{
                let navVC = storyboard?.instantiateViewController(withIdentifier: "ItemVC") as! ItemVC
                navVC.item = i
                self.navigationController?.pushViewController(navVC, animated: true)
            }
        }
    }
    
    
    
    
    
    private func getPlacesWithFilter(lat:Double,long:Double){
        
//        HUD.show(.progress, onView: self.view)
         hud.show(in: self.view)
        let header = APIs.sharedInstance.getHeader()
        
        
        
        let par = "&city_id=\(selectedCity)"
        Alamofire.request(APIs.sharedInstance.getPlaces(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
//                HUD.hide()
                self.hud.dismiss()
                print(value)
                if let temp = value as? Dictionary<String,AnyObject>{
                    if let temp2 = temp["data"]{
                        
                        
                        do {
                            let temp3 = try JSONSerialization.data(withJSONObject: temp2, options: .prettyPrinted)
                            self.shops1 = try JSONDecoder().decode([ResOrCafe].self, from: temp3 )
                            self.setupNotations()
                            print(self.shops)
                        }catch{
//                            HUD.hide()
                            self.hud.dismiss()
                            showError(text:MessagesManger.shared.tryAgainLater)
                        }
                    }
                }else{
                    showError(text:MessagesManger.shared.tryAgainLater)
                }
                
            case .failure(_):
//                HUD.hide()
                self.hud.dismiss()
                showError(text:MessagesManger.shared.tryAgainLater)
                
                break
                
            }
        }
        
    }
    
}
