//
//  TermsAndAgreementVC.swift
//  ZAZA
//
//  Created by Moaz Ezz on 4/17/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import JGProgressHUD
class TermsAndAgreementVC: UIViewController {
    let hud = JGProgressHUD(style: .light)

    @IBOutlet weak var text: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        getTerms()
        text.isEditable = false
        // Do any additional setup after loading the view.
    }

    @IBAction func done(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    private func getTerms(){
        
        
        let header = APIs.sharedInstance.getHeader()
//        HUD.show(.progress, onView: self.view)
         hud.show(in: self.view)
        Alamofire.request(APIs.sharedInstance.Settings(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
                
                
            case .success(let value):
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12"))
                        
                    }catch{
                        showError(text:MessagesManger.shared.checkYourData)
                    }
                }else {
                    if let temp = value as? Dictionary<String,AnyObject>{
                        if let temp2 = temp["data"]{
                            
                            
                            do {
                                let temp3 = try JSONSerialization.data(withJSONObject: temp2, options: .prettyPrinted)
                                let temp4 = try JSONDecoder().decode(contactUsModel.self, from: temp3 )
                                self.text.text = "\(temp4.data?.usage?.name ?? "") \n\n \(temp4.data?.usage?.description ?? "")"
                               
                            }catch{
//                                HUD.hide()
                                self.hud.dismiss()
                                showError(text:MessagesManger.shared.tryAgainLater)
                            }
                        }
                    }
                    
                }
//                HUD.hide()
                self.hud.dismiss()
            case .failure(let error):
//                HUD.hide()
                self.hud.dismiss()
                print(error)
                
            }
        }
        
    }
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        _ = alert.bounds
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: MessagesManger.shared.done, andButtons: nil)
    }
    

}

struct terms : Decodable {
    var data : terms2?
}
struct terms2 : Decodable {
    var content : String?
}
