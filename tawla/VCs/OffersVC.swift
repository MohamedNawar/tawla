//
//  OffersVC.swift
//  tawla
//
//  Created by Moaz Ezz on 8/11/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit
import Alamofire
import JGProgressHUD

class OffersVC: UIViewController {
    
    var offers = [Offer]()
    let hud = JGProgressHUD(style: .light)
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        getOffers()
        // Do any additional setup after loading the view.
    }
    
    
    private func getOffers(){
        
        hud.show(in: self.view)
        let header = APIs.sharedInstance.getHeader()
        
        Alamofire.request(APIs.sharedInstance.getOffers(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                self.hud.dismiss()
                print(value)
                if let temp = value as? Dictionary<String,AnyObject>{
                    if let temp2 = temp["data"]{
                        
                        
                        do {
                            let temp3 = try JSONSerialization.data(withJSONObject: temp2, options: .prettyPrinted)
                            self.offers = try JSONDecoder().decode([Offer].self, from: temp3 )
                            self.tableView.reloadData()
                        }catch{
                            self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                            self.hud.textLabel.text = MessagesManger.shared.tryAgainLater
                            self.hud.dismiss(afterDelay: 1.0)
                        }
                    }
                }
                
            case .failure(_):
                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                self.hud.textLabel.text = MessagesManger.shared.tryAgainLater
                self.hud.dismiss(afterDelay: 1.0)
                
                break
                
            }
        }
        
    }
}

extension OffersVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OfferCell", for: indexPath) as! OfferCell
        cell.price.text = offers[indexPath.row].price
        cell.price.textAlignment = .center
        cell.name.text = offers[indexPath.row].name
        cell.resName.text = offers[indexPath.row].price
        let url = URL(string: offers[indexPath.row].image ?? "")
        cell.img.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "img7_1"), options: .highPriority, completed: nil)
        cell.view.layer.cornerRadius = 33
        cell.price.textAlignment = .center

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "ItemVC") as! ItemVC
        navVC.item = offers[indexPath.row].place!
        self.navigationController?.pushViewController(navVC, animated: true)
    }
}


class OfferCell: UITableViewCell {
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var resName: UILabel!
    
    @IBOutlet weak var price: UILabel!
    
}
