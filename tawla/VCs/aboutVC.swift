
//
//  aboutVC.swift
//  men elbeit
//
//  Created by mouhammed ali on 12/23/17.
//  Copyright © 2017 mouhammed ali. All rights reserved.
//

import UIKit
import Alamofire
import Font_Awesome_Swift
import PKHUD
import FCAlertView
import JGProgressHUD


class aboutVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var aboutTable: UITableView!
    let hud = JGProgressHUD(style: .light)

    var consistenArr = [Int]()
    var stringArr = [String]()
    var contacttemp = contactUsModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        getContactUs()
    }
    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
////        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row % 2 == 1 {
            if consistenArr.contains(indexPath.row-1){
                return 0
            }
            
        }
        return  UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stringArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if indexPath.row % 2 == 0 {
             cell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! UITableViewCell
            let butt = cell.viewWithTag(1) as! btnInCell
            let img = cell.viewWithTag(2) as! UIImageView
            butt.indexPath = indexPath.row
            butt.addTarget(self, action: #selector(self.pressButton(_:)), for: .touchUpInside)
            butt.setTitle(stringArr[indexPath.row], for: .normal)
            img.setFAIconWithName(icon: .FAAngleDown, textColor: #colorLiteral(red: 0.4126763344, green: 0.4591623545, blue: 0.5150957704, alpha: 1))
            if consistenArr.contains(indexPath.row) {
                img.setFAIconWithName(icon: .FAAngleUp, textColor: #colorLiteral(red: 0.4126763344, green: 0.4591623545, blue: 0.5150957704, alpha: 1))
            }
            
        }
        else {
            
            cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UITableViewCell
            let txtView = cell.viewWithTag(1) as! UITextView
            txtView.text = stringArr[indexPath.row].replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
            
        }
        
        return cell
        
    }
    

    
    
    @objc func pressButton(_ sender: btnInCell){
        print("\(sender.indexPath)")
        if consistenArr.contains(sender.indexPath!) {
            consistenArr = consistenArr.filter { $0 != sender.indexPath! }
            
        }else{
            
            consistenArr.append(sender.indexPath!)
        }
        UIView.animate(withDuration: 0.5) {
            self.aboutTable.beginUpdates()
            self.aboutTable.endUpdates()
        }
        let when = DispatchTime.now() + 0.5
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.aboutTable.reloadData()
        }
        
        
    }
    
    
    
    
    func getContactUs(){
        let header = APIs.sharedInstance.getHeader()
        
//        HUD.show(.progress)
        hud.show(in: self.view)
        Alamofire.request(APIs.sharedInstance.Settings(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
//                HUD.hide()
                self.hud.dismiss()

                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: MessagesManger.shared.tryAgainLater, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12"))
                    }catch{
                        showError(text:MessagesManger.shared.tryAgainLater)
                    }
                }else {
                    do {
                        self.contacttemp = try JSONDecoder().decode(contactUsModel.self, from: response.data!)
                        self.stringArr.append(self.contacttemp.data?.about?.name ?? "")
                        self.stringArr.append(self.contacttemp.data?.about?.name ?? "")
                        self.stringArr.append(self.contacttemp.data?.about?.description ?? "")
                        self.stringArr.append(self.contacttemp.data?.about?.description ?? "")
                        self.aboutTable.reloadData()
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        print(error)
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                }
            case .failure(_):
//                HUD.hide()
                self.hud.dismiss()

                showError(text:MessagesManger.shared.tryAgainLater)
                break
            }
            
        }
    }
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        _ = alert.bounds
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: MessagesManger.shared.done, andButtons: nil)
    }
}

class btnInCell:UIButton {
    var indexPath:Int?
}
