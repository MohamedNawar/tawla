//
//  ReserveVC.swift
//  tawla
//
//  Created by Moaz Ezz on 7/22/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import Alamofire
import PKHUD
import FCAlertView
import JGProgressHUD
import LanguageManager_iOS
class ReserveVC: UIViewController , UITextViewDelegate {
    @IBOutlet weak var MainView: UIView!
    let hud = JGProgressHUD(style: .light)
    var txtDatePicker:UITextField?
    @IBOutlet weak var secView: UIView!
    @IBOutlet var tawlaType: [UIButton]!
    @IBOutlet var tawlaPlace: [UIButton]!
    
    
    
    
    @IBOutlet weak var dateLblqueue: UILabel!
    
    
    @IBOutlet weak var mainHeighCon: NSLayoutConstraint!
    
    @IBOutlet weak var notes: UITextView!
    
    @IBOutlet weak var notesQueue: UITextView!
    
    var selectedIndex = -1
    var SelectedItem = ResOrCafe()
    var reservstion = Reservation()
    var times = Times()
    var isTimeSelected = false
    
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    var timeArray = [String]()
    
    //Holders
    var dateHolder = ""
    var men_countHolder = 0{
        didSet{
            resetTimes()
            if men_countHolder < 0{
                men_countHolder = 0
            }
            manLbl.text = "\(men_countHolder)"
            manLbl2.text = "\(men_countHolder)"
        }
    }
    var women_countHolder = 0{
        didSet{
            resetTimes()
            if women_countHolder < 0{
                women_countHolder = 0
            }
            print(women_countHolder)
            womenLbl.text = "\(women_countHolder)"
            womenLbl2.text = "\(women_countHolder)"
        }
    }
    var children_countHolder = 0{
        didSet{
            resetTimes()
            if children_countHolder < 0{
                children_countHolder = 0
            }
            childLbl.text = "\(children_countHolder)"
            childLbl2.text = "\(children_countHolder)"
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty {
            if L102Language.currentAppleLanguage() == "ar" {
                textView.text = "إذا كان لديك طلب خاص أكتبة من فضلك"
                
            }else{
                textView.text = "Please Enter your Special requests ..."
                
            }
            textView.textColor = UIColor.lightGray
        }
    }
    var table_type_idHolder = Int()
    var has_barrierHolder = Bool()
    var positionHolder = ""
    var getharingHolder = Int()
    var start_atHolder = String()
    
    //    private func resetAll(){
    //        for i in tawlaPlace {
    //            i.backgroundColor = .white
    //        }
    //        for i in tawlaType {
    //            i.backgroundColor = .white
    //        }
    //
    //        table_type_idHolder = -1
    //        has_barrierHolder = Bool.init()
    //        positionHolder = ""
    //        getharingHolder = -1
    //        start_atHolder = ""
    //        times.schedules?.removeAll()
    //
    //        tableShapeCollectionView.reloadData()
    //        getheringTypeCollectionView.reloadData()
    //        children_countHolder = 0
    //        men_countHolder = 0
    //        women_countHolder = 0
    //    }
    
    @IBOutlet weak var timeCollectionView: UICollectionView!
    @IBOutlet weak var tableShapeCollectionView: UICollectionView!
    @IBOutlet weak var getheringTypeCollectionView: UICollectionView!
    
    @IBOutlet weak var childLbl: UILabel!
    @IBOutlet weak var childLbl2: UILabel!
    @IBAction func childMenus(_ sender: Any) {
        children_countHolder -= 1
    }
    
    @IBAction func childPlus(_ sender: Any) {
        children_countHolder += 1
    }
    
    @IBOutlet weak var manLbl: UILabel!
    @IBOutlet weak var manLbl2: UILabel!
    @IBAction func manMenus(_ sender: Any) {
        men_countHolder -= 1
    }
    
    @IBAction func manPlus(_ sender: Any) {
        men_countHolder += 1
    }
    
    @IBOutlet weak var womenLbl: UILabel!
    @IBOutlet weak var womenLbl2: UILabel!
    @IBAction func womenMenus(_ sender: Any) {
        women_countHolder -= 1
    }
    
    @IBAction func womenPlus(_ sender: Any) {
        women_countHolder += 1
        print("+1")
        
    }
    
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var queueNumLbl: UILabel!
    
    
    @IBOutlet weak var timeLblQueue: UILabel!
    @IBAction func queuePressed(_ sender: Any) {
        
        if user.token == nil || user.token == ""{
            makeReserveAlert(title: "", SubTitle: NSLocalizedString("You have to SignIn", comment: ""), Image: #imageLiteral(resourceName: "img39"))
            
        } else if !user.isVerified() {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MobileNavVC") as! UINavigationController
            self.present(vc, animated: true, completion: nil)
        } else {
            
            self.postReservationTime(date: self.dateHolder, men_count: self.men_countHolder, women_count: self.women_countHolder, children_count: self.children_countHolder)
        }
    }
    
    
    @IBAction func tawlaTypeBtn(_ sender: UIButton) {
        print(sender.tag)
        resetTimes()
        switch sender.tag {
        case 1:
            has_barrierHolder = false
        default:
            has_barrierHolder = true
        }
        for i in tawlaType {
            if sender.tag == i .tag{
                i.layer.borderWidth = 2
                i.layer.borderColor = mainColorGRAD.cgColor
            }else{
                i.layer.borderColor = UIColor.clear.cgColor
            }
        }
    }
    
    @IBAction func tawlaPlaceBtn(_ sender: UIButton) {
        print(sender.tag)
        //        inside, outside, anywhere
        resetTimes()
        switch sender.tag {
        case 1:
            positionHolder = "anywhere"
        case 2:
            positionHolder = "inside"
        default:
            positionHolder = "outside"
        }
        for i in tawlaPlace {
            if sender.tag == i .tag{
                i.layer.borderWidth = 2
                i.layer.borderColor = mainColorGRAD.cgColor
            }else{
                i.layer.borderColor = UIColor.clear.cgColor
            }
        }
    }
    
    
    func setupView(type:Int) {
        switch type {
        case 1://queue
            self.MainView.isHidden = true
            self.secView.isHidden = false
        case 2://time
            self.MainView.isHidden = false
            self.secView.isHidden = true
        default://All Hidden
            self.MainView.isHidden = true
            self.secView.isHidden = true
        }
    }
    
    @IBOutlet weak var summryBtn: UIButton!
    
    @IBAction func SummryPressed(_ sender: Any) {
        
        if isTimeSelected {
            if user.token == nil || user.token == ""{
                makeReserveAlert(title: "", SubTitle: NSLocalizedString("You have to SignIn", comment: ""), Image: #imageLiteral(resourceName: "img39"))
            } else if !user.isVerified() {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MobileNavVC") as! UINavigationController
                self.present(vc, animated: true, completion: nil)
            } else{
                postReservationTime(date: dateHolder, men_count: men_countHolder, women_count: women_countHolder, children_count: children_countHolder, table_type_id: table_type_idHolder, has_barrier: has_barrierHolder, position: positionHolder, gathering_types: getharingHolder, start_at: start_atHolder)
            }
        }else{
            getReservationTime(date: dateHolder, men_count: men_countHolder, women_count: women_countHolder, children_count: children_countHolder, table_type_id: table_type_idHolder, has_barrier: has_barrierHolder, position: positionHolder, gathering_types: getharingHolder)
        }
    }
    
    @IBOutlet weak var dateBtn: UIButton!
    @IBAction func datePressed(_ sender: Any) {
        self.setupView(type: 0)
        txtDatePicker?.becomeFirstResponder()

    }
    
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        if L102Language.currentAppleLanguage() == "ar" {
            notes.text = "إذا كان لديك طلب خاص أكتبة من فضلك"
            notesQueue.text =  "إذا كان لديك طلب خاص أكتبة من فضلك"
        }else{
            notes.text = "Please Enter your Special requests ..."
            notesQueue.text = "Please Enter your Special requests ..."
        }
        notes.textColor = UIColor.lightGray
        notes.delegate = self
        notesQueue.textColor = UIColor.lightGray
        notesQueue.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dateBtn.setTitle(self.dateHolder, for: .normal)
        txtDatePicker = UITextField(frame: CGRect(x: -5, y: -100, width: 0, height: 0) )
        self.view.addSubview(txtDatePicker ?? UIView())
        showDatePicker()
        goToNext()
        // Do any additional setup after loading the view.
        if L102Language.currentAppleLanguage() == "ar" {
            notes.text = "إذا كان لديك طلب خاص أكتبة من فضلك"
            notesQueue.text =  "إذا كان لديك طلب خاص أكتبة من فضلك"
        }else{
            notes.text = "Please Enter your Special requests ..."
            notesQueue.text = "Please Enter your Special requests ..."
        }
        notes.textColor = UIColor.lightGray
        notes.delegate = self
        notesQueue.textColor = UIColor.lightGray
        notesQueue.delegate = self
        if LanguageManager.shared.currentLanguage == .ar {
            notes.textAlignment = .right
            notesQueue.textAlignment = .right
        }else{
            notes.textAlignment = .left
            notesQueue.textAlignment = .left
        }
        }
        
    
    
    private func getReservation(date:String){
        let header = APIs.sharedInstance.getHeader()
        let par = ["date":date]
//        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.sharedInstance.getReservationType(id: SelectedItem.id ?? -1), method: .get, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
                
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
//                        HUD.hide()
                        self.hud.dismiss()
                        self.makeDoneAlert(title: MessagesManger.shared.addToFav, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12"))
                        
                        
                    }catch{
                        showSucces(text:MessagesManger.shared.addToFav)
                    }
                    
                }else{
                    do {
                        self.reservstion = try JSONDecoder().decode(Reservation.self, from: response.data! )
                        print("done")
                        self.goToNext()
//                        HUD.hide()
                        self.hud.dismiss()
                        
                        //                                self.setupUpUI()
                    }catch{
                        
                        showError(text:MessagesManger.shared.tryAgainLater)
                    }
                }
                
                
            case .failure(_):
//                HUD.hide()
                self.hud.dismiss()
                showError(text:MessagesManger.shared.tryAgainLater)
                break
            }
            
        }
    }
    
    private func getDate(date:Date,flag:Bool) -> String{//return date as formated -> Y-m-d 11-15-2017
        
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        let second = calendar.component(.second, from: date)
        return flag ? "\(year)-\(String(format: "%02d", month))-\(String(format: "%02d", day))" : "\(String(format: "%02d", hour)):\(String(format: "%02d", minute)):\(String(format: "%02d", second))"
    }
    
    private func goToNext() {
        self.timeCollectionView.reloadData()
        self.tableShapeCollectionView.reloadData()
        self.getheringTypeCollectionView.reloadData()
        if reservstion.mechanism == "time" {
            self.setupView(type: 2)
        }else if reservstion.mechanism == "queue" {
            //            self.makeSpecificAlert2(title: "الحجز المتوقع", SubTitle: " \(dateHolder) \n \(reservstion.schedules?.formated_start_at ?? "") ", Image: #imageLiteral(resourceName: "img18"))
            //            self.queueLbl.text = "الحجز المتوقع\n  \(dateHolder) \n \(reservstion.schedules?.formated_start_at ?? "") \n  الدور : \(reservstion.number ?? 0)"
            
            self.queueNumLbl.text = MessagesManger.shared.queue + "\(reservstion.number ?? -1)"
            self.dateLblqueue.text = dateHolder
            self.timeLblQueue.text = reservstion.schedules?.formated_start_at
            self.setupView(type: 1)
        }else{
            self.makeDoneAlert(title: MessagesManger.shared.sorry, SubTitle: MessagesManger.shared.dayNotAvaliable , Image: #imageLiteral(resourceName: "img18"))
            self.setupView(type: 0)
        }
    }
    
    func makeSpecificAlert2(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        alert.titleFont = BoldFont
        alert.subtitleFont = NormalFont
        alert.doneButtonCustomFont = NormalFont
        alert.firstButtonCustomFont = NormalFont
        alert.doneButtonTitleColor = .black
        alert.firstButtonTitleColor = .black
        alert.addButton(MessagesManger.shared.reserve) {
            //            print("ToDo -> goTo menu")
            if user.token == nil || user.token == ""{
                let navVC = self.storyboard?.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
                navVC.shouldBack = true
                self.navigationController?.pushViewController(navVC, animated: true)
            } else{
                self.postReservationTime(date: self.dateHolder, men_count: self.men_countHolder, women_count: self.women_countHolder, children_count: self.children_countHolder)
            }
        }
        alert.doneBlock = {
            self.setupView(type: 0)
        }
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: MessagesManger.shared.chosseAnotherTime, andButtons: nil)
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        alert.titleFont = BoldFont
        alert.subtitleFont = NormalFont
        alert.doneButtonCustomFont = NormalFont
        alert.doneButtonHighlightedBackgroundColor = secondaryColor
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: MessagesManger.shared.done, andButtons: nil)
    }
    
    
    private func getReservationTime(date:String,men_count:Int,women_count:Int,children_count:Int,table_type_id:Int,has_barrier:Bool,position:String,gathering_types:Int){
        guard  gathering_types != 0 && gathering_types != -1  else {
            showError(text: MessagesManger.shared.gatheringType)
            return
        }
        guard  table_type_id != 0 && table_type_id != -1 else {
            showError(text: MessagesManger.shared.position)
            return
        }
        guard position != "" else {
            showError(text: MessagesManger.shared.tableType2)
            return
        }
        guard  men_count != 0 || women_count != 0 || children_count != 0  else {
            showError(text: MessagesManger.shared.chooseMembers)
            return
        }
//        guard  has_barrier != true && has_barrier != false
//            else {

//            return
//        }
        let header = APIs.sharedInstance.getHeader()
        let par = ["date":date,
                   "men_count":men_count,
                   "women_count":women_count,
                   "children_count":children_count,
                   "table_type_id":table_type_id,
                   "has_barrier":has_barrier,
                   "position":position,
                   "gathering_type_id":gathering_types] as [String : Any]
//        HUD.show(.progress)
        hud.show(in: self.view)
        Alamofire.request(APIs.sharedInstance.getReservationTime(id: SelectedItem.id ?? -1), method: .get, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
//                HUD.hide()
                self.hud.dismiss()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12"))
                        
                    }catch{
                        showError(text:MessagesManger.shared.checkYourData)
                    }
                }else{
                    do {
                        self.times = try JSONDecoder().decode(Times.self, from: response.data! )
                        print("done")
                        if self.times.schedules?.count == 0{
                            self.makeDoneAlert(title:MessagesManger.shared.noAvaliableTimes , SubTitle:   MessagesManger.shared.chosseADiffirantDay, Image: #imageLiteral(resourceName: "img18"))
                        }
                        self.timeCollectionView.reloadData()
                        //                                self.setupUpUI()
                    }catch{
//                        HUD.hide()
                        self.hud.dismiss()
                        showError(text:MessagesManger.shared.tryAgainLater)
                    }
                }
                
                
            case .failure(_):
//                HUD.hide()
                self.hud.dismiss()
                showError(text:MessagesManger.shared.tryAgainLater)
                break
            }
            
        }
    }
    
    private func resetTimes(){
        self.isTimeSelected = false
        self.summryBtn.setTitle(MessagesManger.shared.showTimes, for: .normal)
        self.times.schedules?.removeAll()
        self.timeCollectionView.reloadData()
    }
    
    private func resetView(){
        getharingHolder = -1
        table_type_idHolder = -1
        has_barrierHolder = Bool()
        positionHolder = ""
        for i in tawlaPlace {
            i.layer.borderColor = UIColor.clear.cgColor
        }
        for i in tawlaType {
            i.layer.borderColor = UIColor.clear.cgColor
        }
        
        resetTimes()
    }
    
    
    
    private func postReservationTime(date:String,men_count:Int,women_count:Int,children_count:Int){
        
        
        let header = APIs.sharedInstance.getHeader()
        let par = [
            "date":date,
            "men_count":men_count,
            "women_count":women_count,
            "children_count":children_count,
            "notes":notesQueue.text ?? "",
            ] as [String : Any]
//        HUD.show(.progress)
        hud.show(in: self.view)
        Alamofire.request(APIs.sharedInstance.postReservationQueue(id: SelectedItem.id ?? -1), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
//                HUD.hide()
                self.hud.dismiss()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12"))
                        
                    }catch{
                        showError(text:MessagesManger.shared.checkYourData)
                    }
                }else{
                    if let temp = value as? Dictionary<String,AnyObject>{
                        if let temp2 = temp["data"]{
                            
                            
                            do {
                                let temp3 = try JSONSerialization.data(withJSONObject: temp2, options: .prettyPrinted)
                                self.reservstion = try JSONDecoder().decode(Reservation.self, from: temp3 )
                                user.removeReserveId()
                                user.reserveId =  self.reservstion.id ?? 0
                                UserDefaults.standard.set(self.reservstion.id ?? 0, forKey: "reserveId")
                                self.makeSpecificAlert(title: MessagesManger.shared.reserveSucsse, SubTitle: MessagesManger.shared.chosseFromFollowing, Image:  #imageLiteral(resourceName: "img11"))
                                
                            }catch{
//                                HUD.hide()
                                self.hud.dismiss()
                                showError(text:MessagesManger.shared.tryAgainLater)
                            }
                        }
                    }
                }
                
                
            case .failure(_):
//                HUD.hide()
                self.hud.dismiss()
                showError(text:MessagesManger.shared.tryAgainLater)
                break
            }
            
        }
    }
    private func postReservationTime(date:String,men_count:Int,women_count:Int,children_count:Int,table_type_id:Int,has_barrier:Bool,position:String,gathering_types:Int,start_at:String){
        
        guard position != "" && gathering_types != 0 && gathering_types != -1 && table_type_id != 0 && table_type_id != -1 else {
            showError(text: MessagesManger.shared.emptyField)
            return
        }
//        guard  women_count == 0 && children_count == 0 && men_count == 0  else {

//            return
//        }
        let header = APIs.sharedInstance.getHeader()
        let par = ["date":date,
                   "men_count":men_count,
                   "women_count":women_count,
                   "children_count":children_count,
                   "table_type_id":table_type_id,
                   "has_barrier":has_barrier,
                   "position":position,
                   "notes":notes.text ?? "",
                   "gathering_type_id":gathering_types,
                   "start_at":start_at] as [String : Any]
//        HUD.show(.progress)
        hud.show(in: self.view)
        Alamofire.request(APIs.sharedInstance.postReservationTime(id: SelectedItem.id ?? -1), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
//                HUD.hide()
                self.hud.dismiss()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12"))
                        
                    }catch{
                        showError(text:MessagesManger.shared.checkYourData)
                    }
                }else{
                    if let temp = value as? Dictionary<String,AnyObject>{
                        if let temp2 = temp["data"]{
                            
                            
                            do {
                                let temp3 = try JSONSerialization.data(withJSONObject: temp2, options: .prettyPrinted)
                                self.reservstion = try JSONDecoder().decode(Reservation.self, from: temp3 )
                                 user.removeReserveId()
                                user.reserveId =  self.reservstion.id ?? 0
                                UserDefaults.standard.set(self.reservstion.id ?? 0, forKey: "reserveId")
                                self.makeSpecificAlert(title: MessagesManger.shared.reserveSucsse, SubTitle: MessagesManger.shared.chosseFromFollowing, Image:  #imageLiteral(resourceName: "img11"))
                                self.resetTimes()
                            }catch{
//                                HUD.hide()
                                self.hud.dismiss()
                                showError(text:MessagesManger.shared.tryAgainLater)
                            }
                        }
                    }
                }
                
                
            case .failure(_):
//                HUD.hide()
                self.hud.dismiss()
                showError(text:MessagesManger.shared.tryAgainLater)
                break
            }
            
        }
    }
    
    
    
    func makeSpecificAlert(title: String, SubTitle: String, Image : UIImage) {
        //        let alert = FCAlertView()
        //        alert.avoidCustomImageTint = true
        //        alert.titleFont = BoldFont
        //        alert.subtitleFont = NormalFont
        //        alert.doneButtonCustomFont = NormalFont
        //        alert.firstButtonCustomFont = NormalFont
        //        alert.doneButtonTitleColor = .black
        //        alert.firstButtonTitleColor = .black
        //        alert.addButton("اختيار المنيو الان") {
        ////            print("ToDo -> goTo menu")
        //            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
        //            vc.reservation = self.reservstion;
        //            vc.SelectedItem = self.SelectedItem; self.navigationController?.pushViewController(vc, animated: true)
        //        }
        //        alert.doneBlock = {
        //            self.dismiss(animated: true, completion: nil)
        //        }
        //        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "اختيار المنيو لاحقا", andButtons: nil)
        //    }
        let vc = storyboard?.instantiateViewController(withIdentifier: "DoneVC") as! DoneVC
        print(self.reservstion)
        vc.reservstion = self.reservstion;
        vc.SelectedItem = self.SelectedItem;
        vc.navigationItem.setHidesBackButton(true, animated:true)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    let datePicker = UIDatePicker()
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        datePicker.locale = Locale(identifier: LanguageManager.shared.currentLanguage.rawValue)
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: MessagesManger.shared.done, style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: ""), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        txtDatePicker?.inputAccessoryView = toolbar
        txtDatePicker?.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        
        self.dateHolder = self.getDate(date: datePicker.date , flag: true)
        self.resetView()
        self.dateBtn.setTitle(self.dateHolder, for: .normal)
        self.getReservation(date: self.dateHolder)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }

    
}


extension ReserveVC : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView {
        case tableShapeCollectionView:
            print(reservstion.table_types?.count)
            return reservstion.table_types?.count ?? 0
        case getheringTypeCollectionView:
            print(reservstion.gathering_types?.count)
            return reservstion.gathering_types?.count ?? 0
        case timeCollectionView:
            //            reservstion.schedules?.count ??
            return  times.schedules?.count ?? 0
        default:
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView{
        case tableShapeCollectionView:
            let cell = tableShapeCollectionView.dequeueReusableCell(withReuseIdentifier: "TableShapeCell", for: indexPath)
            let photo = cell.viewWithTag(1) as? UIImageView
            let lbl = cell.viewWithTag(2) as? UILabel
            lbl?.text = reservstion.table_types?[indexPath.row].name
            let url = URL(string: reservstion.table_types?[indexPath.row].image ?? "")
            photo?.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "img02"), options: .highPriority, completed: nil)
            if table_type_idHolder == reservstion.table_types?[indexPath.row].id {
                cell.layer.borderColor = mainColorGRAD.cgColor
                cell.layer.borderWidth = 2
            }else{
                cell.layer.borderColor = UIColor.clear.cgColor
            }
            return cell
        case getheringTypeCollectionView:
            let cell = getheringTypeCollectionView.dequeueReusableCell(withReuseIdentifier: "GetheringCell", for: indexPath)
            let photo = cell.viewWithTag(1) as? UIImageView
            let lbl = cell.viewWithTag(2) as? UILabel
            lbl?.text = reservstion.gathering_types?[indexPath.row].name
            let url = URL(string: reservstion.gathering_types?[indexPath.row].image ?? "")
            photo?.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "img02"), options: .highPriority, completed: nil)
            if getharingHolder == reservstion.gathering_types?[indexPath.row].id {
                cell.layer.borderColor = mainColorGRAD.cgColor
                cell.layer.borderWidth = 2
            }else{
                cell.layer.borderColor = UIColor.clear.cgColor
            }
            return cell
        case timeCollectionView:
            let cell = timeCollectionView.dequeueReusableCell(withReuseIdentifier: "TimeCell", for: indexPath)
            let lbl = cell.viewWithTag(1) as! UILabel
            lbl.text = times.schedules?[indexPath.row].start_at
            if indexPath.row == selectedIndex {
                lbl.layer.borderColor = secondaryColorGRAD.cgColor
                lbl.layer.borderWidth = 1
                lbl.textColor = secondaryColorGRAD
            } else {
                // change color back to whatever it was
                lbl.layer.borderColor = UIColor.clear.cgColor
                lbl.textColor = mainColor
            }
            print(indexPath.row)
            
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case  tableShapeCollectionView:
            if let cell = tableShapeCollectionView.cellForItem(at: indexPath){
                cell.layer.borderColor = mainColorGRAD.cgColor
                cell.layer.borderWidth = 2
                table_type_idHolder = reservstion.table_types?[indexPath.row].id ?? -1
                resetTimes()
            }
        case  getheringTypeCollectionView:
            if let cell = getheringTypeCollectionView.cellForItem(at: indexPath){
                cell.layer.borderColor = mainColorGRAD.cgColor
                cell.layer.borderWidth = 2
                getharingHolder = reservstion.gathering_types?[indexPath.row].id ?? -1
                resetTimes()
            }
        case  timeCollectionView:
            if let cell = timeCollectionView.cellForItem(at: indexPath){
                let lbl = cell.viewWithTag(1) as! UILabel
                lbl.layer.borderColor = secondaryColorGRAD.cgColor
                lbl.textColor = secondaryColorGRAD
                lbl.layer.borderWidth = 1
                start_atHolder = times.schedules?[indexPath.row].start_at ?? ""
                self.summryBtn.setTitle(MessagesManger.shared.reserveNow, for: .normal)
                self.isTimeSelected = true
            }
        default:
            print("something weried happed")
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        switch collectionView {
        case tableShapeCollectionView:
            if let cell = tableShapeCollectionView.cellForItem(at: indexPath){
                cell.layer.borderColor = UIColor.clear.cgColor
            }
        case getheringTypeCollectionView:
            if let cell = getheringTypeCollectionView.cellForItem(at: indexPath){
                cell.layer.borderColor = UIColor.clear.cgColor
            }
        case timeCollectionView:
            if let cell = timeCollectionView.cellForItem(at: indexPath){
                let lbl = cell.viewWithTag(1) as! UILabel
                lbl.layer.borderColor = UIColor.clear.cgColor
                lbl.textColor = mainColor
            }
        default:
            print("hi take a look ")
        }
    }
    func makeReserveAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.9101889729, green: 0.7538033128, blue: 0.2233724296, alpha: 1)
        alert.addButton(NSLocalizedString("Sign In", comment: "")) {
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
            VC.shouldBack = true
            let navVC = UINavigationController(rootViewController: VC)
            self.present(navVC, animated: true, completion: nil)
        }
        
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Cancel", comment: ""), andButtons:nil)
    }
    
}

