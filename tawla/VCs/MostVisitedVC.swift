//
//  MostVisitedVC.swift
//  tawla
//
//  Created by Moaz Ezz on 7/17/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit
import Alamofire
import JGProgressHUD

class MostVisitedVC: UIViewController {
    
    let hud = JGProgressHUD(style: .light)
    var search = false
    var name = String()
    var loaded = false
    var place_type_id = Int()
    var gathering_types = Int()
    var service_types = Int()
    var most_visited = Int()
    var selectedCity = -1

    
    var isMostVisited = false
    var resOrCafe = [ResOrCafe]()
    var imagesArray = [#imageLiteral(resourceName: "img55"),#imageLiteral(resourceName: "img56"),#imageLiteral(resourceName: "img59"),#imageLiteral(resourceName: "img44"),#imageLiteral(resourceName: "img61"),#imageLiteral(resourceName: "img62"),#imageLiteral(resourceName: "img30"),#imageLiteral(resourceName: "img15"),#imageLiteral(resourceName: "img59"),#imageLiteral(resourceName: "img26"),#imageLiteral(resourceName: "img47"),#imageLiteral(resourceName: "img63")]
    var nameArray = ["hello","hello","hello","hello","hello","hello","hello","hello","hello","hello","hello","hello"]
    @IBOutlet weak var tableView: UITableView!
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if isMostVisited{
            //Should get the data from server
            most_visited = 1000
            getPlacesWithFilter()
        }else {
            getPlacesWithFilter()
        }
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }

    private func getPlacesWithFilter(){
//        hud.show(in: self.view)
//        HUD.show(.progress, onView: self.view)
        let header = APIs.sharedInstance.getHeader()
        
        
        var par = "?"
        
        if name != ""{
            par = par + "name=\(name.fixedArabicURL ?? "")&"
        }
        if selectedCity != -1{
            par = par + "city_id=\(selectedCity)&"
        }else if let temp = UserDefaults.standard.string(forKey: "SelectedLoc") {
            par = par + "city_id=\(temp)&"
        }
    
        if place_type_id != -1 && place_type_id != 0{
            par = par + "place_type_id=\(place_type_id)&"
        }
        if gathering_types != -1 && gathering_types != 0{
            par = par + "gathering_types=\(gathering_types)&"
        }
        print(service_types)
        if service_types != -1 && service_types != 0{
            par = par + "service_types=\(service_types)&"
        }
        if most_visited != -1 && most_visited != 0{
            par = par + "most_visited=true"
        }
        
        print(APIs.sharedInstance.getPlaces() + par)
        let url = APIs.sharedInstance.getPlaces() + par
         print(url)

        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            self.loaded = true
            
            
            switch(response.result) {
            case .success(let value):
//                HUD.hide()
                self.hud.dismiss()
                print(value)
                if let temp = value as? Dictionary<String,AnyObject>{
                    if let temp2 = temp["data"]{
                        
                        
                        do {
                            print(temp2)
                            let temp3 = try JSONSerialization.data(withJSONObject: temp2, options: .prettyPrinted)
                            print("---------")
                            print(temp3)
                            self.pares(temp: temp3)
                        }catch{
                            self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                            self.hud.textLabel.text = MessagesManger.shared.tryAgainLater
                            self.hud.dismiss(afterDelay: 1.0)
                            
                        }
                    }
                }
                
            case .failure(_):
                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                self.hud.textLabel.text = MessagesManger.shared.tryAgainLater
                self.hud.dismiss(afterDelay: 1.0)
                self.tableView.reloadData()
                break
                
            }
        }
        
    }
    
    private func pares(temp : Data) {
        do {
            self.resOrCafe = try JSONDecoder().decode([ResOrCafe].self, from: temp )
            self.tableView.reloadData()
            
        } catch  {
            self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
            self.hud.textLabel.text = MessagesManger.shared.tryAgainLater
            self.hud.dismiss(afterDelay: 1.0)
        }
    }
}


extension MostVisitedVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if resOrCafe.count > 0 {
            return resOrCafe.count
        } else if loaded && search{
            tableView.setEmptyMessage(NSLocalizedString("No Results", comment: ""))
            return 0
        }
        return resOrCafe.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! ItemCell
        cell.titleLbl.text = resOrCafe[indexPath.row].name ?? ""
        cell.rate.value = CGFloat((resOrCafe[indexPath.row].rate as NSString?)?.floatValue ?? 0.0)
        cell.timLbl.text = resOrCafe[indexPath.row].working_schedule ?? "----"
        cell.locationLbl.text = resOrCafe[indexPath.row].address ?? "----"
        let url = URL(string: resOrCafe[indexPath.row].image ?? "")
        cell.mainImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "img01"), options: .highPriority, completed: nil)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("todo -> go to the shop page")
                let navVC = storyboard?.instantiateViewController(withIdentifier: "ItemVC") as! ItemVC
            navVC.item = resOrCafe[indexPath.row]
                self.navigationController?.pushViewController(navVC, animated: true)
    }
    
    
}

extension String {
    func utf8EncodedString()-> String {
        let messageData = self.data(using: .nonLossyASCII)
        let text = String(data: messageData!, encoding: .utf16)
        return text ?? ""
    }
    func utf8DecodedString()-> String {
        let data = self.data(using: .utf8)
        if let message = String(data: data!, encoding: .nonLossyASCII){
            return message
        }
        return ""
    }
}
extension String {
    var fixedArabicURL: String?  {
        return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics
            .union(CharacterSet.urlPathAllowed)
            .union(CharacterSet.urlHostAllowed))
    } }
