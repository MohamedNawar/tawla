//
//  ContactUsVCViewController.swift
//  Rafeeq2
//
//  Created by Moaz Ezz on 9/24/17.
//  Copyright © 2017 Moaz Ezz. All rights reserved.
//

import UIKit
import FCAlertView
import Alamofire
import PKHUD
import Spring
import JGProgressHUD

class ContactUsVC: UIViewController  {

    let hud = JGProgressHUD(style: .light)

    // this the variable that holdes the attachments
    var contacttemp = contactUsModel()
    
   
    
    
    @IBAction func callUs(_ sender: Any) {
        if let mobile = contacttemp.data?.phone {
            guard let number = URL(string: "tel://" + mobile) else { return }
            UIApplication.shared.open(number)
        }
    }
    @IBAction func callUs2(_ sender: Any) {
        if let mobile = contacttemp.data?.phone {
            guard let number = URL(string: "tel://" + mobile) else { return }
            UIApplication.shared.open(number)
        }
    }
    
    @IBAction func webSite(_ sender: Any) {
        if let url = NSURL(string: "http://\(contacttemp.data?.website ?? "")"){
            UIApplication.shared.openURL(url as URL)
        }

    }
    @IBAction func mailUs(_ sender: Any) {
        let email = contacttemp.data?.email ?? ""
        if let url = URL(string: "mailto:\(email)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    @IBOutlet weak var desciption: UITextView!
    
    @IBOutlet weak var phone: UILabel!
    
    @IBOutlet weak var phone2: UILabel!
    
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var site: UILabel!
    
    private func setupUpUI(){
        phone.text = contacttemp.data?.mobile ?? ""
        phone2.text = contacttemp.data?.phone ?? ""
        email.text = contacttemp.data?.email ?? ""
        site.text = contacttemp.data?.website ?? ""
    }
    
    
    @IBAction func youtubePressed(_ sender: Any) {
        guard let url = URL(string: contacttemp.data?.youtube ?? "") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func instgramPressed(_ sender: Any) {
        if let url = NSURL(string: "http://\(contacttemp.data?.instagram  ?? "")"){
            UIApplication.shared.openURL(url as URL)
        }
    }
    
    @IBAction func faceBookPressed(_ sender: Any) {
        if let url = NSURL(string: "http://\(contacttemp.data?.twitter ?? "")"){
            UIApplication.shared.openURL(url as URL)
        }
    }
    
    @IBAction func sendMessage(_ sender: Any) {
        postMessage()
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        setupUpUI()
        getContactUs()
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    

    
    
    func phone(phoneNum: String) {
        if let url = URL(string: "tel://\(phoneNum)") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url as URL)
            }
        }
    }
    
    
    
    
    
    func postMessage(){
        
        if desciption.text!.isEmpty == false{
            let header = APIs.sharedInstance.getHeader()
            let par = ["message": desciption.text!]
//            HUD.show(.progress)
            hud.show(in: self.view)

            Alamofire.request(APIs.sharedInstance.contactUs(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
                switch(response.result) {
                case .success(let value):
                    print(response.value)
                    print(response.result.error)
                    print(response.response?.statusCode)
//                    HUD.hide()
                    self.hud.dismiss()

                    print(value)
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                        
                        do {
                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                            self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12"))
                            
                        }catch{
                            showError(text:MessagesManger.shared.checkYourData)
                        }
                    }else{
                        self.desciption.text = ""
                        showSucces(text: NSLocalizedString("Your message was sent successfully", comment: ""))
                    }
                case .failure(_):
//                    HUD.hide()
                    self.hud.dismiss()

                    showError(text:MessagesManger.shared.tryAgainLater)
                    break
                }
        
            }
        }
    }
    
    func getContactUs(){
        let header = APIs.sharedInstance.getHeader()
        
//        HUD.show(.progress)
        hud.show(in: self.view)
        Alamofire.request(APIs.sharedInstance.Settings(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
//                HUD.hide()
                self.hud.dismiss()

                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12"))
                    }catch{
                        showError(text:MessagesManger.shared.checkYourData)
                    }
                }else{
                    do {
                        self.contacttemp = try JSONDecoder().decode(contactUsModel.self, from: response.data!)
                        self.setupUpUI()
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        print(error)
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                }
            case .failure(_):
//                HUD.hide()
                self.hud.dismiss()

                showError(text:MessagesManger.shared.tryAgainLater)
                break
            }
            
        }
    }
    
    
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: MessagesManger.shared.done, andButtons: nil)
    }
    
    
    
    
}
