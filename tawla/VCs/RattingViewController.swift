//
//  RattingViewController.swift
//  examp2
//
//  Created by MACBOOK on 7/24/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import HCSStarRatingView
import Alamofire
import PKHUD
import FCAlertView
import JGProgressHUD
import LanguageManager_iOS
class RattingViewController: UIViewController {
    var shop = ResOrCafe()
    
    var FinishPressed : (()->(Void))?
    let hud = JGProgressHUD(style: .light)

    @IBOutlet weak var comment: UITextView!
    
    @IBOutlet weak var rate: HCSStarRatingView!
    override func viewDidLoad() {
        super.viewDidLoad()
        comment.layer.cornerRadius = 5
        comment.layer.borderColor = UIColor.gray.cgColor
        comment.layer.borderWidth = 1
        print(shop)
        if LanguageManager.shared.currentLanguage == .ar {
            comment.textAlignment = .right
        }else{
            comment.textAlignment = .left
            
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        user.fetchUser()
    }
    @IBAction func post(_ sender: Any) {
        
        if user.token == nil || user.token == ""{
            makeReserveAlert(title: "", SubTitle: NSLocalizedString("You have to SignIn", comment: ""), Image: #imageLiteral(resourceName: "img39"))
            
        }else{
        postRate()
        }
    }
    private func postRate(){
        let header = APIs.sharedInstance.getHeader()
        let par = ["value": Int(rate.value),
                   "comment": comment.text!] as [String : Any]
        print(par)
        
//        HUD.show(.progress, onView: self.view)
         hud.show(in: self.view)
        print(APIs.sharedInstance.postRate(id: shop.id ?? -1))
        print(header)
        Alamofire.request(APIs.sharedInstance.postRate(id: shop.id ?? -1), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
//                HUD.hide()
                self.hud.dismiss()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(NSLocalizedString("you rate it before", comment: ""))", Image: #imageLiteral(resourceName: "img12") ,color: AlertColor)
                        //                            HUD.flash(.labeledError(title: "", subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                        //                            user.saveUser(user: user)
                    }catch{
                        showError(text:MessagesManger.shared.checkYourData)
                    }
                }else{
                    self.makeDoneAlert(title: MessagesManger.shared.thanks, SubTitle: "", Image: #imageLiteral(resourceName: "img12") ,color: AlertColor)
                    self.FinishPressed?()
                    self.dismiss(animated: true, completion: nil)
                }
            case .failure(_):
//                HUD.hide()
                self.hud.dismiss()
                showError(text:MessagesManger.shared.tryAgainLater)
                break
            }
        }
    }

    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage, color : UIColor) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        alert.colorScheme = mainColor
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: MessagesManger.shared.done, andButtons: nil)
    }
    func makeReserveAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.9101889729, green: 0.7538033128, blue: 0.2233724296, alpha: 1)
        alert.addButton(NSLocalizedString("Sign In", comment: "")) {
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
            VC.shouldBack = true
            let navVC = UINavigationController(rootViewController: VC)
            self.present(navVC, animated: true, completion: nil)
        }
        
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Cancel", comment: ""), andButtons:nil)
    }
}
