//
//  FavVC.swift
//  tawla
//
//  Created by Moaz Ezz on 7/19/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import JGProgressHUD
class FavVC: UIViewController {
    var resOrCafe = [ResOrCafe]()
    @IBOutlet weak var tableView: UITableView!
    let hud = JGProgressHUD(style: .light)

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        getFav()
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        
    }
    
    
    
    private func getFav(){
        
        
        let header = APIs.sharedInstance.getHeader()
        
//        HUD.show(.progress, onView: self.view)
        //        let url = APIs.sharedInstance.postMenu(id: reservation.id ?? -1);
        Alamofire.request(APIs.sharedInstance.getFav(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
//                HUD.hide()
                self.hud.dismiss()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12"))
                        
                    }catch{
                        showError(text:MessagesManger.shared.checkYourData)
                    }
                }else{
                    if let temp = value as? Dictionary<String,AnyObject>{
                        if let temp2 = temp["data"]{
                            
                            
                            do {
                                let temp3 = try JSONSerialization.data(withJSONObject: temp2, options: .prettyPrinted)
                                self.resOrCafe = try JSONDecoder().decode([ResOrCafe].self, from: temp3 )
                                if self.resOrCafe.count == 0 {
                                    self.makeDoneAlert(title: MessagesManger.shared.sorry, SubTitle: MessagesManger.shared.noFav, Image: #imageLiteral(resourceName: "img48"))
                                }
                                self.tableView.reloadData()
                            }catch{
//                                HUD.hide()
                                self.hud.dismiss()
                                showError(text:MessagesManger.shared.tryAgainLater)
                            }
                        }
                    }
                }
                
                
            case .failure(_):
//                HUD.hide()
                self.hud.dismiss()
                showError(text:MessagesManger.shared.tryAgainLater)
                break
            }
            
        }
    }
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        _ = alert.bounds
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: MessagesManger.shared.done, andButtons: nil)
    }
    
}


extension FavVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resOrCafe.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! ItemCell
        cell.titleLbl.text = resOrCafe[indexPath.row].name ?? ""
        cell.rate.value = CGFloat((resOrCafe[indexPath.row].rate as NSString?)?.floatValue ?? 0.0)
        cell.timLbl.text = resOrCafe[indexPath.row].working_schedule ?? "----"
        cell.locationLbl.text = resOrCafe[indexPath.row].address ?? "----"
        let url = URL(string: resOrCafe[indexPath.row].image ?? "")
        cell.mainImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "img01"), options: .highPriority, completed: nil)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("todo -> go to the shop page")
        let navVC = storyboard?.instantiateViewController(withIdentifier: "ItemVC") as! ItemVC
        navVC.item = resOrCafe[indexPath.row]
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    
    
}
