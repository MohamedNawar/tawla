//
//  AllVC.swift
//  tawla
//
//  Created by Moaz Ezz on 7/17/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit
import iCarousel
import Alamofire
import JGProgressHUD


class AllVC: UIViewController {
    var resOrCafe = [ResOrCafe]()
    var resOrCafeFeature = [ResOrCafe]()
    let hud = JGProgressHUD(style: .light)
    var place_type_id = -1
    
    @IBOutlet weak var carouselView: iCarousel!
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        carouselView.type = .coverFlow
        getPlaces()
        getFeturedPlaces()
        // Do any additional setup after loading the view.
    }
}


extension AllVC : iCarouselDataSource, iCarouselDelegate{
    func numberOfItems(in carousel: iCarousel) -> Int {
        
        return resOrCafeFeature.count
        
        
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let itemView = Bundle.main.loadNibNamed("ItemSlidingView", owner: self, options: nil)?.first as! ItemSlidingView
        
        itemView.titleLbl.text = resOrCafeFeature[index].name
        itemView.descriptionLbl.text = resOrCafeFeature[index].details
        itemView.rate.value = CGFloat((resOrCafeFeature[index].rate as NSString?)?.floatValue ?? 0.0)
        let url = URL(string: resOrCafeFeature[index].image ?? "")
        itemView.mainImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "img19"), options: .highPriority, completed: nil)
        return itemView
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .spacing) {
            return value * 1.1
        }
        return value
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "ItemVC") as! ItemVC
        navVC.item = resOrCafeFeature[index]
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    
    
    private func getPlaces(){
        
        hud.show(in: self.view)
        
        var par = "?"
        if place_type_id != -1 && place_type_id != 0{
            par = par + "place_type_id=\(place_type_id)&"
        }
        if let temp = UserDefaults.standard.string(forKey: "SelectedLoc") {
            par = par + "city_id=\(temp)"
        }
        let header = APIs.sharedInstance.getHeader()
        
        Alamofire.request(APIs.sharedInstance.getPlaces() + par, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                self.hud.dismiss()
                print(value)
                if let temp = value as? Dictionary<String,AnyObject>{
                    if let temp2 = temp["data"]{
                        
                        
                        do {
                            let temp3 = try JSONSerialization.data(withJSONObject: temp2, options: .prettyPrinted)
                            self.resOrCafe = try JSONDecoder().decode([ResOrCafe].self, from: temp3 )
                            self.carouselView.reloadData()
                            self.tableView.reloadData()
                        }catch{
                            self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                            self.hud.textLabel.text = MessagesManger.shared.tryAgainLater
                            self.hud.dismiss(afterDelay: 1.0)
                        }
                    }
                }
                
            case .failure(_):
                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                self.hud.textLabel.text = MessagesManger.shared.tryAgainLater
                self.hud.dismiss(afterDelay: 1.0)
                
                break
                
            }
        }
        
    }
    
    private func getFeturedPlaces(){
        var par = "?is_featured=true&"
        if place_type_id != -1 && place_type_id != 0{
            par = par + "place_type_id=\(place_type_id)&"
        }
        if let temp = UserDefaults.standard.string(forKey: "SelectedLoc") {
            par = par + "city_id=\(temp)"
        }
        hud.show(in: self.view)
        let header = APIs.sharedInstance.getHeader()
        
        Alamofire.request(APIs.sharedInstance.getPlaces() + par, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                self.hud.dismiss()
                print(value)
                if let temp = value as? Dictionary<String,AnyObject>{
                    if let temp2 = temp["data"]{
                        
                        
                        do {
                            let temp3 = try JSONSerialization.data(withJSONObject: temp2, options: .prettyPrinted)
                            self.resOrCafeFeature = try JSONDecoder().decode([ResOrCafe].self, from: temp3 )
                            self.carouselView.reloadData()
                            self.tableView.reloadData()
                        }catch{
                            self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                            self.hud.textLabel.text = MessagesManger.shared.tryAgainLater
                            self.hud.dismiss(afterDelay: 1.0)
                        }
                    }
                }
                
            case .failure(_):
                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                self.hud.textLabel.text = MessagesManger.shared.tryAgainLater
                self.hud.dismiss(afterDelay: 1.0)
                
                break
                
            }
        }
        
    }
    
}

extension AllVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return resOrCafe.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! ItemCell
        print(indexPath.row)
        print(resOrCafe[indexPath.row].name)
        print("----------")
//        if indexPath.row < resOrCafe.count {
//            let title = resOrCafe[indexPath.row].name ?? ""
        cell.titleLbl.text = resOrCafe[indexPath.row].name ?? ""
        cell.rate.value = CGFloat((resOrCafe[indexPath.row].rate as NSString?)?.floatValue ?? 0.0)
        cell.timLbl.text = resOrCafe[indexPath.row].working_schedule ?? "----"
        cell.locationLbl.text = resOrCafe[indexPath.row].address ?? "----"
        let url = URL(string: resOrCafe[indexPath.row].image ?? "")
            cell.mainImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "img01"), options: .highPriority, completed: nil)
            
//        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "ItemVC") as! ItemVC
        navVC.item = resOrCafe[indexPath.row]
        self.navigationController?.pushViewController(navVC, animated: true)
    }
}

