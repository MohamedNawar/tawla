//
//  ResturantCategoriesVC.swift
//  tawla
//
//  Created by Moaz Ezz on 7/17/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit
import Alamofire
import JGProgressHUD


class CategoriesVC: UIViewController {
    var selectedPlace = Place()
    let hud = JGProgressHUD(style: .light)
//    var imagesArray = [#imageLiteral(resourceName: "img55"),#imageLiteral(resourceName: "img56"),#imageLiteral(resourceName: "img59"),#imageLiteral(resourceName: "img44"),#imageLiteral(resourceName: "img61"),#imageLiteral(resourceName: "img62"),#imageLiteral(resourceName: "img30"),#imageLiteral(resourceName: "img15"),#imageLiteral(resourceName: "img59"),#imageLiteral(resourceName: "img26"),#imageLiteral(resourceName: "img47"),#imageLiteral(resourceName: "img63")]
    var services = [Service]()
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        getServicesType()
        // Do any additional setup after loading the view.
    }
    

    private func getServicesType(){
//        HUD.show(.progress, onView: self.view)
        hud.show(in: self.view)
        let header = APIs.sharedInstance.getHeader()
        Alamofire.request(APIs.sharedInstance.getServices(ForId: selectedPlace.id ?? -1), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
//                HUD.hide()
                
                self.hud.dismiss()
                print(value)
                if let temp = value as? Dictionary<String,AnyObject>{
                    if let temp2 = temp["data"]{
                        
                        
                        do {
                            let temp3 = try JSONSerialization.data(withJSONObject: temp2, options: .prettyPrinted)
                            self.services = try JSONDecoder().decode([Service].self, from: temp3 )
                            self.collectionView.reloadData()
                        }catch{
                            self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                            self.hud.textLabel.text = MessagesManger.shared.tryAgainLater
                            self.hud.dismiss(afterDelay: 1.0)
                        }
                    }
                }
                
            case .failure(_):
                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                self.hud.textLabel.text = MessagesManger.shared.tryAgainLater
                self.hud.dismiss(afterDelay: 1.0)
                
                break
                
            }
        }
        
    }
    
}


extension CategoriesVC : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return services.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RestCategoryCell", for: indexPath)
        let catImageBack = cell.viewWithTag(3) as! UIImageView
        let catImage = cell.viewWithTag(2) as! UIImageView
        let catLbl = cell.viewWithTag(1) as! UILabel
        let url = URL(string: services[indexPath.row].image ?? "")
        catImageBack.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "img02"), options: .highPriority, completed: nil)
        let urlIcon = URL(string: services[indexPath.row].icon ?? "")
        catImage.sd_setImage(with: urlIcon, placeholderImage: #imageLiteral(resourceName: "img02"), options: .highPriority, completed: nil)
        catLbl.text = services[indexPath.row].name
        catLbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("todo -> go to the shop page")
//                let navVC = storyboard?.instantiateViewController(withIdentifier: "ItemVC") as! ItemVC
        let navVC = storyboard?.instantiateViewController(withIdentifier: "MostVisitedVC") as! MostVisitedVC
        print(services[indexPath.row].id ?? -1)
        navVC.service_types = services[indexPath.row].id ?? -1
                self.navigationController?.pushViewController(navVC, animated: true)
    }
    
}

extension CategoriesVC : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        print("collectionView height : \(collectionView.bounds.size.height)")
        print("collectionView width : \(collectionView.bounds.size.width)")
        return CGSize(width: collectionView.bounds.size.width , height: 180)
    }
    
}
