//
//  SelectCityVC.swift
//  tawla
//
//  Created by Moaz Ezz on 8/5/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import Alamofire
import PKHUD
import FCAlertView
import McPicker
import RSSelectionMenu
import LanguageManager_iOS
import JGProgressHUD
//import MOLH

class SelectCityVC: UIViewController {
    var user = userData()
    var selectedCityId = -1
    var SelectedItem = ResOrCafe()
    var cities = CitiesAndSpecial()
    var dateHolder = ""
    
    @IBOutlet weak var dissOutlet: UIButton!
    let hud = JGProgressHUD(style: .light)
    
    @IBAction func diss(_ sender: Any) {
        if UserDefaults.standard.string(forKey: "SelectedLoc") == nil || UserDefaults.standard.string(forKey: "SelectedLoc") == "-1" {
            showError(text: NSLocalizedString("choose city", comment: ""))
            return
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var langLbl: UILabel!
    
    @IBOutlet weak var langImg: UIImageView!
    @IBOutlet weak var dateBtn: UIButton!
    @IBAction func datePressed(_ sender: Any) {
        if cities.data?.count ?? 0 > 0 {
        let nameArray = cities.getStrings()
        
        let mcPicker = McPicker(data: [nameArray])
        mcPicker.show {  (selections: [Int : String]) -> Void in
            if selections.count > 0 {
                if let StringTemp = selections[0]{
                    self.dateBtn.setTitle(StringTemp, for: .normal)
                    for i in 0..<nameArray.count{
                        if self.cities.data?[i].name == StringTemp{
                            self.selectedCityId = self.cities.data?[i].id ?? -1
                            return
                        }
                    }
                }
            }
        }
        }else{
            return
        }
        }
    
    
    @IBAction func saveAndDiss(_ sender: Any) {
        goToNext()
    }
    
    @IBOutlet weak var selectLangOutlet: UIButton!
    
    
    @IBAction func selectLangPressed(_ sender: Any) {
        let simpleDataArray = ["اللغة العربية","English Language"]
        
        // Show menu with datasource array - Default SelectionType = Single
        // Here you'll get cell configuration where you can set any text based on condition
        // Cell configuration following parameters.
        // 1. UITableViewCell   2. Object of type T   3. IndexPath
        
        let selectionMenu =  RSSelectionMenu(dataSource: simpleDataArray) { (cell, object, indexPath) in
            if indexPath.row == 0{
                cell.textLabel?.text = object
                cell.imageView?.image = #imageLiteral(resourceName: "SA")
            }else{
                cell.textLabel?.text = object
                cell.imageView?.image = #imageLiteral(resourceName: "AC")
            }
        }
        
        
        selectionMenu.onDismiss = {(text) in
            if text.first == "اللغة العربية"{
                self.langLbl.text = text.first ?? ""
                self.langImg.image = #imageLiteral(resourceName: "SA")
                //                APIs.sharedInstance.changeLang(lang: true)
                //                MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "ar")
                
                //                MOLH.setLanguageTo("ar")
                //                MOLH.reset()
                //                print(MOLHLanguage.currentAppleLanguage())
                //
                //                MOLH.reset()
                
                self.putLang(lang: "ar")
            }else {
                self.langLbl.text = text.first ?? ""
                self.langImg.image = #imageLiteral(resourceName: "AC")
                //                APIs.sharedInstance.changeLang(lang: false)
                //                MOLH.setLanguageTo("en")
                //                MOLH.setLanguageTo("en")
                //                print(MOLHLanguage.currentAppleLanguage())
                //                MOLH.reset()
                self.putLang(lang: "en")
            }
            
            
            
            
        }
        
        // show as PresentationStyle = Push
        selectionMenu.show(style: .popover(sourceView: selectLangOutlet, size: CGSize(width: 250, height: 100)), from: self)
    }
    
    @IBAction func seeNearBy(_ sender: Any) {
        let VC = storyboard?.instantiateViewController(withIdentifier: "ShowLocationInMapViewController") as!
        ShowLocationInMapViewController
        VC.isViewer = false
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCities()
        if UserDefaults.standard.string(forKey: "isFirstEntry2") != "No" {
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "IntroVC2") as! IntroVC2
            self.present(VC, animated: false, completion: nil)
            UserDefaults.standard.set("No", forKey: "isFirstEntry2")
        }
        
        //        if UserDefaults.standard.string(forKey: "isFirstEntry2") == nil {
        //            self.dissOutlet.isHidden = true
        //            let navVC = storyboard?.instantiateViewController(withIdentifier: "navgationC")
        //            self.present(navVC!, animated: true, completion: nil)
        //            
        //        }
        
        langImg.layer.cornerRadius = 15
        if LanguageManager.shared.currentLanguage == .ar{
            self.langLbl.text = "اللغة العربية"
            self.langImg.image = #imageLiteral(resourceName: "SA")
        }else{
            self.langLbl.text = "English Language"
            self.langImg.image = #imageLiteral(resourceName: "AC")
        }
        // Do any additional setup after loading the view.
    }
    
    func changeLang(lang:String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateInitialViewController()
        if lang == "ar" {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            Bundle.setLanguage(lang)
            LanguageManager.shared.setLanguage(language: .ar, rootViewController: viewController, animation: { view in
                MessagesManger.shared.reinit()
                view.transform = CGAffineTransform(scaleX: 2, y: 2)
                view.alpha = 0
            })
            
        } else {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            Bundle.setLanguage(lang)
            LanguageManager.shared.setLanguage(language: .en, rootViewController: viewController, animation: { view in
                MessagesManger.shared.reinit()
                view.transform = CGAffineTransform(scaleX: 2, y: 2)
                view.alpha = 0
            })
            
            
        }
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    
    
    
    private func getCities(){
        hud.show(in: self.view)
        let header = APIs.sharedInstance.getHeader()
        
        //        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.sharedInstance.getCities(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        //                        HUD.hide()
                        self.hud.dismiss()
                        self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12"))
                        
                        
                    }catch{
                        showError(text:MessagesManger.shared.checkYourData)
                    }
                    
                }else{
                    do {
                        self.cities = try JSONDecoder().decode(CitiesAndSpecial.self, from: response.data! )
                        print("done")
                        //                        self.goToNext()
                        //                        HUD.hide()
                        self.hud.dismiss()
                        
                        //                                self.setupUpUI()
                    }catch{
                        
                        showError(text:MessagesManger.shared.tryAgainLater)
                    }
                }
                
                
            case .failure(_):
                //                HUD.hide()
                self.hud.dismiss()
                showError(text:MessagesManger.shared.tryAgainLater)
                break
            }
            
        }
    }
    
    private func putLang(lang:String){
        
        if (user.token ?? "") == "" {
            self.changeLang(lang: lang)
        }else{
            let header = APIs.sharedInstance.getHeader()
            
            HUD.show(.progress, onView: self.view)
            Alamofire.request(APIs.sharedInstance.putLang(lang: lang), method: .put, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
                switch(response.result) {
                case .success(let value):
                    print(value)
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                        
                        do {
                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                            HUD.hide()
                            self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12"))
                            
                            
                            //                            user.saveUser(user: user)
                        }catch{
                            showError(text:MessagesManger.shared.checkYourData)
                        }
                        
                    }else{
                        self.changeLang(lang: lang)
                    }
                    
                    
                case .failure(_):
                    HUD.hide()
                    showError(text:MessagesManger.shared.tryAgainLater)
                    break
                }
                
            }
            
        }
    }
    
    private func goToNext() {
        UserDefaults.standard.set(selectedCityId, forKey: "SelectedLoc")
        if UserDefaults.standard.string(forKey: "SelectedLoc") == nil || UserDefaults.standard.string(forKey: "SelectedLoc") == "-1" {
            showError(text: NSLocalizedString("choose city", comment: ""))
            return
        }
        self.dismiss(animated: true, completion: nil)
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: MessagesManger.shared.done, andButtons: nil)
    }
    
    
    
}

extension UIViewController {
    func loopThroughSubViewAndFlipTheImageIfItsAUIImageView(subviews: [UIView]) {
        
        if subviews.count > 0 {
            for subView in subviews {
                print("flipping...")
                if subView.isKind(of:UIButton.self) {
                    let toRightArrow = subView as! UIButton
                    if let _img = toRightArrow.currentImage {
                        let flippedImg = UIImage(cgImage: _img.cgImage!, scale: _img.scale, orientation:  UIImageOrientation.upMirrored)
                        
                        /*1*/toRightArrow.setImage(flippedImg, for: .normal)
                    }
                }
                /*2*/loopThroughSubViewAndFlipTheImageIfItsAUIImageView(subviews: subView.subviews)
            }
        }
    }
}
