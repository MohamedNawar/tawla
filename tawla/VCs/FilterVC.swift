//
//  FilterVC.swift
//  tawla
//
//  Created by Moaz Ezz on 7/19/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
// http://app.tawla.net/api/place_types/1/service_types

import UIKit
import PKHUD
import Alamofire
import McPicker
import FCAlertView
import LanguageManager_iOS
import JGProgressHUD
class FilterVC: UIViewController, UIPickerViewDelegate , UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return categories.data?.count ?? 0
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        servicesTxt.text = categories.data?[row].name ?? ""
        categoryId = "\(categories.data?[row].id ?? 0)"
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return categories.data?[row].name ?? ""
    }
    let categoryPicker = UIPickerView()
    func showcategoryPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donePicker));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        toolbar.setItems([doneButton , spaceButton1], animated: false)
        toolbar.barTintColor = .gray
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        servicesTxt.inputView = categoryPicker
        servicesTxt.inputAccessoryView = toolbar
    }
    @objc func cancel1(){
        servicesTxt.text = ""
        self.view.endEditing(true)
    }
    @objc func donePicker(){
        if servicesTxt.text == "" {
            servicesTxt.text = categories.data?[0].name ?? ""
            categoryId = "\(categories.data?[0].id ?? 0)"
        }
        self.view.endEditing(true)
    }
var categoryId = "-1"
    var SelectedPlace = Place()
    
    let hud = JGProgressHUD(style: .light)
    func getCategories(){
        let header = APIs.sharedInstance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(URL(string: "http://app.tawla.net/api/place_types/1/service_types")! , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
//                        let err = try JSONDecoder().decode(Error.self, from: response.data!)
//                        self.makeDoneAlert(title: NSLocalizedString("Error", comment: ""), SubTitle: err.message ?? "", Image:  #imageLiteral(resourceName: "alarm"))
//                        print()
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.categories = try JSONDecoder().decode(CategoryData.self, from: response.data!)
                        
                    }catch{
                        print(error)
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        HUD.flash(.labeledError(title: "", subtitle: lockString), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    @IBOutlet weak var serchName: DesignableUITextField!
    @IBOutlet weak var getheringTypeCollectionView: UICollectionView!
    var categories = CategoryData()
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var servicesCollectionView: UICollectionView!
    var gatheringTypes = [Service]()
    var servicesTypes = [Service]()
    var places = [Place]()
    var selectedGathering = -1
    var selectedService = -1
    var selectedPlaceType = -1
    var selectedCityId = -1
    var cities = CitiesAndSpecial()
    @IBAction func cancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBOutlet weak var servicesTxt: UITextField!
    @IBOutlet weak var categoryTextField: UITextField!
    
    @IBOutlet weak var dateBtn: UIButton!
    @IBAction func datePressed(_ sender: Any) {
        let nameArray = cities.getStrings()
        
        let mcPicker = McPicker(data: [nameArray])
        mcPicker.show {  (selections: [Int : String]) -> Void in
            if let StringTemp = selections[0]{
                self.dateBtn.setTitle(StringTemp, for: .normal)
                for i in 0..<nameArray.count{
                    if self.cities.data?[i].name == StringTemp{
                        self.selectedCityId = self.cities.data?[i].id ?? -1
                        return
                    }
                }
            }
        }
    }
   
    
    @IBAction func seeNearBy(_ sender: Any) {
        let VC = storyboard?.instantiateViewController(withIdentifier: "ShowLocationInMapViewController") as!
        ShowLocationInMapViewController
        VC.isViewer = false
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func selectCat(_ sender: Any) {
        var nameArray = [String]()
        for i in places{
            nameArray.append(i.name ?? "")
        }
        let mcPicker = McPicker(data: [nameArray])
        mcPicker.show {  (selections: [Int : String]) -> Void in
            if let StringTemp = selections[0]{
                self.categoryTextField.text = StringTemp
                for i in 0..<nameArray.count{
                    if self.places[i].name == StringTemp{
                        self.selectedPlaceType = self.places[i].id ?? -1
                        if self.selectedPlaceType == 1 {
                            self.categoryView.isHidden = false
                            self.getCategories()
                        }else{
                            self.categoryView.isHidden = true
                        }
                        return
                    }
                }
            }
        }
    }
    
    
    @IBAction func showResult(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "MostVisitedVC") as! MostVisitedVC
        vc.search = true
        if selectedGathering != -1{
         vc.gathering_types = gatheringTypes[selectedGathering].id ?? -1
        }else{
          vc.gathering_types = -1
        }
        if categoryId != "-1"{
            vc.service_types = Int(categoryId)!
            print( Int(categoryId) ?? -1)
        }else{
            vc.service_types = -1
        }
        vc.name = serchName.text ?? ""
        vc.place_type_id = selectedPlaceType
        if selectedCityId == -1 {
             HUD.flash(.label( NSLocalizedString("choose City", comment:"")), delay: 1.0)
            return
        }
        vc.selectedCity = selectedCityId
        selectedGathering = -1
        getheringTypeCollectionView.reloadData()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        categoryPicker.delegate = self
        categoryPicker.dataSource = self
        categoryPicker.backgroundColor = .white
        categoryPicker.showsSelectionIndicator = true
        showcategoryPicker()
        print(LanguageManager.shared.currentLanguage)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        getGatheringType()
        getServicesType()
        getPlaceType()
        getCities()
        if LanguageManager.shared.currentLanguage == .ar {
            categoryTextField.textAlignment = .right
            serchName.textAlignment = .right
            
        }else{
            categoryTextField.textAlignment = .left
            serchName.textAlignment = .left
            
        }
    }
    
    private func getCities(){
        let header = APIs.sharedInstance.getHeader()
        
//        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.sharedInstance.getCities(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
                
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
//                        HUD.hide()
                        self.hud.dismiss()
                        self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12"))
                        
                        
                    }catch{
                        showError(text:MessagesManger.shared.checkYourData)
                    }
                    
                }else{
                    do {
                        self.cities = try JSONDecoder().decode(CitiesAndSpecial.self, from: response.data! )
                        print("done")
                        if let loc = UserDefaults.standard.string(forKey: "SelectedLoc"){
                            self.selectedCityId = Int(loc) ?? -1
                            let stringTemp = self.cities.getName(id: Int(loc) ?? -1)
                            self.dateBtn.setTitle(stringTemp, for: .normal)
                        }
                        
                        
//                        HUD.hide()
                        self.hud.dismiss()
                        
                        //                                self.setupUpUI()
                    }catch{
                        
                        showError(text:MessagesManger.shared.tryAgainLater)
                    }
                }
                
                
            case .failure(_):
//                HUD.hide()
                self.hud.dismiss()
                showError(text:MessagesManger.shared.tryAgainLater)
                break
            }
            
        }
    }
    
    private func getServicesType(){
//        HUD.show(.progress, onView: self.view)
        let header = APIs.sharedInstance.getHeader()
        Alamofire.request(APIs.sharedInstance.getServices(ForId: SelectedPlace.id ?? -1), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
//                HUD.hide()
                self.hud.dismiss()
                print(value)
                if let temp = value as? Dictionary<String,AnyObject>{
                    if let temp2 = temp["data"]{
                        
                        
                        do {
                            let temp3 = try JSONSerialization.data(withJSONObject: temp2, options: .prettyPrinted)
                            self.servicesTypes = try JSONDecoder().decode([Service].self, from: temp3 )
                            self.servicesCollectionView.reloadData()
                        }catch{
//                            HUD.hide()
                            self.hud.dismiss()
                            showError(text:MessagesManger.shared.tryAgainLater)
                        }
                    }
                }
                
            case .failure(_):
//                HUD.hide()
                self.hud.dismiss()
                showError(text:MessagesManger.shared.tryAgainLater)
                
                break
                
            }
        }
        
    }
    
    
    private func getGatheringType(){
//        HUD.show(.progress, onView: self.view)
        let header = APIs.sharedInstance.getHeader()
        Alamofire.request(APIs.sharedInstance.getGatheringTypes(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
//                HUD.hide()
                self.hud.dismiss()
                print(value)
                if let temp = value as? Dictionary<String,AnyObject>{
                    if let temp2 = temp["data"]{
                        
                        
                        do {
                            let temp3 = try JSONSerialization.data(withJSONObject: temp2, options: .prettyPrinted)
                            self.gatheringTypes = try JSONDecoder().decode([Service].self, from: temp3 )
                            self.getheringTypeCollectionView.reloadData()
                        }catch{
//                            HUD.hide()
                            self.hud.dismiss()
                            showError(text:MessagesManger.shared.tryAgainLater)
                        }
                    }
                }
                
            case .failure(_):
//                HUD.hide()
                self.hud.dismiss()
                showError(text:MessagesManger.shared.tryAgainLater)
                
                break
                
            }
        }
        
    }
    
    func getPlaceType(){
//        HUD.show(.progress, onView: self.view)
        let header = APIs.sharedInstance.getHeader()
        Alamofire.request(APIs.sharedInstance.getPlaceType(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
//                HUD.hide()
                self.hud.dismiss()
                print(value)
                if let temp = value as? Dictionary<String,AnyObject>{
                    if let temp2 = temp["data"]{
                        
                        
                        do {
                            let temp3 = try JSONSerialization.data(withJSONObject: temp2, options: .prettyPrinted)
                            self.places = try JSONDecoder().decode([Place].self, from: temp3 )
                        }catch{
//                            HUD.hide()
                            self.hud.dismiss()
                            showError(text:MessagesManger.shared.tryAgainLater)
                        }
                    }
                }
                
            case .failure(_):
//                HUD.hide()
                self.hud.dismiss()
                showError(text:MessagesManger.shared.tryAgainLater)
                
                break
                
            }
        }
        
    }

    

}

extension FilterVC : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case getheringTypeCollectionView:
            return gatheringTypes.count
        default:
            return servicesTypes.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        switch collectionView {
        case getheringTypeCollectionView:
            let cell = getheringTypeCollectionView.dequeueReusableCell(withReuseIdentifier: "ServiceCell", for: indexPath)
            
            let photo = cell.viewWithTag(1) as? UIImageView
            let lbl = cell.viewWithTag(2) as? UILabel
            let url = URL(string: gatheringTypes[indexPath.row].image ?? "")
            photo?.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "img02"), options: .highPriority, completed: nil)
            lbl?.text = gatheringTypes[indexPath.row].name
            if selectedGathering == indexPath.row {
                cell.layer.borderColor = mainColorGRAD.cgColor
                cell.layer.borderWidth = 2
            }else{
                cell.layer.borderColor = UIColor.white.cgColor
            }
            return cell
        default:
            let cell = servicesCollectionView.dequeueReusableCell(withReuseIdentifier: "ServiceCell", for: indexPath)
            
            let photo = cell.viewWithTag(1) as? UIImageView
            let lbl = cell.viewWithTag(2) as? UILabel
            let url = URL(string: servicesTypes[indexPath.row].image ?? "")
            photo?.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "img02"), options: .highPriority, completed: nil)
            lbl?.text = servicesTypes[indexPath.row].name
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case getheringTypeCollectionView:
            if let cell = getheringTypeCollectionView.cellForItem(at: indexPath){
//                cell.backgroundColor = .lightGray
                cell.layer.borderColor = mainColorGRAD.cgColor
                cell.layer.borderWidth = 2
                selectedGathering = indexPath.row
                print(selectedGathering)
            }
        default:
            if let cell = servicesCollectionView.cellForItem(at: indexPath){
//                cell.backgroundColor = .lightGray
                cell.layer.borderColor = mainColorGRAD.cgColor
                cell.layer.borderWidth = 2
                selectedService = servicesTypes[indexPath.row].id ?? -1
                print(selectedGathering)
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        switch collectionView {
        case getheringTypeCollectionView:
            if let cell = getheringTypeCollectionView.cellForItem(at: indexPath){
                cell.layer.borderColor = UIColor.white.cgColor
                
            }
        default:
            if let cell = servicesCollectionView.cellForItem(at: indexPath){
                cell.layer.borderColor = UIColor.white.cgColor
            }
        }
    }
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: MessagesManger.shared.done, andButtons: nil)
    }
}
