//
//  ChooseDateVC.swift
//  tawla
//
//  Created by Moaz Ezz on 8/4/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import Alamofire
import PKHUD
import FCAlertView
import JGProgressHUD
import LanguageManager_iOS
class ChooseDateVC: UIViewController {
    var selectedIndex = -1
    var SelectedItem = ResOrCafe()
    var reservstion = Reservation()
    var dateHolder = ""
    @IBAction func diss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    let hud = JGProgressHUD(style: .light)
    var txtDatePicker:UITextField?
    @IBOutlet weak var dateBtn: UIButton!
    @IBAction func datePressed(_ sender: Any) {
        txtDatePicker?.becomeFirstResponder()
    }
    
    override func viewDidLoad() {
        txtDatePicker = UITextField(frame: CGRect(x: -5, y: -100, width: 0, height: 0) )
        self.view.addSubview(txtDatePicker ?? UIView())
        showDatePicker()

        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }


    private func getReservation(date:String){
        let header = APIs.sharedInstance.getHeader()
        let par = ["date":date]
//        HUD.show(.progress, onView: self.view)
        hud.show(in: self.view)

        Alamofire.request(APIs.sharedInstance.getReservationType(id: SelectedItem.id ?? -1), method: .get, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
                
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
//                        HUD.hide()
                        self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12"))
                        
                        //
                    }catch{
                        showError(text:MessagesManger.shared.checkYourData)
                    }
                    
                }else{
                    do {
                        self.reservstion = try JSONDecoder().decode(Reservation.self, from: response.data! )
                        print("done")
                        self.goToNext()
//                        HUD.hide()
                        
                        //                                self.setupUpUI()
                    }catch{
                        
                        showError(text:MessagesManger.shared.tryAgainLater)
                    }
                }
                
                
            case .failure(_):
//                HUD.hide()
                showError(text:MessagesManger.shared.tryAgainLater)
                break
            }
            
        }
    }
    
    
    private func goToNext() {
        if reservstion.mechanism == "time" {
            let vc = storyboard?.instantiateViewController(withIdentifier: "ReserveVC") as! ReserveVC
            vc.SelectedItem = SelectedItem
            vc.reservstion = reservstion
           vc.dateHolder = dateHolder
            self.navigationController?.pushViewController(vc, animated: true)
        }else if reservstion.mechanism == "queue" {
            self.makeSpecificAlert2(title: MessagesManger.shared.estimatedReserve, SubTitle: " \(dateHolder) \n \(reservstion.schedules?.formated_start_at ?? "") ", Image: #imageLiteral(resourceName: "img18"))
        }else{
           self.makeDoneAlert(title: MessagesManger.shared.sorry, SubTitle: MessagesManger.shared.dayNotAvaliable , Image: #imageLiteral(resourceName: "img18"))
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        alert.titleFont = BoldFont
        alert.subtitleFont = NormalFont
        alert.doneButtonCustomFont = NormalFont
        alert.doneButtonHighlightedBackgroundColor = secondaryColor
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "تم", andButtons: nil)
    }
    
    func makeSpecificAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        alert.titleFont = BoldFont
        alert.subtitleFont = NormalFont
        alert.doneButtonCustomFont = NormalFont
        alert.firstButtonCustomFont = NormalFont
        alert.doneButtonTitleColor = .black
        alert.firstButtonTitleColor = .black
        alert.addButton("اختيار المنيو الان") {
            //            print("ToDo -> goTo menu")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
            vc.reservation = self.reservstion;
            vc.SelectedItem = self.SelectedItem; self.navigationController?.pushViewController(vc, animated: true)
        }
        alert.doneBlock = {
            self.dismiss(animated: true, completion: nil)
        }
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "اختيار المنيو لاحقا", andButtons: nil)
    }
    
    func makeSpecificAlert2(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        alert.titleFont = BoldFont
        alert.subtitleFont = NormalFont
        alert.doneButtonCustomFont = NormalFont
        alert.firstButtonCustomFont = NormalFont
        alert.doneButtonTitleColor = .black
        alert.firstButtonTitleColor = .black
        alert.addButton("حجز") {
            //            print("ToDo -> goTo menu")
            if user.token == nil || user.token == ""{
                let navVC = self.storyboard?.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
                self.navigationController?.pushViewController(navVC, animated: true)
            } else{
                self.postReservationTime(date: self.reservstion.date ?? "")
            }
        }
        alert.doneBlock = {
//            self.dismiss(animated: true, completion: nil)
        }
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "اختيار ميعاد اخر", andButtons: nil)
    }
    
    private func getDate(date:Date,flag:Bool) -> String{//return date as formated -> Y-m-d 11-15-2017
        
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        let second = calendar.component(.second, from: date)
        return flag ? "\(year)-\(String(format: "%02d", month))-\(String(format: "%02d", day))" : "\(String(format: "%02d", hour)):\(String(format: "%02d", minute)):\(String(format: "%02d", second))"
    }
    
    private func postReservationTime(date:String){
        
        
        let header = APIs.sharedInstance.getHeader()
        let par = ["date":date]
//        HUD.show(.progress)
        hud.show(in: self.view)
        Alamofire.request(APIs.sharedInstance.postReservationQueue(id: SelectedItem.id ?? -1), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
//                HUD.hide()
                 self.hud.dismiss()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12"))
                
                        
                    }catch{
                        showError(text: MessagesManger.shared.checkYourData)
                    }
                }else{
                    if let temp = value as? Dictionary<String,AnyObject>{
                        if let temp2 = temp["data"]{
                            
                            
                            do {
                                let temp3 = try JSONSerialization.data(withJSONObject: temp2, options: .prettyPrinted)
                                self.reservstion = try JSONDecoder().decode(Reservation.self, from: temp3 )
                                self.makeSpecificAlert(title: "تم الحجز بنجاح", SubTitle: " من فضلك اخر احدي الاختيارات التاليه", Image:  #imageLiteral(resourceName: "img11"))
                                
                            }catch{
//                                HUD.hide()
                                 self.hud.dismiss()
                                showError(text: MessagesManger.shared.tryAgainLater)
                            }
                        }
                    }
                }
                
                
            case .failure(_):
//                HUD.hide()
                 self.hud.dismiss()

                 showError(text: MessagesManger.shared.tryAgainLater)
                break
            }
            
        }
    }
    let datePicker = UIDatePicker()
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        datePicker.locale = Locale(identifier: LanguageManager.shared.currentLanguage.rawValue)
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: MessagesManger.shared.done, style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: ""), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        txtDatePicker?.inputAccessoryView = toolbar
        txtDatePicker?.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        
        self.dateHolder = self.getDate(date: datePicker.date, flag: true)
        self.dateBtn.setTitle(self.dateHolder, for: .normal)
        self.getReservation(date: self.dateHolder)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }

}
