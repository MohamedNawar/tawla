//
//  SubMainVC.swift
//  tawla
//
//  Created by Moaz Ezz on 7/17/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit
import Parchment
import ChameleonFramework

class SubMainVC: UIViewController {
    var SelectedPlace = Place()

    @IBOutlet weak var MainView: UIView!
    
    override func viewWillDisappear(_ animated: Bool) {
    self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    @IBAction func filterPressed(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "MyNavVC") as! MyNavVC
        vc.selectedPlace = SelectedPlace
        present(vc, animated: true, completion: nil)
     }
    
    
    @IBOutlet weak var notifyBtnOutlet: UIButton!
    @IBAction func notifyBtn(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if user.token == "" || user.token == nil{
            notifyBtnOutlet.isHidden = true
        }

        if L102Language.currentAppleLanguage() == "ar" {
            let forthViewController = storyboard?.instantiateViewController(withIdentifier: "CategoriesVC") as! CategoriesVC
            forthViewController.selectedPlace = SelectedPlace
            
            
            let thirdViewController  = storyboard?.instantiateViewController(withIdentifier: "MostVisitedVC") as! MostVisitedVC
            thirdViewController.isMostVisited = true
            thirdViewController.place_type_id = SelectedPlace.id ?? -1
            
            
            let secondViewController = storyboard?.instantiateViewController(withIdentifier: "AllVC") as! AllVC
            secondViewController.place_type_id = SelectedPlace.id ?? -1
            
            let  firstViewController = storyboard?.instantiateViewController(withIdentifier: "OffersVC") as! OffersVC
            
            
            
            // Initialize a FixedPagingViewController and pass
            // in the view controllers.
           forthViewController.title = MessagesManger.shared.categories
           thirdViewController.title = MessagesManger.shared.mostVisited
            secondViewController.title = MessagesManger.shared.all
            firstViewController.title = MessagesManger.shared.offers
            let pagingViewController = FixedPagingViewController(viewControllers: [
                firstViewController,
                forthViewController,
                thirdViewController,
                secondViewController
                ])
            
            pagingViewController.select(index: 3)
            pagingViewController.textColor = UIColor.white
            pagingViewController.selectedTextColor = UIColor.white
            pagingViewController.indicatorColor = .white
            pagingViewController.menuBackgroundColor = UIColor.init(gradientStyle: .topToBottom, withFrame: CGRect(x: 0, y: 0, width: pagingViewController.menuItemSize.width, height: pagingViewController.menuItemSize.height), andColors: [secondaryColorGRAD, mainColorGRAD])
            
            //            UIColor.init(gradientStyle: . , withFrame: CGRect(x: 0, y: 0, width: pagingViewController.menuItemSize.width, height: pagingViewController.menuItemSize.height), andColors: [secondaryColorGRAD, mainColorGRAD])
            pagingViewController.font = NormalFont!
            pagingViewController.selectedFont = BoldFont!
            
            
            pagingViewController.menuItemSize = PagingMenuItemSize.sizeToFit(minWidth: view.frame.width/4, height: pagingViewController.menuItemSize.height)
            
            
            
            pagingViewController.selectedScrollPosition = .preferCentered
            //        pagingViewController.menuInteraction = .swipe
            // Make sure you add the PagingViewController as a child view
            // controller and contrain it to the edges of the view.
            addChildViewController(pagingViewController)
            MainView.addSubview(pagingViewController.view)
            
            
            pagingViewController.didMove(toParentViewController: self)
            pagingViewController.view.translatesAutoresizingMaskIntoConstraints = false
            
            
            
            
            NSLayoutConstraint.activate([
                pagingViewController.view.leadingAnchor.constraint(equalTo: MainView.leadingAnchor),
                pagingViewController.view.trailingAnchor.constraint(equalTo: MainView.trailingAnchor),
                pagingViewController.view.bottomAnchor.constraint(equalTo: MainView.bottomAnchor),
                pagingViewController.view.topAnchor.constraint(equalTo: MainView.topAnchor )
                ])
        }else{
            let firstViewController = storyboard?.instantiateViewController(withIdentifier: "CategoriesVC") as! CategoriesVC
            firstViewController.selectedPlace = SelectedPlace
            let secondViewController = storyboard?.instantiateViewController(withIdentifier: "MostVisitedVC") as! MostVisitedVC
            secondViewController.isMostVisited = true
            secondViewController.place_type_id = SelectedPlace.id ?? -1
            
            let thirdViewController = storyboard?.instantiateViewController(withIdentifier: "AllVC") as! AllVC
            thirdViewController.place_type_id = SelectedPlace.id ?? -1
            let  forthViewController = storyboard?.instantiateViewController(withIdentifier: "OffersVC") as! OffersVC
            
            
            
            // Initialize a FixedPagingViewController and pass
            // in the view controllers.
            firstViewController.title = MessagesManger.shared.categories
            secondViewController.title = MessagesManger.shared.mostVisited
            thirdViewController.title = MessagesManger.shared.all
            forthViewController.title = MessagesManger.shared.offers
            let pagingViewController = FixedPagingViewController(viewControllers: [
                thirdViewController,
                secondViewController,
                firstViewController,
                forthViewController
                ])
            
            pagingViewController.textColor = UIColor.white
            pagingViewController.selectedTextColor = UIColor.white
            pagingViewController.indicatorColor = .white
            pagingViewController.menuBackgroundColor = UIColor.init(gradientStyle: .topToBottom, withFrame: CGRect(x: 0, y: 0, width: pagingViewController.menuItemSize.width, height: pagingViewController.menuItemSize.height), andColors: [secondaryColorGRAD, mainColorGRAD])
            
            //            UIColor.init(gradientStyle: . , withFrame: CGRect(x: 0, y: 0, width: pagingViewController.menuItemSize.width, height: pagingViewController.menuItemSize.height), andColors: [secondaryColorGRAD, mainColorGRAD])
            pagingViewController.font = NormalFont!
            pagingViewController.selectedFont = BoldFont!
            
            
            pagingViewController.menuItemSize = PagingMenuItemSize.sizeToFit(minWidth: view.frame.width/4, height: pagingViewController.menuItemSize.height)
            
            
            
            pagingViewController.selectedScrollPosition = .preferCentered
            //        pagingViewController.menuInteraction = .swipe
            // Make sure you add the PagingViewController as a child view
            // controller and contrain it to the edges of the view.
            addChildViewController(pagingViewController)
            MainView.addSubview(pagingViewController.view)
            
            
            pagingViewController.didMove(toParentViewController: self)
            pagingViewController.view.translatesAutoresizingMaskIntoConstraints = false
            
            
            
            
            NSLayoutConstraint.activate([
                pagingViewController.view.leadingAnchor.constraint(equalTo: MainView.leadingAnchor),
                pagingViewController.view.trailingAnchor.constraint(equalTo: MainView.trailingAnchor),
                pagingViewController.view.bottomAnchor.constraint(equalTo: MainView.bottomAnchor),
                pagingViewController.view.topAnchor.constraint(equalTo: MainView.topAnchor )
                ])
        }
    }
    

}
