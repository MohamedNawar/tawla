//
//  SignUpTeacherVC.swift
//  MyTeacher
//
//  Created by Moaz Ezz on 1/23/18.
//  Copyright © 2018 Moaz Ezz. All rights reserved.
//

import UIKit
import TextFieldEffects
import FCAlertView
import Alamofire
import PKHUD
import Spring
import McPicker
import MobileCoreServices
import Atributika
import DCKit
import ImagePicker
import CountryPickerView
import JGProgressHUD
import LanguageManager_iOS
 class SignUpVC: UIViewController, UITextFieldDelegate, UINavigationControllerDelegate, CountryPickerViewDelegate, CountryPickerViewDataSource {
    
      let hud = JGProgressHUD(style: .light)
    
    
    
    var imageDate = UIImage()
    var imageAttacment : UIImage?
    var genderType = 0
    var cityId = -1
    var memberType = -1
    var shouldBack = false
    var countryCodeHolder = "+966"
    
    var cities = CitiesAndSpecial()
    var type = false
    @IBOutlet weak var scrollView: UIScrollView!
//    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var name: UITextField!
    
    @IBOutlet weak var mobile2: UITextField!
    //    @IBOutlet weak var lastName: UITextField!
  
    @IBOutlet weak var email: UITextField!
    
    
    @IBOutlet weak var pass: UITextField!
    
    @IBOutlet weak var passConfig: UITextField!
    
    @IBOutlet weak var agreeLbl: UILabel!
    
//    @IBOutlet weak var checkBox: DCBorderedButton!
    var cpvInternal = CountryPickerView()
    
    @IBAction func signUp(_ sender: Any) {
        postMessage()
    }
    
    @IBOutlet weak var maleBtn: DCBorderedButton!
    @IBAction func setMale(_ sender: Any) {
        if let button = sender as? UIButton {
            if button.isSelected {
                genderType = 0
                button.isSelected = false
            } else {
                genderType = 1
                button.isSelected = true
            }
            print(genderType)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        name.delegate = self
        email.delegate = self
        pass.delegate = self
        passConfig.delegate = self
//        lastName.delegate = self
        
        mobile2.delegate = self
        cpvInternal.delegate = self
        cpvInternal.dataSource = self
        
        let cp = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 120, height: 20))
        cp.textColor = .white
        cp.delegate = self
        cp.dataSource = self
        mobile2.leftView = cp
        mobile2.leftViewMode = .always
        cp.setCountryByPhoneCode("+966")
        
        self.cpvInternal = cp
        setupAgreementLbl()
        view.viewOfType(type:UITextField.self) {
            view in
            if LanguageManager.shared.currentLanguage == .ar {
                view.textAlignment = .right
            }else{
                view.textAlignment = .left
            }
        }
        
    }
    
    private func setupAgreementLbl() {
        agreeLbl.isUserInteractionEnabled = true
        agreeLbl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.agreeTapped)))
        let u = Style("u").underlineStyle(.styleSingle)
        let b = Style("b").foregroundColor(secondaryColor)
        agreeLbl.attributedText = MessagesManger.shared.termAndCoditions
            .style(tags: u, b)
            .attributedString
        
    }
    @objc private func agreeTapped() {
        let popVC = storyboard?.instantiateViewController(withIdentifier: "TermsAndAgreementVC") as! TermsAndAgreementVC
        popVC.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(popVC, animated: true, completion: nil)
    }
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        countryCodeHolder = country.phoneCode
        mobile2.text = ""
        print(countryCodeHolder)
    }
    
    
    

    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == mobile2 {
            
            if range.location == 0{
                if string == "0" {
                   showError(text: MessagesManger.shared.checkZeros)
                    return false
                }
            }
            
            
            let maxValue = 13 - countryCodeHolder.count
            print(mobile2.text)
            if (textField.text?.length ?? 9) >= maxValue && string != ""{
                return false
            }
        }

        return true
    }
    
    
    
    func convertImageToBase64(image: UIImage) -> String {
        
        let imageData = UIImageJPEGRepresentation(image,0.05)
        let base64String = imageData?.base64EncodedString()
        
        return base64String!
        
    }// end convertImageToBase64

    func checkEmpty() -> Bool {
        if name.text!.isEmpty == false && email.text!.isEmpty == false && pass.text!.isEmpty == false && passConfig.text!.isEmpty == false && mobile2.text!.isEmpty == false{
            
            return true
        }
        if name.text!.isEmpty == false && email.text!.isEmpty == false && pass.text!.isEmpty == false && passConfig.text!.isEmpty == false && mobile2.text!.isEmpty == false{
            
            return true
        }
        makeDoneAlert(title: MessagesManger.shared.emptyField, SubTitle: MessagesManger.shared.checkYourData, Image: #imageLiteral(resourceName: "img55"), color: UIColor.red)
        return false
    }
    
    
 
    
    func postMessage(){
        
        if checkEmpty(){
            if pass.text != passConfig.text{
                makeDoneAlert(title: "", SubTitle: MessagesManger.shared.passwordDontMatch , Image: #imageLiteral(resourceName: "img55"), color: UIColor.red)
                return
            }
            
            if countryCodeHolder == ""{
                makeDoneAlert(title: "", SubTitle: MessagesManger.shared.checkYourData, Image: #imageLiteral(resourceName: "img55"), color: UIColor.red)
                return
            }
            if !maleBtn.isSelected{
                makeDoneAlert(title:"", SubTitle: MessagesManger.shared.checkYourData1, Image: #imageLiteral(resourceName: "img55"), color: UIColor.red)
                return
            }
            
            
            
            
            let header = APIs.sharedInstance.getHeader()
//            var tempText = mobile.text! + mobile2.text!
//            tempText.remove(at: tempText.startIndex)

            let par = ["name": "\(name.text ?? "")",
                       "email": email.text!,
                       "password": pass.text!,
                       "password_confirmation": passConfig.text!,
                       "mobile": countryCodeHolder + mobile2.text!]
            print(par)
            
//            HUD.show(.progress, onView: self.view)
            hud.show(in: self.view)

            Alamofire.request(APIs.sharedInstance.signUp(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
                switch(response.result) {
                case .success(let value):
                    print(response.value)
                    print(response.result.error)
                    print(response.response?.statusCode)
//                    HUD.hide()
                    self.hud.dismiss()
                    print(value)
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                        
                        do {
                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                             let error1 = err.parseError()
                            if error1 == "" {
                                let navVC = self.storyboard?.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
                                self.navigationController?.pushViewController(navVC, animated: true)
                            }else{
                            self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12"), color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1))
                            }
                           
                        }catch{
                            showError(text: MessagesManger.shared.checkYourData)
                        }
                    }else{
                        
                        do {
                            user = try JSONDecoder().decode(userData.self, from: response.data!)
                            user.saveUser()
                            self.goToMain()
                            
                            
                        }catch{
                            showError(text:MessagesManger.shared.tryAgainLater)
                        }
                    }
                case .failure(_):
//                    HUD.hide()
                    self.hud.dismiss()
                    showError(text:MessagesManger.shared.tryAgainLater)
                    break
                }
            }

        }
    }
    
    

    
    
    
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage, color : UIColor) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = false
//        let updatedFrame = alert.bounds
        alert.colorScheme = color
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: MessagesManger.shared.done, andButtons: nil)
    }
    
    func goToMain() {
        if !user.isVerified() {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MobileActivationVC") as! MobileActivationVC
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        guard !shouldBack else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        user.saveUser()
        let navVC = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        navVC.navigationItem.hidesBackButton = true
        self.navigationController?.pushViewController(navVC, animated: true)
        
    }
    
    

}
