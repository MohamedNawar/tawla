//
//  ViewController.swift
//  examp
//
//  Created by MACBOOK on 7/21/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import HCSStarRatingView
import Alamofire
import PKHUD
import FCAlertView
import JGProgressHUD
class SummeryVC: UIViewController , UITableViewDelegate , UITableViewDataSource  {
    @IBOutlet weak var image: UIImageView!
    let hud = JGProgressHUD(style: .light)

    @IBOutlet weak var rate: HCSStarRatingView!
    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var placeDetails: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    
    @IBOutlet weak var taxLbl: UILabel!
    @IBOutlet weak var ServiceLbl: UILabel!
    @IBOutlet weak var total: UILabel!
    var totalTemp = 0{
        didSet{
            total.text = "\(totalTemp)"
        }
    }
    @IBAction func confirmOrder(_ sender: Any) {
        postMenu()
    }
    
    var SelectedMenuItems = [MenuItem]()
    var reservation = Reservation()
    var place = ResOrCafe()
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SelectedMenuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SummryCell", for: indexPath) as! SummryCell
        cell.name.text = SelectedMenuItems[indexPath.row].name
        cell.price.text = SelectedMenuItems[indexPath.row].price
        cell.numberOfOrders.text = String(SelectedMenuItems[indexPath.row].quentity ?? 0);
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        placeName.text = place.name
        placeDetails.text = place.details
        taxLbl.text = "0 %"
        ServiceLbl.text = "0 %"
        let url = URL(string: place.image ?? "")
        image.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "img02"), options: .highPriority, completed: nil)
        rate.value = CGFloat((place.rate as NSString?)?.floatValue ?? 0.0)
        tableView.delegate = self
        tableView.dataSource = self
        
        
        self.totalTemp = 0
        for i in SelectedMenuItems{
            if let temp = i.quentity{
                self.totalTemp += Int(i.price ?? "0")! * temp
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    private func postMenu(){
        var par = [String: Int]()
        for i in SelectedMenuItems {
            let temp = "menu_items[\(i.id ?? -1)][quantity]"
            par.updateValue(i.quentity ?? 0, forKey: temp)
        }
        
        let header = APIs.sharedInstance.getHeader()
        
//        HUD.show(.progress)
        hud.show(in: self.view)
//        let url = APIs.sharedInstance.postMenu(id: reservation.id ?? -1);
        Alamofire.request(APIs.sharedInstance.postMenu(id: reservation.id ?? -1), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
//                HUD.hide()
                self.hud.dismiss()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12"))
                        
                    }catch{
                        showError(text:MessagesManger.shared.tryAgainLater)
                    }
                }else{
                    do {
                        self.reservation = try JSONDecoder().decode(Reservation.self, from: response.data! )
                        self.goToNext()
                        
                        
                        
                        //                                self.setupUpUI()
                    }catch{
//                        HUD.hide()
                        self.hud.dismiss()
                        showError(text:MessagesManger.shared.tryAgainLater)
                    }
                }
                
                
            case .failure(_):
//                HUD.hide()
                self.hud.dismiss()
                showError(text:MessagesManger.shared.tryAgainLater)
                break
            }
            
        }
    }
    
    private func goToNext(){
//        makeDoneAlert(title: "تم اضافة المنيو الي حجزك", SubTitle: "للمتابعة الحجز من فضلك توجه لقسم حجوزاتي", Image: #imageLiteral(resourceName: "img11"))
//        self.dismiss(animated: true, completion: nil)
        let vc = storyboard?.instantiateViewController(withIdentifier: "DoneVC") as! DoneVC
//        vc.reservstion = self.reservstion;
//        vc.SelectedItem = self.SelectedItem;
        vc.navigationItem.setHidesBackButton(true, animated:true)
        vc.isMenu = true; self.navigationController?.pushViewController(vc, animated: true)
    }

    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        _ = alert.bounds
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: MessagesManger.shared.done, andButtons: nil)
    }
}

