//
//  MobileActivationVC.swift
//  tawla
//
//  Created by Moaz Ezz on 9/10/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import Spring
import FCAlertView
import JGProgressHUD
import LanguageManager_iOS
class MobileActivationVC: UIViewController {
    var tokenMain = ""
    var token = userData()
    let hud = JGProgressHUD(style: .light)

    
    @IBOutlet weak var codeView: SpringView!
    
    
    
    
    @IBOutlet weak var code: UITextField!
    
    @IBOutlet weak var sendCodeBtn: UIButton!
    
    
    @IBOutlet weak var dissBtnOutlet: UIButton!
    
    
    @IBOutlet weak var timerLbl: UILabel!
    
    @IBAction func resend(_ sender: Any) {
        createVerificationCode(email: user.data?.mobile ?? "")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = secondaryColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
        setupCode()
        var _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCounter), userInfo: nil, repeats: true)
        if LanguageManager.shared.currentLanguage == .ar {
            code.textAlignment = .right
        }else{
            code.textAlignment = .left
            
        }

//        createVerificationCode(email: user.data?.mobile ?? "")
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = secondaryColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    
    @IBAction func done(_ sender: Any) {
        user.remove()
        
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        
    }
    
    
    @IBAction func sendCode(_ sender: Any) {
        if code.text == ""{
           showError(text:MessagesManger.shared.emptyField)
            return
        }else{
            sendVerificationCode(email: user.data?.mobile ?? "", code: code.text!)
        }
    }
    
    
    
    func setupCode(){
        codeView.isHidden = false
        codeView.animate()
    }
    
    func setupResetPassword(){
//        codeView.x = -414
//        codeView.animateToNext {
//            self.emailView.animation = "slideLeft"
//            //            self.emailView.animate()
//        }
//        resetPass.isHidden = false
//        resetPass.animate()
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func  createVerificationCode(email : String){
        let header = APIs.sharedInstance.getHeader()
        let par = ["mobile": email
            ] as [String : Any]
        print(par)
//        HUD.show(.progress, onView: self.view)
         hud.show(in: self.view)
        Alamofire.request(APIs.sharedInstance.sendMobileCode(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                
//                HUD.hide()
                self.hud.dismiss()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img18"))
                        
                    }catch{
                        showError(text:MessagesManger.shared.checkYourData)
                    }
                    
                }else{
                    
                    do {
                        let temp = try JSONDecoder().decode(userData.self, from: response.data!)
                        showSucces(text: MessagesManger.shared.codeSent)
                        var _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCounter), userInfo: nil, repeats: true)
//                        self.setupCode()
                    }catch{
                        showError(text:MessagesManger.shared.tryAgainLater)
                    }
                }
            case .failure(_):
//                HUD.hide()
                self.hud.dismiss()
                showError(text:MessagesManger.shared.tryAgainLater)
                break
            }
        }
    }
    
    
    var counter = 600
    @objc func updateCounter() {
        //you code, this is an example
        if counter > 0 {
            let min = counter / 60
            let sec = counter % 60
            timerLbl.text = "\(min):\(sec)"
            counter -= 1
        }else{
            timerLbl.text = MessagesManger.shared.mustResendCode
        }
    }
    
    func  sendVerificationCode(email : String, code : String){
        let header = APIs.sharedInstance.getHeader()
        let par = ["mobile": email,
                   "code": code,
                   "onesignal-player-id":UserDefaults.standard.string(forKey: "onesignalid") ?? ""
            ] as [String : Any]
        print("ToDo -> add OnePlayerId")
        print(par)
//        HUD.show(.progress, onView: self.view)
         hud.show(in: self.view)
        Alamofire.request(APIs.sharedInstance.sendMobileActivationCode(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                
//                HUD.hide()
                self.hud.dismiss()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img18"))
                        
                    }catch{
                        showError(text:MessagesManger.shared.checkYourData)
                    }
                    
                }else{
                    
                    user.data?.verified = 1
                    user.saveUser()

                    showSucces(text: MessagesManger.shared.activated, completion: {
                        self.setupResetPassword()
                    })
                    
                }
            case .failure(_):
//                HUD.hide()
                self.hud.dismiss()
                showError(text: MessagesManger.shared.codeDontMatch)
                break
            }
        }
    }
    
//    func  sendNewPass(newPass : String, conNewPass : String, token: String){
//        let header = APIs.sharedInstance.getHeader()
//        let par = ["password": newPass,
//                   "password_confirmation": conNewPass,
//                   "token": token
//            ] as [String : Any]
//        print(par)
//        HUD.show(.progress, onView: self.view)
//        Alamofire.request(APIs.sharedInstance.resetPass(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
//            switch(response.result) {
//            case .success(let value):
//
//                HUD.hide()
//                print(value)
//                let temp = response.response?.statusCode ?? 400
//                if temp >= 300 {
//                    do {
//                        let temp = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)

//                        self.setupResetPassword()
//                        //                        self.dismiss(animated: true, completion: nil)
//                    }catch{
//
//                        HUD.flash(.labeledError(title: "", subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
//                    }
//                }else{
//
//                    do {
//                        let temp = try JSONDecoder().decode(userData.self, from: response.data!)
//                        HUD.flash(.labeledSuccess(title: "", subtitle: "تم تغيير كلمة الير بنجاح"), delay: 1.0)
//                        self.dismiss(animated: true, completion: nil)
//                    }catch{
//                        HUD.flash(.labeledError(title: "", subtitle: "حدث خطأ برجاء اعادة المحاولة"), delay: 1.0)
//                    }
//                }
//            case .failure(_):
//                HUD.hide()
//                HUD.flash(.labeledError(title: "", subtitle:"حدث خطأ برجاء إعادة المحاولة لاحقاً"), delay: 1.0)
//                break
//            }
//        }
//    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = false
        alert.colorScheme = secondaryColor
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: MessagesManger.shared.done, andButtons: nil)
    }
}
