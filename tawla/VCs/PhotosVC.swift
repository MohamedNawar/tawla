//
//  PhotosVC.swift
//  MoazEzzCVApp
//
//  Created by Moaz Ezz on 7/24/18.
//  Copyright © 2018 Moaz Ezz. All rights reserved.
//

import UIKit
import Auk
import moa

class PhotosVC: UIViewController {
    var imageArray = [String]()
    var imagesIndex = Int()
    
    var titleText = ""
    var priceLbl1 = ""
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    

    
    

    @IBOutlet weak var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if titleText == ""{
            titleLbl.isHidden = true
        }else{
            titleLbl.text = titleText
        }
        if priceLbl1 == ""{
            priceLbl.isHidden = true
        }else{
            priceLbl.text = priceLbl1
        }
        
        Moa.settings.cache.requestCachePolicy = .returnCacheDataElseLoad
        scrollView.auk.settings.preloadRemoteImagesAround = 4
        for i in imageArray{
            scrollView.auk.show(url: i)
        }
        scrollView.auk.scrollToPage(atIndex: imagesIndex, animated: false)
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        scrollView.auk.removeAll()
        for i in imageArray{
            scrollView.auk.show(url: i)
        }
    }

}
