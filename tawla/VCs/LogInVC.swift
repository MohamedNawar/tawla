//
//  SingInVC.swift
//  Dream Box
//
//  Created by Moaz Ezz on 11/17/17.
//  Copyright © 2017 Moaz Ezz. All rights reserved.
//

import UIKit
import TextFieldEffects
import FCAlertView
import Alamofire
import PKHUD
import Spring
import CountryPickerView
import JGProgressHUD
import LanguageManager_iOS

class LogInVC: UIViewController, UITextFieldDelegate, CountryPickerViewDelegate, CountryPickerViewDataSource {
    
    let hud = JGProgressHUD(style: .light)

    
    
    
    @IBOutlet weak var scrollView2: UIScrollView!
    var shouldBack = false
    var countryCodeHolder = "+966"
    var cpvInternal = CountryPickerView()
    @IBOutlet weak var mobile: UITextField!
    //@IBOutlet weak var mobile2: UITextField!
    @IBOutlet weak var password: UITextField!
    
    
    @IBAction func goToSignUp(_ sender: Any) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        navVC.shouldBack = shouldBack
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    
    @IBAction func diss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func forgetPass(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ResetVC") as! ResetVC
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    
    @IBOutlet weak var sendMessageOutlet: UIButton!
    
    @IBAction func sendMessage(_ sender: Any) {
        login()
    }

    
    
    @IBAction func closePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if !shouldBack{
            self.navigationItem.leftBarButtonItem = nil
        }
        password.delegate = self
        mobile.delegate = self
        cpvInternal.delegate = self
        cpvInternal.dataSource = self
        let cp = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 120, height: 20))

        cp.textColor = .white
        cp.delegate = self
        cp.dataSource = self
        mobile.leftView = cp
        mobile.leftViewMode = .always
        cp.setCountryByPhoneCode("+966")
        
        self.cpvInternal = cp
        
        if LanguageManager.shared.currentLanguage == .ar {
            mobile.textAlignment = .right
            mobile.semanticContentAttribute = .forceLeftToRight
            password.textAlignment = .right
        }else{
            mobile.textAlignment = .left
            password.textAlignment = .left
        }
//        mobile.text = "+966"
        
       
//        mobile.inputView = picker
        
    }
    
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        countryCodeHolder = country.phoneCode
        mobile.text = ""
        print(countryCodeHolder)
    }
    
    
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == mobile {
            
            if range.location == 0{
                if string == "0" {
                   showError(text: MessagesManger.shared.checkZeros)
                    return false
                }
            }
            
            let maxValue = 13 - countryCodeHolder.count
            if (textField.text?.length ?? 9) >= maxValue && string != ""{
                return false
            }
        }
        
        return true
    }
    
    
    

    
    override func viewWillAppear(_ animated: Bool) {
//        scrollView2.addSubview(mainView2)
//        scrollView2.contentSize = CGSize(width : mainView2.frame.size.width, height : mainView2.frame.size.height )
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
    }
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear

    }
    
    
    
    
    
    
    
   
    
    func login(){
        if countryCodeHolder == ""{
            makeDoneAlert(title:  MessagesManger.shared.checkPhoneCode, SubTitle: MessagesManger.shared.checkYourData, Image: #imageLiteral(resourceName: "img55"), color: UIColor.red)
            return
        }
        
        if mobile.text!.isEmpty == false && password.text!.isEmpty == false {

            
            let header = APIs.sharedInstance.getHeader()
//                        var tempText = mobile2.text! + mobile.text!
//                        tempText.remove(at: tempText.startIndex)
            let par = ["mobile": countryCodeHolder + mobile.text!,
                       "password": password.text!,
                       "onesignal-player-id":UserDefaults.standard.string(forKey: "onesignalid") ?? ""
            ]
            print(par)
            
//            HUD.show(.progress, onView: self.view)
            hud.show(in: self.view)

            Alamofire.request(APIs.sharedInstance.logIn(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
                switch(response.result) {
                case .success(let value):
                    print(response.value)
                    print(response.result.error)
                    print(response.response?.statusCode)
//                    HUD.hide()
                    self.hud.dismiss()

                    print(value)
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                       
                        do {
                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data ?? Data())
                            self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12") ,color: AlertColor)
                            
                        }catch{
                            showError(text: MessagesManger.shared.checkYourData)
                        }
                    }else{
                        
                        do {
                            user = try JSONDecoder().decode(userData.self, from: response.data!)
                          //  showSucces(text: user.message ?? "")
                            user.saveUser()
                            if user.data?.type == "customer" {
                                self.goToMain()

                            }else{
                                let nav = self.storyboard?.instantiateViewController(withIdentifier: "MyReservationsVC2") as! MyReservationsVC2
                    self.navigationController?.pushViewController(nav, animated: true)
                            }
                        }catch{
                            showError(text:MessagesManger.shared.tryAgainLater)
                        }
                    }
                case .failure(_):
//                    HUD.hide()
                    self.hud.dismiss()

                    showError(text:MessagesManger.shared.tryAgainLater)
                    break
                }
            }
        }else{
            showError(text: MessagesManger.shared.emptyField)
        }
    }
    
    func goToMain()  {
        if !user.isVerified() {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MobileActivationVC") as! MobileActivationVC
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        guard !shouldBack else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        let navVC = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        navVC.navigationItem.hidesBackButton = true
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage, color : UIColor) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        alert.colorScheme = mainColor
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle:  MessagesManger.shared.done, andButtons: nil)
    }
    
    
    
    
}
