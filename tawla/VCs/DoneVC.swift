//
//  DoneVC.swift
//  tawla
//
//  Created by Moaz Ezz on 9/3/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit

class DoneVC: UIViewController {
    var SelectedItem = ResOrCafe()
    var reservstion = Reservation()
    
    
    var isMenu = false
    @IBAction func NotNowBtn(_ sender: Any) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "MyReservationsVC2") as! MyReservationsVC2
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    
    @IBAction func gotoMain(_ sender: Any) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    @IBAction func nowBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
        vc.reservation = self.reservstion;
        vc.SelectedItem = self.SelectedItem; self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBOutlet weak var orderNumber: UILabel!
    @IBOutlet weak var mainLbl: UILabel!
    @IBOutlet weak var forlaterBtn: UIButton!
    @IBOutlet weak var nowBtnOutlet: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        user.fetchUser()
        if L102Language.currentAppleLanguage() == "ar" {
            mainLbl.text = "تم إرسال طلبك بنجاح و جاري مراجعته"
        }else{
             mainLbl.text = "Your Request has been sent successfully and is pending for approval"
        }
        if isMenu{
            nowBtnOutlet.isHidden = true
            forlaterBtn.setTitle(MessagesManger.shared.done, for: .normal)
        }
        print(MessagesManger.shared.orderNum + "  \(reservstion.number ?? -1)")
    orderNumber.text = MessagesManger.shared.orderNum + "  \(user.reserveId ?? 0)"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
