//
//  ResetVC.swift
//  Dream Box
//
//  Created by Moaz Ezz on 12/11/17.
//  Copyright © 2017 Moaz Ezz. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import Spring
import FCAlertView
import CountryPickerView
import JGProgressHUD
import LanguageManager_iOS
class ResetVC: UIViewController,CountryPickerViewDelegate, CountryPickerViewDataSource,UITextFieldDelegate  {
    var cpvInternal = CountryPickerView()
    var tokenMain = ""
    var token = userData()
    var countryCodeHolder = "+966"
    let hud = JGProgressHUD(style: .light)

    @IBOutlet weak var emailView: SpringView!
    
    @IBOutlet weak var codeView: SpringView!
    
    @IBOutlet weak var resetPass: SpringView!
   
    @IBAction func reseendCode(_ sender: Any) {
        createVerificationCode(email: countryCodeHolder + (phoneLbl.text ?? "" ))
    }
    //    @IBOutlet weak var codeLbl: UITextField!
    
    @IBOutlet weak var phoneLbl: UITextField!
    @IBOutlet weak var code: UITextField!
    
    @IBOutlet weak var sendCodeBtn: UIButton!
    
    @IBOutlet weak var newPass: UITextField!
    @IBOutlet weak var conNewPass: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cpvInternal.delegate = self
        cpvInternal.dataSource = self;
        phoneLbl.delegate = self
        let cp = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 120, height: 20))
        cp.textColor = mainColor
        cp.delegate = self
        cp.dataSource = self
        phoneLbl.leftView = cp
        phoneLbl.leftViewMode = .always
        cp.setCountryByPhoneCode("+966")
        self.cpvInternal = cp
        self.navigationController?.navigationBar.backgroundColor = secondaryColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
        view.viewOfType(type:UITextField.self) {
            view in
            if LanguageManager.shared.currentLanguage == .ar {
                view.textAlignment = .right
            }else{
                view.textAlignment = .left
                
            }
            
        }
        
        // Do any additional setup after loading the view.
    }
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        countryCodeHolder = country.phoneCode
        phoneLbl.text = ""
        print(countryCodeHolder)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = secondaryColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    
    @IBAction func done(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendEmail(_ sender: Any) {
        let textTemp = countryCodeHolder + phoneLbl.text!
        if countryCodeHolder == "" || phoneLbl.text == ""{
           showError(text:MessagesManger.shared.emptyField)
            return
        }else{
            createVerificationCode(email: textTemp)
        }
        
    }
    
    @IBAction func sendCode(_ sender: Any) {
        let textTemp = countryCodeHolder + phoneLbl.text!
        if countryCodeHolder == ""{
           showError(text:MessagesManger.shared.emptyField)
            return
        }else{
            sendVerificationCode(email: textTemp, code: code.text!)
        }
    }
    
    @IBAction func sendNewPass(_ sender: Any) {
        if newPass.text == "" || conNewPass.text == ""{
           showError(text:MessagesManger.shared.emptyField)
            return
        }else if newPass.text != conNewPass.text{
            showError(text: MessagesManger.shared.passwordDontMatch)
            return
        }else if token.token == "" || token.token == nil{
            showError(text: MessagesManger.shared.tryAgainLater) {
                self.dismiss(animated: true, completion: nil)
            }
            
        }else{
            sendNewPass(newPass: newPass.text!, conNewPass: conNewPass.text!, token: token.token!)
        }
    }
    
    func setupCode(){
        emailView.x = -414
        emailView.animateToNext {
            self.emailView.animation = "slideLeft"
//            self.emailView.animate()
        }
        codeView.isHidden = false
        codeView.animate()
    }
    
    func setupResetPassword(){
        codeView.x = -414
        codeView.animateToNext {
            self.emailView.animation = "slideLeft"
            //            self.emailView.animate()
        }
        resetPass.isHidden = false
        resetPass.animate()
    }
    
    func  createVerificationCode(email : String){
        let header = APIs.sharedInstance.getHeader()
        let par = ["mobile": email
            ] as [String : Any]
        print(par)
//        HUD.show(.progress, onView: self.view)
        hud.show(in: self.view)
        Alamofire.request(APIs.sharedInstance.forget(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                
//                HUD.hide()
                self.hud.dismiss()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img18"))
                        
                    }catch{
                        showError(text:MessagesManger.shared.checkYourData)
                    }

                }else{
                    
                    do {
                        let temp = try JSONDecoder().decode(userData.self, from: response.data!)
                        showSucces(text: temp.message ?? "")
                        self.setupCode()
                    }catch{
                        showError(text:MessagesManger.shared.tryAgainLater)
                    }
                }
            case .failure(_):
//                HUD.hide()
                self.hud.dismiss()
                showError(text:MessagesManger.shared.tryAgainLater)
                break
            }
        }
    }
    
    func  sendVerificationCode(email : String, code : String){
        let header = APIs.sharedInstance.getHeader()
        let par = ["mobile": email,
                   "code": code
            ] as [String : Any]
        print(par)
//        HUD.show(.progress, onView: self.view)
        hud.show(in: self.view)
        Alamofire.request(APIs.sharedInstance.checkCode(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                
//                HUD.hide()
                self.hud.dismiss()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img18"))
                        
                    }catch{
                        showError(text:MessagesManger.shared.checkYourData)
                    }

                }else{
                    
                    do {
                        self.token = try JSONDecoder().decode(userData.self, from: response.data!)
                        showSucces(text: self.token.message ?? "")
                        self.setupResetPassword()
                    }catch{
                        showError(text:MessagesManger.shared.tryAgainLater)
                    }
                }
            case .failure(_):
//                HUD.hide()
                self.hud.dismiss()
                showError(text: MessagesManger.shared.codeDontMatch)
                break
            }
        }
    }

    func  sendNewPass(newPass : String, conNewPass : String, token: String){
        if self.newPass.text != self.conNewPass.text {
            HUD.flash(.label(NSLocalizedString("Password not identical", comment: "حدث خطأ برجاء اعادة المحاولة")), delay: 1.0)
            return
            
        }
        if self.newPass.text?.count ?? 0 < 6 {
            HUD.flash(.label(NSLocalizedString("The Password must be 6 digits", comment: "حدث خطأ برجاء اعادة المحاولة")), delay: 1.0)
            return
            
        }

        let header = APIs.sharedInstance.getHeader()
        let par = ["password": newPass,
                   "password_confirmation": conNewPass,
                   "token": token
            ] as [String : Any]
        print(par)
//        HUD.show(.progress, onView: self.view)
        hud.show(in: self.view)
        Alamofire.request(APIs.sharedInstance.resetPass(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                
//                HUD.hide()
                self.hud.dismiss()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let temp = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        showError(text: temp.message ?? "")
                        self.setupResetPassword()
                        //                        self.dismiss(animated: true, completion: nil)
                    }catch{
                        
                        showError(text:MessagesManger.shared.checkYourData)
                    }
                }else{
                    
                    do {
                        let temp = try JSONDecoder().decode(userData.self, from: response.data!)
                        showSucces(text: NSLocalizedString("The Password had been changed successfully", comment: ""), completion: {
                            self.dismiss(animated: true, completion: nil)
                        })
                        
                    }catch{
                        showError(text:MessagesManger.shared.tryAgainLater)
                    }
                }
            case .failure(_):
//                HUD.hide()
                self.hud.dismiss()
                showError(text:MessagesManger.shared.tryAgainLater)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = false
        alert.colorScheme = mainColorGRAD
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: MessagesManger.shared.done, andButtons: nil)
    }
    
    
    
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == phoneLbl {
            
            if range.location == 0{
                if string == "0" {
                    showError(text: MessagesManger.shared.checkZeros)
                    return false
                }
            }
            
            
            let maxValue = 13 - countryCodeHolder.count
            print(phoneLbl.text)
            if (textField.text?.length ?? 9) >= maxValue && string != ""{
                return false
            }
        }
        
        return true
    }
}
