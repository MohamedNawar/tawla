//
//  MenuVC.swift
//  tawla
//
//  Created by Moaz Ezz on 8/5/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import JGProgressHUD
class MenuVC: UIViewController {
    var SelectedItem = ResOrCafe()
    var menuItems = [MenuItem]()
    var selectedMenuItems = [MenuItem]()
    var reservation = Reservation()
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var subTotal = Int(){
        didSet{
            totalPrice.text = "\(subTotal)"
        }
    }
    
    let hud = JGProgressHUD(style: .light)

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        getMenus()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    @IBAction func orderDetails(_ sender: Any) {
        selectedMenuItems.removeAll()
        for i in menuItems {
            if i.quentity != nil && i.quentity != 0{
                selectedMenuItems.append(i)
            }
        }
        if selectedMenuItems.isEmpty {
            showError(text: MessagesManger.shared.chosseMenu)
            return
        }
        let vc = storyboard?.instantiateViewController(withIdentifier: "SummeryVC") as! SummeryVC
        vc.SelectedMenuItems = selectedMenuItems;
        vc.place = SelectedItem;
        vc.reservation = reservation;
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    private func getMenus(){
        let header = APIs.sharedInstance.getHeader()
        
//        HUD.show(.progress, onView: self.view)
         hud.show(in: self.view)
        Alamofire.request(APIs.sharedInstance.getMenu(id: SelectedItem.id!), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
                
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
//                        HUD.hide()
                        self.hud.dismiss()
                        self.makeDoneAlert(title: MessagesManger.shared.checkYourData, SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "img12"))
                        
                           
                    }catch{
                        showError(text:MessagesManger.shared.checkYourData)
                    }
                    
                }else{
//                    HUD.hide()
                    self.hud.dismiss()
                    print(value)
                    if let temp = value as? Dictionary<String,AnyObject>{
                        if let temp2 = temp["data"]{
                            
                            
                            do {
                                let temp3 = try JSONSerialization.data(withJSONObject: temp2, options: .prettyPrinted)
                                self.menuItems = try JSONDecoder().decode([MenuItem].self, from: temp3 )
                                self.tableView.reloadData()
                            }catch{
//                                HUD.hide()
                                self.hud.dismiss()
                                showError(text:MessagesManger.shared.tryAgainLater)
                            }
                        }
                    }
                }
            case .failure(_):
//                HUD.hide()
                self.hud.dismiss()
                showError(text:MessagesManger.shared.tryAgainLater)
                break
            }
            
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "تم", andButtons: nil)
    }
}

extension MenuVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableView1Cell", for: indexPath) as! OrderTableViewCell
        cell.ContentName.text = menuItems[indexPath.row].name
        cell.price.text = menuItems[indexPath.row].price
        cell.contentIngredient.text = menuItems[indexPath.row].details
        let url = URL(string: menuItems[indexPath.row].image ?? "")
        cell.contentImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "img02"), options: .highPriority, completed: nil)
        cell.quentityChanged = { () in
            self.setupTotal(index: indexPath.row,quentity: cell.quentity)
        }
        return cell
    }
    
    private func setupTotal(index:Int,quentity:Int){
        menuItems[index].quentity = quentity
        self.subTotal = 0
        for i in menuItems{
            if let temp = i.quentity{
                self.subTotal += Int(i.price ?? "0")! * temp
            }
        }
    }
}
