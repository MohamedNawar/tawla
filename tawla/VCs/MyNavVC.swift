//
//  ViewController.swift
//  tawla
//
//  Created by Moaz Ezz on 7/26/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit

class MyNavVC: UINavigationController {
    var selectedPlace = Place()
    override func viewDidLoad() {
        let vc = storyboard?.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        vc.SelectedPlace = selectedPlace
        self.pushViewController(vc, animated: true)
    }

    

}

