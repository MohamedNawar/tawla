//
//  ProfileVC.swift
//  NewArt
//
//  Created by Moaz Ezz on 5/9/18.
//  Copyright © 2018 elnooronline. All rights reserved.
//

import UIKit
import FCAlertView
import Alamofire
import PKHUD
import JGProgressHUD
import LanguageManager_iOS
class ProfileVC: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!

    let hud = JGProgressHUD(style: .light)

    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var oldPass: UITextField!
    @IBOutlet weak var newPass: UITextField!
    
    
    @IBOutlet weak var oldPassLbl: UILabel!
    
    @IBOutlet weak var newPassLbl: UILabel!
    @IBOutlet weak var sendOutlet: UIButton!
    override func viewDidLoad() {
        view.viewOfType(type:UITextField.self) {
            view in
            if LanguageManager.shared.currentLanguage == .ar {
                view.textAlignment = .right
            }else{
                view.textAlignment = .left
                
            }
            
        }

    }
    @IBAction func sendMessage(_ sender: Any) {
        postMessage()
    }
    
    
    @IBOutlet weak var editBtnOutlet: UIButton!
    @IBAction func Edit(_ sender: Any) {
        if editBtnOutlet.tag == 0{// now is editable
            
//            UIView.animate(withDuration: 0.2, delay: 0, options: [], animations: {
//                self.editView.isHidden = false
//                self.editView.alpha = 1 // Here you will get the animation you want
//            }, completion: { _ in
//                // Here you hide it when animation done
//            })
            oldPass.isHidden = false
            newPass.isHidden = false
            sendOutlet.isHidden = false
            oldPassLbl.isHidden = false
            newPassLbl.isHidden = false
            editBtnOutlet.tag = 1
        }else{
            
//            UIView.animate(withDuration: 0.2, delay: 0, options: [], animations: {
//                self.editView.alpha = 0 // Here you will get the animation you want
//            }, completion: { _ in
//                self.editView.isHidden = true // Here you hide it when animation done
//            })
            oldPass.isHidden = true
            newPass.isHidden = true
            sendOutlet.isHidden = true
            oldPassLbl.isHidden = true
            newPassLbl.isHidden = true
            editBtnOutlet.tag = 0
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        getUser()
        name.text = user.data?.name
        email.text = user.data?.email
        phone.text = user.data?.mobile
        oldPass.isHidden = true
        newPass.isHidden = true
        sendOutlet.isHidden = true
        oldPassLbl.isHidden = true
        newPassLbl.isHidden = true
        
    }
    
    
    
    func postMessage(){
        if name.text!.isEmpty == false && email.text!.isEmpty == false && phone.text!.isEmpty == false {
            let header = APIs.sharedInstance.getHeader()
            let par = [
                       "old_password": oldPass.text!,
                       "password": newPass.text!]
//            HUD.show(.progress)
            hud.show(in: self.view)
            Alamofire.request(APIs.sharedInstance.profile(), method: .patch, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
                switch(response.result) {
                case .success(let value):
//                    HUD.hide()
                    self.hud.dismiss()
                    print(value)
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                        
                        do {
                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        
                            showError(text: err.parseError())

                            //                            user.saveUser(user: user)
                        }catch{
                            showError(text:MessagesManger.shared.checkYourData)
                        }
                    }else{
                        
                        HUD.flash(.success , delay: 1.0)
                        self.name.isEnabled = false
                        self.email.isEnabled = false
                        self.phone.isEnabled = false
//                        self.editView.isHidden = true
                        self.oldPass.text = ""
                        self.newPass.text = ""
                        self.editBtnOutlet.tag = 0
                        
                    }
                case .failure(let error):
                    print(error)
//                    HUD.hide()
                    self.hud.dismiss()

                    showError(text:MessagesManger.shared.tryAgainLater)
                    break
                }
            }
        }else{
            showError(text: MessagesManger.shared.emptyField)
        }
    }
    
    private func getUser(){
        let header = APIs.sharedInstance.getHeader()
        
        
        
//        HUD.show(.progress)
        hud.show(in: self.view)
        Alamofire.request(APIs.sharedInstance.profile(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(response.value)
                print(response.result.error)
                print(response.response?.statusCode)
//                HUD.hide()
                self.hud.dismiss()

                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        showError(text:MessagesManger.shared.tryAgainLater)
                       
                        //                            user.saveUser(user: user)
                    }catch{
                        showError(text:MessagesManger.shared.checkYourData)
                    }
                }else{
                    
                    do {
                        var userTemp = try JSONDecoder().decode(userData.self, from: response.data!)

//                        user.data?.name = userTemp.data?.name
//                        user.data?.email = userTemp.data?.email
////                        user.data?.balance = userTemp.data?.balance
//                        user.saveUser()
//                        self.name.text = user.data?.name
//                        self.email.text = user.data?.email
//                        self.phone.text = user.data?.mobile
                        
                    }catch{
                        showError(text:MessagesManger.shared.tryAgainLater)
                    }
                }
            case .failure(_):
//                HUD.hide()
                self.hud.dismiss()
                showError(text:MessagesManger.shared.tryAgainLater)
                break
            }
        }
    }
    

}
