//
//  HeaderCell.swift
//  tawla
//
//  Created by Moaz Ezz on 8/5/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit
import LanguageManager_iOS
class HeaderCell: UITableViewCell {
    var res = Reservation2(){
        didSet{
            //            let url2 = URL(string: res.place?.image ?? "")
            //            mainImage.sd_setImage(with: url2, placeholderImage: #imageLiteral(resourceName: "img02"), options: .highPriority, completed: nil)
            //            titleLbl.text = res.place?.name
            //            date.text = res.date
            //
            //            if res.isExpanded ?? false{
            //                ArrowImg.setFAIconWithName(icon: .FAAngleUp, textColor: #colorLiteral(red: 0.4126763344, green: 0.4591623545, blue: 0.5150957704, alpha: 1))
            //            }else{
            //                ArrowImg.setFAIconWithName(icon: .FAAngleDown, textColor: #colorLiteral(red: 0.4126763344, green: 0.4591623545, blue: 0.5150957704, alpha: 1))
            //            }
            //            switch res.status {
            //            case "approved":
            //                cancelOutlet.isHidden = false
            //                btnsStack.isHidden = true
            //                adress.text = res.status
            //                adress.textColor = .green
            //            case "rejected":
            //                cancelBtnHeight.constant = 0
            //                cancelOutlet.isHidden = true
            //                btnsStack.isHidden = true
            //                adress.text = res.status
            //                adress.textColor = .red
            //            case "pendding":
            //                cancelOutlet.isHidden = true
            //                btnsStack.isHidden = false
            //                adress.text = res.status
            //                adress.textColor = .black
            //            default:
            //                cancelBtnHeight.constant = 0
            //                cancelOutlet.isHidden = true
            //                btnsStack.isHidden = true
            //                adress.text = res.status
            //                adress.textColor = .blue
            //            }
            //
            //
            //            if user.data?.type != "place_owner"{
            //                cancelBtnHeight.constant = 0
            //                cancelOutlet.isHidden = true
            //                btnsStack.isHidden = true
            //            }
        }
    }
    var cancelAction : (() -> (Void))?
    var approveAction : (() -> (Void))?
    var rejectAction : (() -> (Void))?
    var cellAction : (() -> (Void))?
    
    var isExpanded = false
    @IBOutlet weak var mainImage: UIImageView!
    
    @IBOutlet weak var cancelOutlet: UIButton!
    @IBOutlet weak var approvelOutlet: UIButton!
    @IBOutlet weak var rejectOutlet: UIButton!
    @IBOutlet weak var btnsStack: UIStackView!
    
    @IBAction func cellpressed(_ sender: Any) {
        
        if res.isExpanded ?? false{
            ArrowImg.setFAIconWithName(icon: .FAAngleDown, textColor: #colorLiteral(red: 0.4126763344, green: 0.4591623545, blue: 0.5150957704, alpha: 1))
        }else{
            ArrowImg.setFAIconWithName(icon: .FAAngleUp, textColor: #colorLiteral(red: 0.4126763344, green: 0.4591623545, blue: 0.5150957704, alpha: 1))
        }
        cellAction?()
    }
    
    
    @IBAction func cancelPressed(_ sender: Any) {
        cancelAction?()
    }
    
    @IBAction func rejectPressed(_ sender: Any) {
        rejectAction?()
    }
    
    @IBAction func approvePressed(_ sender: Any) {
        approveAction?()
    }
    
    //    @IBOutlet weak var cancelBtnHeight: NSLayoutConstraint!
    
    @IBAction func phoneNum(_ sender: Any) {
        if let mobile = res.customer?.mobile {
        guard let number = URL(string: "tel://" + mobile) else { return }
        UIApplication.shared.open(number)
        }
    }
    @IBOutlet weak var phoneNumber: UIButton!
    @IBOutlet weak var customerName: UIButton!
    @IBOutlet weak var stackBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var adress: UILabel!
    @IBOutlet weak var ArrowImg: UIImageView!
    @IBOutlet weak var rejectLbl: UIButton!
    @IBOutlet weak var cancelLbl: UIButton!
    @IBOutlet weak var approveLbl: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        rejectLbl.setTitle(NSLocalizedString("Reject", comment:"رفض"), for: .normal)
        cancelLbl.setTitle(NSLocalizedString("Cancel", comment:"الغاء"), for: .normal)
        approveLbl.setTitle(NSLocalizedString("Aprrove", comment:"قبول"), for: .normal)

    }
    
    func SetupUI(res:Reservation2)  {
        
        btnsStack.isHidden = false
        cancelOutlet.isHidden = false
        approvelOutlet.isHidden = false
        rejectOutlet.isHidden = false
        self.res = res
        let url2 = URL(string: res.place?.image ?? "")
        mainImage.sd_setImage(with: url2, placeholderImage: #imageLiteral(resourceName: "img02"), options: .highPriority, completed: nil)
        titleLbl.text = res.place?.name
        date.text = res.created_at
        date.textAlignment = .right
        phoneNumber.setTitle(res.customer?.mobile ?? "", for: .normal)
        customerName.setTitle(res.customer?.name ?? "", for: .normal)
        if res.isExpanded ?? false{
            ArrowImg.setFAIconWithName(icon: .FAAngleUp, textColor: #colorLiteral(red: 0.4126763344, green: 0.4591623545, blue: 0.5150957704, alpha: 1))
        }else{
            ArrowImg.setFAIconWithName(icon: .FAAngleDown, textColor: #colorLiteral(red: 0.4126763344, green: 0.4591623545, blue: 0.5150957704, alpha: 1))
        }
        
        
        if user.data?.type == "place_owner" {
            
            switch res.status {
            case "approved":
                cancelOutlet.isHidden = false
                approvelOutlet.isHidden = true
                rejectOutlet.isHidden = true
                //            btnsStack.isHidden = true
                if L102Language.currentAppleLanguage() == "ar" {
                    adress.text = "مقبول"
                }else{
                    adress.text = res.status

                }
                adress.textColor = .green
            case "rejected":
                //            cancelOutlet.isHidden = true
                btnsStack.isHidden = true
                if L102Language.currentAppleLanguage() == "ar" {
                    adress.text = "مرفوض"
                }else{
                    adress.text = res.status
                    
                }
                adress.textColor = .red
            case "pendding":
                cancelOutlet.isHidden = true
                approvelOutlet.isHidden = false
                rejectOutlet.isHidden = false
                if L102Language.currentAppleLanguage() == "ar" {
                    adress.text = "قيد الأنتظار"
                }else{
                    adress.text = res.status
                    
                }
                adress.textColor = .black
            default:
                //            cancelOutlet.isHidden = true
                btnsStack.isHidden = true
                if L102Language.currentAppleLanguage() == "ar" {
                    adress.text = "ألغي"
                }else{
                    adress.text = res.status
                    
                }
                adress.textColor = .black
            }
            
        }else{
            switch res.status {
            case "approved":
                if L102Language.currentAppleLanguage() == "ar" {
                    adress.text = "مقبول"
                }else{
                    adress.text = res.status
                }
            case "rejected":
                 if L102Language.currentAppleLanguage() == "ar" {
                    adress.text = "مرفوض"
                }else{
                    adress.text = res.status
                }
            case "pendding":
            if L102Language.currentAppleLanguage() == "ar" {
                    adress.text = "قيد الأنتظار"
                }else{
                    adress.text = res.status
                }
            default:
            if L102Language.currentAppleLanguage() == "ar" {
                    adress.text = "ألغي"
                }else{
                    adress.text = res.status
                }
            }
            btnsStack.isHidden = true
            adress.textColor = .black
        }
        if LanguageManager.shared.currentLanguage == .ar {
            adress.textAlignment = .right
        }else{
            adress.textAlignment = .left
            
        }

        
        //        if user.data?.type != "place_owner"{
        //            cancelBtnHeight.constant = 0
        //            cancelOutlet.isHidden = true
        //            btnsStack.isHidden = true
        //        }
        
    }
    
}
