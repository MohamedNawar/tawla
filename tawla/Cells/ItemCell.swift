//
//  ItemCell.swift
//  tawla
//
//  Created by Moaz Ezz on 7/17/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit
import HCSStarRatingView
import LanguageManager_iOS
class ItemCell: UITableViewCell {

    @IBOutlet weak var mainImage: UIImageView!
    
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var timLbl: UILabel!
    
    @IBOutlet weak var locationLbl: UILabel!
    
    @IBOutlet weak var rate: HCSStarRatingView!
    override func awakeFromNib() {
        if LanguageManager.shared.currentLanguage == .ar {
            titleLbl.textAlignment = .right
            timLbl.textAlignment = .right
            locationLbl.textAlignment = .right
        }else{
            titleLbl.textAlignment = .left
            timLbl.textAlignment = .left
            locationLbl.textAlignment = .left
        }
    }
}
