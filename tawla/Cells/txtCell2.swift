//
//  txtCell2.swift
//  tawla
//
//  Created by Moaz Ezz on 8/5/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit

class txtCell2: UITableViewCell {
    
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var orderNum: UILabel!
    
    @IBOutlet weak var positionImg: UIImageView!
    @IBOutlet weak var positionLbl: UILabel!
    @IBOutlet weak var notesLbl: UILabel!

    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    public func setupUI(res:Reservation2) {
        timeLbl.text = res.start_at
        orderNum.text = String(res.id ?? 0)
        let url = URL(string: res.table?.table_type?.image ?? "")
        positionImg.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "img18"), options: .highPriority, completed: nil)
        if L102Language.currentAppleLanguage() == "ar" {
            notesLbl.text = " طلبات خاصة: \n \(res.notes ?? "")"
        }else{
            notesLbl.text = "Special Requests: \n \(res.notes ?? "")"
        }
        positionLbl.text = res.date
        //        dateLbl.text = res.date
        //        let url2 = URL(string: res.table?.table_type?.image ?? "")
        //        tableShapeImg.sd_setImage(with: url2, placeholderImage: #imageLiteral(resourceName: "img02"), options: .highPriority, completed: nil)
        //        tableShapeLbl.text = res.table?.table_type?.name
        //        manLbl.text = String(res.men_count ?? 0)
        //        boyLbl.text = String(res.children_count ?? 0)
        //        womanLbl.text = String(res.women_count ?? 0)
        
        
    }
}
