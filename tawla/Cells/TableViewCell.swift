//
//  TableViewCell.swift
//  examp
//
//  Created by MACBOOK on 7/21/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import LanguageManager_iOS
class SummryCell: UITableViewCell {

    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var numberOfOrders: UILabel!
    @IBOutlet weak var name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        if LanguageManager.shared.currentLanguage == .ar {
            numberOfOrders.textAlignment = .right
            price.textAlignment = .right
            name.textAlignment = .right
        }else{
            numberOfOrders.textAlignment = .left
            price.textAlignment = .left
            name.textAlignment = .left
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
