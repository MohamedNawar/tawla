//
//  txtCell.swift
//  tawla
//
//  Created by Moaz Ezz on 8/5/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit
import LanguageManager_iOS
class txtCell: UITableViewCell {
    
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var orderNum: UILabel!
    @IBOutlet var orderNumberLbl: UILabel!
    
    @IBOutlet weak var positionImg: UIImageView!
    @IBOutlet weak var positionLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
   @IBOutlet weak var curtainsLbl: UILabel!
    @IBOutlet weak var curtainsTitle: UILabel!
    @IBOutlet weak var tableShapeImg: UIImageView!
    
    @IBOutlet weak var tableShapeLbl: UILabel!
    
    @IBOutlet weak var manLbl: UILabel!
    
    @IBOutlet weak var total: UILabel!
//    @IBOutlet weak var seviceLbl: UILabel!
//    @IBOutlet weak var taxLbl: UILabel!
    @IBOutlet weak var getheringLbl: UILabel!
    @IBOutlet weak var getheringImg: UIImageView!
    @IBOutlet weak var notesLbl: UILabel!
    
    
    @IBOutlet weak var boyLbl: UILabel!
    @IBOutlet weak var womanLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if LanguageManager.shared.currentLanguage == .ar {
            orderNum.textAlignment = .right
            orderNumberLbl.textAlignment = .right
        }else{
            orderNum.textAlignment = .left
            orderNumberLbl.textAlignment = .left
            
        }
        // Initialization code
    }
    
    public func setupUI(res:Reservation2) {
        timeLbl.text = res.start_at
        orderNum.text = String(res.id ?? 0)
        let url = URL(string: res.table?.table_type?.image ?? "")
        positionImg.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "img18"), options: .highPriority, completed: nil)
        if res.gathering_type?.name == "Reservation for two persons" {
             getheringLbl.text = "Reservation for two persons"
            getheringLbl.font = UIFont(name: "Cairo-Regular" , size: 8.0)
        }else{
            getheringLbl.text = res.gathering_type?.name ?? ""

        }
         getheringLbl.textAlignment = .center
        positionLbl.text = res.table?.position
        dateLbl.text = res.date
        let url2 = URL(string: res.table?.table_type?.image ?? "")
        tableShapeImg.sd_setImage(with: url2, placeholderImage: #imageLiteral(resourceName: "img02"), options: .highPriority, completed: nil)
        if L102Language.currentAppleLanguage() == "ar" {
           notesLbl.text = " طلبات خاصة: \n \(res.notes ?? "")"
        }else{
           notesLbl.text = "Special Requests: \n \(res.notes ?? "")"
        }
        tableShapeLbl.text = res.table?.table_type?.name
        if L102Language.currentAppleLanguage() == "ar" {
            manLbl.text = "رجال \n" + String(res.men_count ?? 0)
            boyLbl.text = "أطفال \n" + String(res.children_count ?? 0)
            womanLbl.text = "نساء \n" + String(res.women_count ?? 0)
             curtainsTitle.text = "نوع الطاولة"
            if let barrier = res.table?.has_barrier {
                if barrier == 0 {
                   
                    curtainsLbl.text =   "طاولة بدون ستائر"
                }else{
                      curtainsLbl.text =  "طاولة بستائر"
                }
            }
        }else{
            manLbl.text = "Man \n" + String(res.men_count ?? 0)
            boyLbl.text = "Children \n" + String(res.children_count ?? 0)
            womanLbl.text = "woman \n" + String(res.women_count ?? 0)
            curtainsTitle.text = "Table Type"
            if let barrier = res.table?.has_barrier {
                if  res.table?.has_barrier ?? 0 == 0 {
                    print(barrier)
                    curtainsLbl.text = "Table without Curtains"
                }else{
                     curtainsLbl.text = "Table with Curtains"
                }
            }
        }
     
    }
}
