//
//  CommentCell.swift
//  tawla
//
//  Created by Moaz Ezz on 7/21/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import UIKit
import HCSStarRatingView

class CommentCell: UITableViewCell {

    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var rate: HCSStarRatingView!
    
}
