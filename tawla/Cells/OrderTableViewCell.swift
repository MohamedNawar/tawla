//
//  OrderTableViewCell.swift
//  examp
//
//  Created by MACBOOK on 8/5/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import LanguageManager_iOS
class OrderTableViewCell: UITableViewCell {
    
    var quentityChanged:(()-> Void)!

    @IBOutlet weak var numberOfOrder: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var contentIngredient: UILabel!
    @IBOutlet weak var ContentName: UILabel!
    @IBOutlet weak var contentImage: UIImageView!
    var quentity = Int(){
        didSet{
            numberOfOrder.text = "\(quentity)"
            quentityChanged()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        if LanguageManager.shared.currentLanguage == .ar {
            numberOfOrder.textAlignment = .right
            price.textAlignment = .right
            contentIngredient.textAlignment = .right
            ContentName.textAlignment = .right
        }else{
            numberOfOrder.textAlignment = .left
            price.textAlignment = .left
            contentIngredient.textAlignment = .left
            ContentName.textAlignment = .left
            
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func addOne(_ sender: Any) {
        
        quentity += 1
        
    }
    @IBAction func removeOne(_ sender: Any) {
        
        quentity -= 1
        if quentity < 0 {
            quentity = 0
        }
    }
    
}
