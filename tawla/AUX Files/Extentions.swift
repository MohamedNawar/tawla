//
//  Extentions.swift
//  tawla
//
//  Created by Moaz Ezz on 7/15/18.
//  Copyright © 2018 Elnooronline. All rights reserved.
//

import Foundation
import UIKit
import FCAlertView
public extension UIWindow {
    
    /** @return Returns the current Top Most ViewController in hierarchy.   */
    public func topMostVC()->UIViewController? {
        
        var topController = rootViewController
        
        while let presentedController = topController?.presentedViewController {
            topController = presentedController
        }
        
        return topController
    }
    
    /** @return Returns the topViewController in stack of topMostController.    */
    public func currentViewController()->UIViewController? {
        
        var currentViewController = topMostVC()
        
        while currentViewController != nil && currentViewController is UINavigationController && (currentViewController as! UINavigationController).topViewController != nil {
            currentViewController = (currentViewController as! UINavigationController).topViewController
        }
        
        return currentViewController
    }
}
public func  showError(text:String) {
    let alert = FCAlertView()
    alert.makeAlertTypeCaution()
    alert.avoidCustomImageTint = true
    
    alert.colorScheme = mainColorGRAD
    alert.showAlert(withTitle: MessagesManger.shared.error, withSubtitle: text, withCustomImage: nil, withDoneButtonTitle: MessagesManger.shared.done, andButtons: nil)
}
public func showError(text:String,completion:@escaping ()->()) {
    let alert = FCAlertView()
    alert.makeAlertTypeCaution()
    alert.avoidCustomImageTint = true
    alert.doneBlock = completion
    alert.colorScheme = mainColorGRAD
    alert.showAlert(withTitle: MessagesManger.shared.error, withSubtitle: text, withCustomImage: nil, withDoneButtonTitle: MessagesManger.shared.done, andButtons: nil)
}
public func  showSucces(text:String) {
    let alert = FCAlertView()
    alert.makeAlertTypeSuccess()
    alert.avoidCustomImageTint = true
    
    alert.colorScheme = mainColorGRAD
    alert.showAlert(withTitle: MessagesManger.shared.success, withSubtitle: text, withCustomImage: nil, withDoneButtonTitle: MessagesManger.shared.done, andButtons: nil)
}
public func showSucces(text:String,completion:@escaping ()->()) {
    let alert = FCAlertView()
    alert.makeAlertTypeSuccess()
    alert.avoidCustomImageTint = true
    alert.doneBlock = completion
    alert.colorScheme = mainColorGRAD
    alert.showAlert(withTitle: MessagesManger.shared.success, withSubtitle: text, withCustomImage: nil, withDoneButtonTitle: MessagesManger.shared.done, andButtons: nil)
}
@IBDesignable
class DesignableUITextField: UITextField {
    
    // Provides right padding for images
    // Provides left padding for images
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRect(forBounds: bounds)
        textRect.origin.x += leftPadding
        return textRect
    }
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var leftPadding: CGFloat = 0
    
    @IBInspectable var color: UIColor = UIColor.lightGray {
        didSet {
            updateView()
        }
    }
    
    func updateView() {
        if let image = leftImage {
            leftViewMode = UITextFieldViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 20))
            imageView.contentMode = .scaleAspectFit
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = color
            leftView = imageView
        } else {
            leftViewMode = UITextFieldViewMode.never
            leftView = nil
        }
        
        // Placeholder text color
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: color])
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.lightGray {
        didSet {
            updateBorder()
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet {
            updateBorder()
        }
    }
    
    @IBInspectable var borderRadius: CGFloat = 0.0 {
        didSet {
            updateBorder()
        }
    }
    
    
    
    func updateBorder(){
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = borderRadius
    }
}

@IBDesignable
class DesignableUIButton: UIButton {
    
    // Provides left padding for images
    
    
    
    
    
    
    @IBInspectable var borderColor: UIColor = UIColor.lightGray {
        didSet {
            updateBorder()
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet {
            updateBorder()
        }
    }
    
    @IBInspectable var borderRadius: CGFloat = 0.0 {
        didSet {
            updateBorder()
        }
    }
    
    func updateBorder(){
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = borderRadius
    }
}


@IBDesignable
class DesignableUIView: UIView {
    
    @IBInspectable var borderColor: UIColor = UIColor.lightGray {
        didSet {
            updateBorder()
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet {
            updateBorder()
        }
    }
    
    @IBInspectable var borderRadius: CGFloat = 0.0 {
        didSet {
            updateBorder()
        }
    }
    
    func updateBorder(){
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = borderRadius
    }
    
}
extension UITableView {
    
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .darkGray
        messageLabel.numberOfLines = 0;
        messageLabel.tag = -1
        messageLabel.textAlignment = .center;
        messageLabel.font = BoldFont?.withSize(20)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel;
        self.separatorStyle = .none;
    }
    
    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}
extension UIView {
    func viewOfType<T:UIView>(type:T.Type, process: (_ view:T) -> Void)
    {
        if let view = self as? T
        {
            process(view)
        }
        else {
            for subView in subviews
            {
                subView.viewOfType(type:type, process:process)
            }
        }
    }
}

private var kBundleKey: UInt8 = 0
class BundleEx: Bundle {
    
    override func localizedString(forKey key: String, value: String?, table tableName: String?) -> String {
        if let bundle = objc_getAssociatedObject(self, &kBundleKey) {
            return (bundle as! Bundle).localizedString(forKey: key, value: value, table: tableName)
        }
        return super.localizedString(forKey: key, value: value, table: tableName)
    }
    
}

extension Bundle {
    
    static let once: Void = {
        object_setClass(Bundle.main, type(of: BundleEx()))
    }()
    
    class func setLanguage(_ language: String?) {
        Bundle.once
        let isLanguageRTL = Bundle.isLanguageRTL(language)
        if (isLanguageRTL) {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        } else {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
        UserDefaults.standard.set(isLanguageRTL, forKey: "AppleTextDirection")
        UserDefaults.standard.set(isLanguageRTL, forKey: "NSForceRightToLeftWritingDirection")
        UserDefaults.standard.synchronize()
        
        let value = (language != nil ? Bundle.init(path: (Bundle.main.path(forResource: language, ofType: "lproj"))!) : nil)
        objc_setAssociatedObject(Bundle.main, &kBundleKey, value, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    
    class func isLanguageRTL(_ languageCode: String?) -> Bool {
        return (languageCode != nil && Locale.characterDirection(forLanguage: languageCode!) == .rightToLeft)
    }
    
}
