import Foundation
import UIKit



    @IBDesignable
class ThreeColorsGradientView: UIButton {
        var firstColor: UIColor = #colorLiteral(red: 0.9960784314, green: 0.8274509804, blue: 0.3176470588, alpha: 1)
        var secondColor: UIColor = #colorLiteral(red: 0.7763102651, green: 0.642593801, blue: 0.08450616151, alpha: 1)
        var thirdColor: UIColor = #colorLiteral(red: 0.9960784314, green: 0.8274509804, blue: 0.3176470588, alpha: 1)
        
        @IBInspectable var vertical: Bool = false {
            didSet {
                updateGradientDirection()
            }
        }
        @IBInspectable var cornerRadius: CGFloat = 0.0 {
            didSet {
                updateGradientDirection()
            }
        }
        
        lazy var gradientLayer: CAGradientLayer = {
            let layer = CAGradientLayer()
            layer.colors = [firstColor.cgColor, secondColor.cgColor, thirdColor.cgColor]
            layer.startPoint = CGPoint.zero
            return layer
        }()
        
        //MARK: -
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            
            applyGradient()
        }
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            applyGradient()
        }
        
        override func prepareForInterfaceBuilder() {
            super.prepareForInterfaceBuilder()
            applyGradient()
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            updateGradientFrame()
        }
        
        //MARK: -
        
        func applyGradient() {
            updateGradientDirection()
            layer.sublayers = [gradientLayer]
        }
        
        func updateGradientFrame() {
            gradientLayer.frame = bounds
        }
        
        func updateGradientDirection() {
            gradientLayer.cornerRadius = cornerRadius
            layer.cornerRadius = cornerRadius
            gradientLayer.endPoint = vertical ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0)
        }
    }



@IBDesignable
class ThreeColorsGradientView2: UIView {
    var firstColor: UIColor = #colorLiteral(red: 0.9960784314, green: 0.8274509804, blue: 0.3176470588, alpha: 1)
    var secondColor: UIColor = #colorLiteral(red: 0.7763102651, green: 0.642593801, blue: 0.08450616151, alpha: 1)
    var thirdColor: UIColor = #colorLiteral(red: 0.9960784314, green: 0.8274509804, blue: 0.3176470588, alpha: 1)
    
    @IBInspectable var vertical: Bool = false {
        didSet {
            updateGradientDirection()
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            updateGradientDirection()
        }
    }
    
    lazy var gradientLayer: CAGradientLayer = {
        let layer = CAGradientLayer()
        layer.colors = [firstColor.cgColor, secondColor.cgColor, thirdColor.cgColor]
        layer.startPoint = CGPoint.zero
        return layer
    }()
    
    //MARK: -
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        applyGradient()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        applyGradient()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        applyGradient()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateGradientFrame()
    }
    
    //MARK: -
    
    func applyGradient() {
        updateGradientDirection()
        layer.sublayers = [gradientLayer]
    }
    
    func updateGradientFrame() {
        gradientLayer.frame = bounds
    }
    
    func updateGradientDirection() {
        gradientLayer.cornerRadius = cornerRadius
        layer.cornerRadius = cornerRadius
        gradientLayer.endPoint = vertical ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0)
    }
}
